var couchActions = require('../requests/couchActions.js');
var db = require('../requests/db.js');

var getPersonAttribute = function (personDoc, attribute) {

    if (attribute == "state_id" || attribute == "country_id") {

        var addressArray = personDoc["address"];
        for (var i = 0; i < addressArray.length; i++) {
            if (addressArray[i].residence)
                return addressArray[i][attribute];
        }
    }
    else {
        return personDoc[attribute];
    }
};

var personIdsToValuesScreeningSummaryVersion = function (person, callback) {

    var idsArray = [];
    var dictionary = {};

    /* un-nesting address, email, and phone */
    for (var i = 0; i < person.address.length; i++) {
        if (person.address[i].residence == true) {
            person.address = person.address[i];
            break;
        }
    }
    person.email = person.email[0];
    person.phone = person.phone[0];

    /* grabbing all attributes with ids */
    for (var attrName in person) {

        var attrValue = person[attrName];

        if (attrName.endsWith('_id') && attrValue) {

            idsArray.push(attrValue);
            dictionary[attrName] = attrValue;

        } else if (attrName == "address" && attrValue) {
            if(attrValue.state_id) {
                idsArray.push(attrValue.state_id);
                dictionary["state_id"] = attrValue.state_id;
            }
            if (attrValue.country_id) {
                idsArray.push(attrValue.country_id);
                dictionary["country_id"] = attrValue.country_id;
            }
        } else if (attrName == "phone" && attrValue) {
            if(attrValue.phoneType_id) {
                idsArray.push(attrValue.phoneType_id);
                dictionary["phoneType_id"] = attrValue.phoneType_id;
            }
        } else if (attrName == "guarantor" && attrValue) {
            if(attrValue.relation_id) {
                idsArray.push(attrValue.relation_id);
                dictionary["relation_id"] = attrValue.relation_id;
            }
        }
    }

    var query = "SELECT META(x).id, x.display FROM deco_db x WHERE META(x).id IN "
        + JSON.stringify(idsArray);

    db.n1qlRequest(query, function (err, res) {

        if (err) {
            console.log("ERROR GETTING LISTNAMES, CODE:PDF1");
            return;
        }

        var listNames = res;

        for (var j = 0; j < listNames.length; j++) {

            for (var attrName in dictionary) {

                if (listNames[j].id == dictionary[attrName]) {

                    if (attrName == "state_id" || attrName == "country_id")
                        person.address[attrName] = listNames[j].display;
                    else if (attrName == "phoneType_id")
                        person.phone[attrName] = listNames[j].display;
                    else if (attrName == "relation_id")
                        person.guarantor[attrName] = listNames[j].display;
                    else
                        person[attrName] = listNames[j].display;
                }
            }
        }
        callback(person)
    });
};

var personIdsToValues = function (person, callback) {

    var idsArray = [];
    var dictionary = {};

    /* un-nesting address, email, and phone */
    for (var i = 0; i < person.address.length; i++) {
        if (person.address[i].residence == true) {
            person.address = person.address[i];
            break;
        }
    }
    person.email = person.email[0];
    person.phone = person.phone[0];

    /* grabbing all attributes with ids */
    for (var attrName in person) {

        var attrValue = person[attrName];

        if (attrName.endsWith('_id') && attrValue) {

            idsArray.push(attrValue);
            dictionary[attrName] = attrValue;

        } else if (attrName == "address" && attrValue) {
            if(attrValue.state_id) {
                idsArray.push(attrValue.state_id);
                dictionary["state_id"] = attrValue.state_id;
            }
            if (attrValue.country_id) {
                idsArray.push(attrValue.country_id);
                dictionary["country_id"] = attrValue.country_id;
            }
        } else if (attrName == "phone" && attrValue) {
            if(attrValue.phoneType_id) {
                idsArray.push(attrValue.phoneType_id);
                dictionary["phoneType_id"] = attrValue.phoneType_id;
            }
        } else if (attrName == "guarantor" && attrValue) {
            if(attrValue.relation_id) {
                idsArray.push(attrValue.relation_id);
                dictionary["relation_id"] = attrValue.relation_id;
            }
        }
    }

    var query = "SELECT META(x).id, x.display FROM deco_db x WHERE META(x).id IN "
        + JSON.stringify(idsArray);

    couchActions.n1qlRequest(query, function (err, res) {

        if (err) {
            console.log("ERROR GETTING LISTNAMES, CODE:PDF1");
            return;
        }

        var listNames = res;

        for (var j = 0; j < listNames.length; j++) {

            for (var attrName in dictionary) {

                if (listNames[j].id == dictionary[attrName]) {

                    if (attrName == "state_id" || attrName == "country_id")
                        person.address[attrName] = listNames[j].display;
                    else if (attrName == "phoneType_id")
                        person.phone[attrName] = listNames[j].display;
                    else if (attrName == "relation_id")
                        person.guarantor[attrName] = listNames[j].display;
                    else
                        person[attrName] = listNames[j].display;
                }
            }
        }
        callback(person)
    });
};

var removeClientIds = function (docClientIds, updatePersonClient_ids) {

    var newClientIds = [];

    for (var i = 0; i < docClientIds.length; i++) {

        var currentDocClientId = docClientIds[i];
        for (var j = 0; j < updatePersonClient_ids.length; j++) {

            var currentUpdatePersonClient = updatePersonClient_ids[j];

            if (currentDocClientId === currentUpdatePersonClient) {
                newClientIds.push(currentDocClientId);
                break;
            }
        }
    }

    return newClientIds;
};

module.exports.getPersonAttribute = getPersonAttribute;
module.exports.personIdsToValues = personIdsToValues;
module.exports.personIdsToValuesScreeningSummaryVersion = personIdsToValuesScreeningSummaryVersion;
module.exports.removeClientIds = removeClientIds;