var async = require('async'),
    viewQueryPOST = require('../requests/viewQueryPOST'),
    couchActions = require('../requests/couchActions.js');

function replaceFlatJSONIdsWithValues (res, flatObj, idToPropertyNameDictionary) {

    for (var i = 0; i < res.values.length; i++) {

        var currentResult = res.values[i];

        var propArr = idToPropertyNameDictionary[currentResult.id];

        for (var j = 0; j < propArr.length; j++) {

            var prop = propArr[j];

            if (currentResult.display)
                flatObj[prop] = currentResult.display;
            else if (currentResult.question)
                flatObj[prop] = currentResult.question;
            else if (currentResult.title)
                flatObj[prop] = currentResult.title;
            else if (currentResult.fullName)
                flatObj[prop] = currentResult.fullName;
        }
    }
    

    for (var prop in flatObj) {

        if (prop.endsWith("answerNumber")) {
            //the value of this property will be the [question_id, answerNumber]
            //we need to swap this out for the answer (contained in our viewquery results, ie res.questionValues)
            var ans = flatObj[prop];
            var ansType = Object.prototype.toString.call(ans);
            if (ansType  == "[object Array]") {
                var qestion_id = ans[0];
                var answerNumber = ans[1];

                var answerObject = res.questionValues.filter(function ( obj ) {
                    return (obj.key[0] == qestion_id && obj.key[1] == answerNumber);
                })[0];

                if (answerObject)
                    flatObj[prop] = answerObject.value.answer;
            }
        }
    }
}

// tested to support list, person, question, and baseline ids
function getValuesFromIds (arrays, callback) {

    var valuesQuery = "SELECT META(x).id AS id, x.display, x.question, x.title, " +
        "x.nameFirst || ' ' || x.nameLast AS fullName FROM deco_db x WHERE META(x).id IN "
        + JSON.stringify(arrays.idsArray);

    //var questionValuesQuery = couchActions.ViewQueryTemp.from('lookUps', 'nonListQuestionAnswers')
    //    .keys(arrays.answerNumberArray)
    //    .stale(couchActions.ViewQueryTemp.Update.BEFORE);

    var data = {
        keys: arrays.answerNumberArray,
        stale: 1 // = .stale(couchActions.ViewQueryTemp.Update.BEFORE);  i think... :)
    };



    async.parallel({
        values: function(callback){
            couchActions.n1qlRequest(valuesQuery, function(err, res) {
                if (err) {
                    callback({err:err, code:"IDTV1"}, null);
                    return;
                }
                callback(err, res);
            });
        },
        questionValues: function(callback){

            viewQueryPOST.post(data, function (err, res) {
                if (err) {
                    callback({err:err, code:"IDTV2"}, null);
                    return;
                }
                callback(err, res);
            });

            //couchActions.viewQuery(questionValuesQuery, function(err, res) {
            //    if (err) {
            //        callback({err:err, code:"IDTV2"}, null);
            //        return;
            //    }
            //    callback(err, res);
            //});
        }
    }, function(err, res) {
        callback(err, res);
    });
}

function pushToDictionary (idToPropertyNameDictionary, id, prop) {

    if (idToPropertyNameDictionary.hasOwnProperty(id))
        idToPropertyNameDictionary[id].push(prop);
    else
        idToPropertyNameDictionary[id] = [prop];
}

function findIdsInFlatJSON (flatObj, idToPropertyNameDictionary, arrays) {

    var currentQuestion_id;

    for (var prop in flatObj) {

        var currentFlatObj = flatObj[prop];
        var propMinus2 = prop.substr(0,prop.length-2);

        if (prop.endsWith("answerNumber") && currentFlatObj) {
            arrays.answerNumberArray.push([currentQuestion_id, currentFlatObj]);
            flatObj[prop] = [currentQuestion_id, currentFlatObj];
        }


        else if ((prop.endsWith("_id") || propMinus2.endsWith("_ids[") || prop.endsWith("value1") || prop.endsWith("value2")) && currentFlatObj) {

            // set dictionary
            pushToDictionary(idToPropertyNameDictionary, currentFlatObj, prop);

            // add to idsArray
            if (arrays.idsArray.indexOf(currentFlatObj) === -1)
                arrays.idsArray.push(currentFlatObj);

            if (prop.endsWith("question_id"))
                currentQuestion_id = currentFlatObj;
        }
    }
}


var replaceFlatJSON = function (flatObj, callback) {

    var idToPropertyNameDictionary = {};

    var arrays = {
        idsArray: [],
        answerNumberArray: []
    };

    findIdsInFlatJSON(flatObj, idToPropertyNameDictionary, arrays);

    getValuesFromIds(arrays, function(err, res) {

        if (err) {
            callback(err, null);
            return;
        }

        replaceFlatJSONIdsWithValues(res, flatObj, idToPropertyNameDictionary);

        callback(null, flatObj);
    });
};

var replaceJSON = function (obj, callback) {

    var flatObj = JSON.flatten(obj);

    replaceFlatJSON(flatObj, function (err, res) {

        if (err) {
            callback(err, res);
            return;
        }

        var unflattenObj = JSON.unflatten(res);
        callback(err, unflattenObj);
    });
};

/* PUBLIC API */
module.exports = {
    replaceJSON: replaceJSON,
    replaceFlatJSON: replaceFlatJSON
};