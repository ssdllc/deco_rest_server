function findQueryAlias (query) {

    var alias = query.split(' ');
    return alias[alias.indexOf('deco_db')+1];
}

function removeFromClauseInUpdateQuery (query, alias) {

    return query.replace('deco_db ' + alias + ' ','');
}

function removeFromClauseInDeleteQuery (query, alias) {

    return query.replace('FROM deco_db ' + alias + ' ','');
}

function aggregateSelectStatement (query, aggregate, whereClause) {

    var newQuery =
        "SELECT " + aggregate + "(aEC.id) AS " + aggregate + " " +
        "FROM (" +
        "SELECT aEF.* " +
        "FROM (" + query + ") aEF " +
        "JOIN deco_db aE ON KEYS aEF.id " + whereClause +
        ") aEC";

    return newQuery;
}

function selectStatement (query, whereClause) {

    var newQuery =
        "SELECT aEF.* " +
        "FROM (" + query + ") aEF " +
        "JOIN deco_db aE ON KEYS aEF.id " + whereClause;

    return newQuery;
}

function updateStatement (query, whereClause) {

    var alias = findQueryAlias(query);
    query = removeFromClauseInUpdateQuery(query, alias);

    var newQuery =
        "MERGE INTO deco_db " + alias + " " +
        "USING (SELECT aE.*, META(aE).id as id FROM deco_db aE " + whereClause + ") aEF " +
        "ON KEY aEF.id " +
        "WHEN MATCHED THEN " + query;

    return newQuery;
}

function deleteStatement (query, whereClause) {

    var alias = findQueryAlias(query);
    query = removeFromClauseInDeleteQuery(query, alias);

    var newQuery =
        "MERGE INTO deco_db " + alias + " " +
        "USING (SELECT aE.*, META(aE).id as id FROM deco_db aE " + whereClause + ") aEF " +
        "ON KEY aEF.id " +
        "WHEN MATCHED THEN " + query;

    return newQuery;
}

var getAuthEngineQuery = function (query, userPermissions) {

    var queryLower = query.toLowerCase();

    if (queryLower.substring(0,6) == "select")
        return selectStatement(query, userPermissions.read.whereClause);
    else if (queryLower.substring(0,6) == "update")
        return updateStatement(query, userPermissions.update.whereClause);
    else if (queryLower.substring(0,6) == "delete")
        return deleteStatement(query, userPermissions.delete.whereClause);
};

var getAuthEngineAggregateQuery = function (query, aggregate, userPermissions) {

    return aggregateSelectStatement(query, aggregate, userPermissions.read.whereClause);
};

var addClausesToQuery = function (query, clauses) {

    return query + " " + clauses;
};

function deleteDictionaryStrings(type, dictionary) {

    for (var prop in dictionary) {

        if (prop.endsWith(':'+type)) {
            delete dictionary[prop];
        }
    }
    return dictionary;
}

function turnDictionaryIntoWhereClause(dictionary) {

    var whereClause = "WHERE ";

    for (var prop in dictionary) {
        whereClause += dictionary[prop];
    }
    return whereClause.slice(0, -4);
}

function createWhereClause(perm) {

    var usedTypesAndIdsDictionary = {};

    for (var i=0; i < perm.length; i++) {

        var currentPerm = perm[i];

        /* if currentPerm does not have a client_id
         *  AND dictionary does not contain a property with currentPerm.type
         *  AND dictionary DOES contain a property with currentPerm.client_id + : + currentPerm.type
         */
        if (!currentPerm.client_id
            && !usedTypesAndIdsDictionary[currentPerm.type]
            && usedTypesAndIdsDictionary[currentPerm.client_id + ":" + currentPerm.type]) {

            deleteDictionaryStrings(currentPerm.type, usedTypesAndIdsDictionary);
            usedTypesAndIdsDictionary[currentPerm.type] = "(aE.type='" + currentPerm.type + "') OR ";
        }

        /* if dictionary does not contain a property with currentPerm.type
         *  AND dictionary does not contain a property with currentPerm.client_id + : + currentPerm.type
         */
        else if (!usedTypesAndIdsDictionary[currentPerm.type]
            && !usedTypesAndIdsDictionary[currentPerm.client_id + ":" + currentPerm.type]) {

            if (currentPerm.hasOwnProperty("client_id"))
                usedTypesAndIdsDictionary[currentPerm.client_id + ":" + currentPerm.type] = "(aE.type='" + currentPerm.type + "' " +
                    "AND (ARRAY_LENGTH(aE.client_ids) = 0 OR '" + currentPerm.client_id + "' IN aE.client_ids)) OR ";
            else
                usedTypesAndIdsDictionary[currentPerm.type] = "(aE.type='" + currentPerm.type + "') OR ";
        }
    }
    return turnDictionaryIntoWhereClause(usedTypesAndIdsDictionary);
}

var createUserWhereClauses = function (userPermissions) {

    for(var propertyName in userPermissions) {
        var currentPermissions = userPermissions[propertyName];

        currentPermissions.whereClause = createWhereClause(currentPermissions.permissions)
    }

    return userPermissions;
};


/* PUBLIC API */

module.exports = {
    getAuthEngineQuery: getAuthEngineQuery,
    getAuthEngineAggregateQuery: getAuthEngineAggregateQuery,
    addClausesToQuery: addClausesToQuery,
    createUserWhereClauses: createUserWhereClauses
};