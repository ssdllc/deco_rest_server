
/* PUBLIC API */

/* timestamp */
module.exports.timeStamp = function() {
    var now= new Date(),
        ampm= 'am',
        Y= now.getFullYear(),
        M= (now.getMonth() + 1), //zero based
        d= now.getDate(),
        h= now.getHours(),
        m= now.getMinutes(),
        s= now.getSeconds();
    if(h>= 12){
        if(h>12) h -= 12;
        ampm= 'pm';
    }

    if(m<10) m= '0'+m;
    if(s<10) s= '0'+s;
    return Y + '/' + M + '/' + d + ' ' + h + ':' + m + ':' + s + ampm + ' ';
};
