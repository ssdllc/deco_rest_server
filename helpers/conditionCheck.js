var moment = require('moment');
var response = require('./response.js');

var conditionCheck = function (a, b1, b2, comparator, resObj) {

    if (comparator == "equals" || comparator == "equal")
        return (a == b1);

    else if (comparator == "different")
        return (a != b1);



    else if (comparator == "lessThanDate")
        return moment(a).isBefore(b1);

    else if (comparator == "lessThanOrEqualToDate")
        return moment(a).isSameOrBefore(b1);

    else if (comparator == "moreThanDate")
        return moment(a).isAfter(b1);

    else if (comparator == "moreThanOrEqualToDate")
        return moment(a).isSameOrAfter(b1);

    else if (comparator == "equalToDate")
        return moment(a).isSame(b1);

    else if (comparator == "betweenDates")
        return moment(a).isBetween(b1, b2);



    else if (comparator == "lessThanDateByDay")
        return moment(a).isBefore(b1, 'day');

    else if (comparator == "lessThanOrEqualToDateByDay")
        return moment(a).isSameOrBefore(b1, 'day');

    else if (comparator == "moreThanDateByDay")
        return moment(a).isAfter(b1, 'day');

    else if (comparator == "moreThanOrEqualToDateByDay")
        return moment(a).isSameOrAfter(b1, 'day');

    else if (comparator == "equalToDateByDay")
        return moment(a).isSame(b1, 'day');

    else if (comparator == "betweenDatesByDay")
        return moment(a).isBetween(b1, b2, 'day');



    else if (comparator == "lessThanYear") {
        a = moment().diff(a, 'years');
        return (a < b1);
    }

    else if (comparator == "lessThanOrEqualToYear") {
        a = moment().diff(a, 'years');
        return (a <= b1);
    }

    else if (comparator == "moreThanYear") {
        a = moment().diff(a, 'years');
        return (a > b1);
    }

    else if (comparator == "moreThanOrEqualToYear") {
        a = moment().diff(a, 'years');
        return (a >= b1);
    }

    else if (comparator == "equalToYear") {
        a = moment().diff(a, 'years');
        return (a == b1);
    }

    else if (comparator == "betweenYears") {
        a = moment().diff(a, 'years');
        return ((a >= b1) && (a < b2));
    }



    else if (comparator == "lessThanMonth") {
        a = moment().diff(a, 'months');
        return (a < b1);
    }

    else if (comparator == "lessThanOrEqualToMonth") {
        a = moment().diff(a, 'months');
        return (a <= b1);
    }

    else if (comparator == "moreThanMonth") {
        a = moment().diff(a, 'months');
        return (a > b1);
    }

    else if (comparator == "moreThanOrEqualToMonth") {
        a = moment().diff(a, 'months');
        return (a >= b1);
    }

    else if (comparator == "equalToMonth") {
        a = moment().diff(a, 'months');
        return (a == b1);
    }

    else if (comparator == "betweenMonths") {
        a = moment().diff(a, 'months');
        return ((a >= b1) && (a < b2));
    }



    else if (comparator == "lessThanDay") {
        a = moment().diff(a, 'days');
        return (a < b1);
    }

    else if (comparator == "lessThanOrEqualToDay") {
        a = moment().diff(a, 'days');
        return (a <= b1);
    }

    else if (comparator == "moreThanDay") {
        a = moment().diff(a, 'days');
        return (a > b1);
    }

    else if (comparator == "moreThanOrEqualToDay") {
        a = moment().diff(a, 'days');
        return (a >= b1);
    }

    else if (comparator == "equalToDay") {
        a = moment().diff(a, 'days');
        return (a == b1);
    }

    else if (comparator == "betweenDays") {
        a = moment().diff(a, 'days');
        return ((a >= b1) && (a < b2));
    }

    else {
        if (resObj)
            resObj = response.respond(resObj, "No comparators match", true, true);
        else console.log("No comparators match");
    }
};

/* PUBLIC API */
module.exports.conditionCheck = conditionCheck;
