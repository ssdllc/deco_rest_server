var moment = require('moment');

var lessThanTime = function (now, then, minutes) {

    var difference = now.diff(then, "minutes");

    if (difference < minutes)
        return true;
    else return false;
};


/* PUBLIC API */
module.exports.lessThanTime = lessThanTime;