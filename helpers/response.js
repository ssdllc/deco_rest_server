
/* function that handles the response to the client */
var respond = function(resObj, data, send, err) {

    /* if responded is equal to true don't respond again */
    if (!resObj.responded) {

        /* if you don't want to send json */
        if (send) {
            /* if there is a server error */
            if (err) {
                resObj.res.status(500).send(data);
            }
        } else {
            if (data.err) {
                console.log("LOGGING ERROR THAT IS BEING RETURNED TO CLIENT!");
                console.log(data);
                console.log(data.err);
            }
            resObj.res.json(data);
        }
        resObj.responded = true;
    }
    return resObj;
};

/* function that handles redirects to client */
var redirect = function(resObj, url) {

    /* if responded is equal to true don't respond again */
    if (!resObj.responded) {

        resObj.res.redirect(url);
        resObj.responded = true;
    }
    return resObj;
};

/* PUBLIC API */
module.exports.respond = respond;
module.exports.redirect = redirect;