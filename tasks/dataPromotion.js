var db = require('../requests/db.js');
var moment = require('moment');
var fs = require('fs');
var async = require('async');
var questionIds = [];
var recursiveCount = 0;
var consistentDate = {};

// Write JSON to file
function writeToFile (filename, exportDoc) {
    // ADD DATE?
    fs.writeFile("/Users/deco_edge/deco_rest_server/tasks/dataPromotion/" + consistentDate + filename + ".json", JSON.stringify(exportDoc), "utf8", function (err) {
        if (err) throw err;
        console.log('file saved');
    });
}

//CHECK TO SEE IF I NEED THIS
// Reset Module Global Variables
function resetModuleGlobals () {
    consistentDate = moment().format();
    questionIds = [];
    recursiveCount = 0;
}

// Callback used to get all question docs and save them to JSON after recursive step
function finishedGettingAllQuestions () {

    var query = "SELECT META(x).id AS id, x.* " +
        "FROM deco_db x " +
        "WHERE META(x).id IN " + JSON.stringify(questionIds);

    db.n1qlRequest(query, function (err, res) {

        if (err) {
            console.log({err:err, code:"DP1"});
            return;
        }

        writeToFile("questions", res);
    });
}

// Recursive!
// - find a doc based off a questionId
// - run a function to find more questionIds based off the doc you just found
// - run a callback function if all recursion is done
function recursiveQuestionDocs (questionId, callback) {
    recursiveCount++;

    db.getDoc(questionId, function (err, res) {

        if (err) {
            console.log({err:err, code:"DP2"});
            return;
        }

        var questionDoc = res.value;
        getQuestionIdsFromConditionDocs(questionDoc.conditions, callback);
        recursiveCount--;

        // If all recursion is done and we have a callback, run the callback
        // This is the last function to put everything together in a nice JSON doc
        if (recursiveCount === 0 && callback)
            callback();
    });
}

// If array does not include this item then add it
function pushInArrayIfNotIncluded (array, item) {
    if (!array.includes(item)) {
        array.push(item);
        return true;
    } else return false;
}

// Goes through a Condition JSON object to find all questionsIds
// Will add those Ids to a global array called questionIds
// Will run a recursive operation to find more questionIds and then add those to the array
function getQuestionIdsFromConditionDocs (conditionDocs, callback) {

    for (var i = 0; i < conditionDocs.length; i++) {
        var currentConditionDoc = conditionDocs[i];

        for (var j = 0; j < currentConditionDoc.subconditions.length; j++) {
            var currentSubcondition = currentConditionDoc.subconditions[j];

            if (currentSubcondition.source === "question") {
                var questionId = currentSubcondition.criteria.question_id;

                // Add questionId to questionIds array
                // Start recursive operation to find more questionIds if questionId was successfully pushed
                var hasQuestionIdBeenPushed = pushInArrayIfNotIncluded(questionIds, questionId);
                if (hasQuestionIdBeenPushed)
                    recursiveQuestionDocs(questionId, callback);
            }
        }
    }
}

function getEligibilityDocs (programIds) {
    var query = "SELECT META(x).id AS id, x.* " +
        "FROM deco_db x " +
        "WHERE x.type = 'eligibilityObject' " +
        "AND x.program_id IN " + JSON.stringify(programIds);

    db.n1qlRequest(query, function (err, res) {

        if (err) {
            console.log({err:err, code:"DP3"});
            return;
        }

        var eligibilityDocs = res;
        writeToFile("eligibility", eligibilityDocs);

        for (var i = 0; i < eligibilityDocs.length; i++) {
            var currentEligDoc = eligibilityDocs[i];

            //BEGIN RECURSION
            getQuestionIdsFromConditionDocs(currentEligDoc.conditions, finishedGettingAllQuestions);
        }
    });
}

function getIncomeAssetDocs (programIds) {
    var query = "SELECT META(x).id AS id, x.* " +
        "FROM deco_db x " +
        "WHERE x.criteriaType = 'incomeAssetsEligibility' " +
        "AND x.program_id IN " + JSON.stringify(programIds);

    db.n1qlRequest(query, function (err, res) {

        if (err) {
            console.log({err:err, code:"DP4"});
            return;
        }

        writeToFile("incomeAssets", res);
    });
}

function getClientDocs (clientIds) {
    var query = "SELECT META(x).id AS id, x.* " +
        "FROM deco_db x " +
        "WHERE x.type = 'client' " +
        "AND META(x).id IN " + JSON.stringify(clientIds);

    db.n1qlRequest(query, function (err, res) {

        if (err) {
            console.log({err:err, code:"DPClient"});
            return;
        }

        consistentDate = "1";
        writeToFile("clients", res);
    });
}

// Get list and baseline docs, then write them to a json file
var getListAndBaselineDocs = function () {

    resetModuleGlobals();

    var query = "SELECT META(x).id AS id, x.* " +
        "FROM deco_db x " +
        "WHERE x.type = 'list' OR x.type ='baseline' OR x.type = 'documentConditions' OR x.type = 'permissionSetDocumentTypes'";

    db.n1qlRequest(query, function (err, res) {

        if (err) {
            console.log({err:err, code:"DP5"});
            return;
        }

        writeToFile("listsAndBaselines", res);
    });
};

var getWizardDocs = function (wizardIds) {

    resetModuleGlobals();

    var query = "SELECT META(x).id AS id, x.* " +
        "FROM deco_db x " +
        "WHERE META(x).id IN " + JSON.stringify(wizardIds);

    db.n1qlRequest(query, function (err, res) {

        if (err) {
            console.log({err:err, code:"DP6"});
            return;
        }

        var wizardDocs = res;
        writeToFile("wizards", wizardDocs);

        //BEGIN RECURSION
        for (var i = 0; i < wizardDocs.length; i++) {
            var currentWizardDoc = wizardDocs[i];

            for (var j = 0; j < currentWizardDoc.question_ids.length; j++) {

                var questionId = currentWizardDoc.question_ids[j];

                pushInArrayIfNotIncluded(questionIds, questionId);
                recursiveQuestionDocs(questionId, finishedGettingAllQuestions);
            }
        }
    });
};

var getProgramDocs = function (programIds) {

    resetModuleGlobals();

    var query = "SELECT META(x).id AS id, x.* " +
        "FROM deco_db x " +
        "WHERE META(x).id IN " + JSON.stringify(programIds);

    db.n1qlRequest(query, function (err, res) {

        if (err) {
            console.log({err:err, code:"DP7"});
            return;
        }

        writeToFile("programs", res);
        getIncomeAssetDocs(programIds);
        getEligibilityDocs(programIds);
    });
};

function getPermissionSetDocs (roleDefinitionDocs) {

    var permissionSetIds = [];

    for (var i = 0; i < roleDefinitionDocs.length; i ++) {

        permissionSetIds = permissionSetIds.concatNoDupes(roleDefinitionDocs[i].permissionSet_ids);
    }

    var query =
        "SELECT META(x).id AS id, x.* " +
        "FROM deco_db x " +
        "WHERE META(x).id IN " + JSON.stringify(permissionSetIds);

    db.n1qlRequest(query, function (err, res) {

        if (err) {
            console.log({err:err, code:"DP8"});
            return;
        }

        consistentDate = "1";
        writeToFile("permissionSets", res);
    });
}

var getRoleDefinitionDocs = function (roleDefinitionIds) {

    var query =
        "SELECT META(x).id AS id, x.* " +
        "FROM deco_db x " +
        "WHERE META(x).id IN " + JSON.stringify(roleDefinitionIds);

    db.n1qlRequest(query, function (err, res) {

        if (err) {
            console.log({err:err, code:"DP9"});
            return;
        }

        consistentDate = "1";
        writeToFile("roleDefinitions", res);
        getPermissionSetDocs(res);
    });
};

var importData = function (consistentDate, files) {

    function upsertDoc (id, doc) {
        delete doc.id;
        db.upsertDoc(id, doc, function(err, res){
            if (err) {
                console.log({err:err, code:"DP10"});
                return;
            }
            console.log('json doc uploaded');
        });
    }

    function loopAndUpsertDocs (docs) {
        for (var i = 0; i < docs.length; i++) {
            var doc = docs[i];
            upsertDoc(doc.id,doc);
        }
    }

    for (var i = 0; i < files.length; i++) {
        var docs = require("./dataPromotion/" + consistentDate + files[i] + '.json');
        loopAndUpsertDocs(docs);
    }
};

var getBaselineImportDocs = function () {

    var userAndrew = "7c5a8f70-5335-11e5-8ff1-75243753f1a6";
    var roleDefinition = "99ca1790-df0f-11e5-84f0-932a74faa389";
    var permissionSet = "fcd40e00-df0e-11e5-84f0-932a74faa389";
    var client = "526c9370-50dd-11e5-b238-0b6941b3afec"; //might need to be deco client one day, it is bon right now

    var ids = [userAndrew, roleDefinition, permissionSet, client];

    var idsQuery =
        "SELECT META(x).id AS id, x.* " +
        "FROM deco_db x " +
        "WHERE (x.type = 'user' OR x.type = 'roleDefinition' OR x.type = 'permissionSet' OR x.type = 'client') " +
        "AND META(x).id IN " + JSON.stringify(ids);
    var subDocsQuery =
        "SELECT META(x).id AS id, x.* " +
        "FROM deco_db x " +
        "WHERE (x.type = 'userCredentials' OR x.type = 'roleAssignment') " +
        "AND x.user_id = '" + userAndrew + "'";
    var fourTypesQuery =
        "SELECT META(x).id AS id, x.* " +
        "FROM deco_db x " +
        "WHERE x.type = 'list' OR x.type ='baseline' " +
        "OR x.type = 'documentConditions' OR x.type = 'permissionSetDocumentTypes'";

    async.parallel([
        function(callback) {
            db.n1qlRequest(idsQuery, function (err, res) {
                callback(err, res);
            });
        },
        function(callback){
            db.n1qlRequest(subDocsQuery, function (err, res) {
                callback(err, res);
            });
        },
        function(callback){
            db.n1qlRequest(fourTypesQuery, function (err, res) {
                callback(err, res);
            });
        }
    ], function(err, res) {

        if (err) {
            console.log({err:err, code:"DP11"});
            return;
        }

        var oneBigArray = [].concat.apply([], res);
        consistentDate = "1";
        writeToFile("baselineImport", oneBigArray);
    });
};

/* PUBLIC API */
module.exports.baselineImport = getBaselineImportDocs;
module.exports.roleDefinitions = getRoleDefinitionDocs;
module.exports.programs = getProgramDocs;
module.exports.wizards = getWizardDocs;
module.exports.clients = getClientDocs;
module.exports.listsAndBaselines = getListAndBaselineDocs;
module.exports.import = importData;