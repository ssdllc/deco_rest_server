const db = require('../requests/db.js');
const fs = require("fs");
const Rx = require('rx');
const n1qlRequest = Rx.Observable.fromNodeCallback(db.n1qlRequest);

function writeToFile (doc) {
    fs.writeFile("./tasks/incomeAssetReport.json", JSON.stringify(doc), "utf8", function () {
        console.log('file saved');
    });
}

function cleanIdsInFamilySize (doc, incomeAssetRelDictionary) {
    for (var i = 0; i < doc.relationshipType_ids.length; i++) {

        doc.relationshipType_ids[i] = incomeAssetRelDictionary[doc.relationshipType_ids[i]];
    }
}

function cleanIdsInDoesCount (doc, type, incomeAssetRelDictionary) {

    for (var i = 0; i < doc.doesCount.length; i++) {

        var currentDoesCount = doc.doesCount[i];

        if (type === "totalIncome") {
            currentDoesCount.incomeType_id = incomeAssetRelDictionary[currentDoesCount.incomeType_id];
            currentDoesCount.relationshipType_id = incomeAssetRelDictionary[currentDoesCount.relationshipType_id];
        } else if (type === "totalAssets") {
            currentDoesCount.assetType_id = incomeAssetRelDictionary[currentDoesCount.assetType_id];
            currentDoesCount.relationshipType_id = incomeAssetRelDictionary[currentDoesCount.relationshipType_id];
        }
    }
}

function findDocsWithIds (programsIncomeAssetDocs, incomeAssetRelDictionary) {

    for (var i = 0; i < programsIncomeAssetDocs.length; i++) {

        var currentDoc = programsIncomeAssetDocs[i];

        if (currentDoc.type == "totalIncome") {
            cleanIdsInDoesCount(currentDoc, "totalIncome", incomeAssetRelDictionary);

        } else if (currentDoc.type == "totalAssets") {
            cleanIdsInDoesCount(currentDoc, "totalAssets", incomeAssetRelDictionary);

        } else if (currentDoc.type == "familySize") {
            cleanIdsInFamilySize(currentDoc, incomeAssetRelDictionary);
        }
    }

    writeToFile(programsIncomeAssetDocs);
}

function createDictionary (incomeAssetRelTypeDocs) {

    var incomeAssetRelDictionary = {};

    for (var i = 0; i < incomeAssetRelTypeDocs.length; i++) {

        var currentDoc = incomeAssetRelTypeDocs[i];
        incomeAssetRelDictionary[currentDoc.id] = currentDoc.display;
    }

    return incomeAssetRelDictionary
}

var getData = function () {

    var programsIncomeAssetQuery =
        "SELECT prog.title AS program_title, incAsset.* " +
        "FROM deco_db incAsset " +
        "JOIN deco_db prog ON KEYS incAsset.program_id " +
        "WHERE prog.type = 'program' " +
        "AND incAsset.criteriaType = 'incomeAssetsEligibility' " +
        "ORDER BY prog.title";
    var incomeAssetRelTypeQuery =
        "SELECT META(x).id AS id, x.display " +
        "FROM deco_db x " +
        "WHERE x.type = 'list' " +
        "AND (x.listName = 'assetType' OR x.listName = 'incomeType' OR x.listName = 'relationshipType')";

    var s1 = n1qlRequest(programsIncomeAssetQuery);
    var s2 = n1qlRequest(incomeAssetRelTypeQuery);

    var combination = Rx.Observable.zip(s1, s2, function (s1, s2) {
        return {programsIncomeAssetDocs:s1, incomeAssetRelTypeDocs:s2};
    });

    var finishedObj = {};

    var logObserver = Rx.Observer.create(

        function (result) {
            finishedObj = result;
        },

        function (err) {
            console.log({err:err, code:"IAR1"});
        },

        function () {

            var incomeAssetRelDictionary = createDictionary(finishedObj.incomeAssetRelTypeDocs);
            findDocsWithIds(finishedObj.programsIncomeAssetDocs, incomeAssetRelDictionary);
        }
    );
    combination.subscribe(logObserver);
};

/* PUBLIC API */
module.exports.getReport = getData;
