const uuid = require('node-uuid');
const moment = require('moment');
const db = require('../requests/db.js');
const couchActions = require('../requests/couchActions.js');
const response = require('../helpers/response.js');
const eligPro = require('../endPoints/screening/eligibilityProgramsLists.js');
const conditionCheck = require('../helpers/conditionCheck.js');
const personDocFunc = require('../helpers/personDocFunc.js');
const Rx = require('rx');
const n1qlRequest = Rx.Observable.fromNodeCallback(db.n1qlRequest);
const couchN1qlRequest = Rx.Observable.fromNodeCallback(couchActions.n1qlRequest);

function removeIdsFromEligibilityLists (eligReturnObj) {

    for (var i = 0; i < eligReturnObj.E.length; i++)
        delete eligReturnObj.E[i].program_id;

    for (var j = 0; j < eligReturnObj.I.length; j++)
        delete eligReturnObj.I[j].program_id;

    for (var k = 0; k < eligReturnObj.NMI.length; k++)
        delete eligReturnObj.NMI[k].program_id;

    return eligReturnObj;
}

function buildHouseholdWithIncomeAssets (householdMemberDocs, incomeDocs, assetDocs, personDoc) {

    var householdWithIandA = [];

    if (householdMemberDocs.length > 0) {

        for (var i = 0; i < householdMemberDocs.length; i++) {

            var mem = householdMemberDocs[i];

            /* adding name if relationshipType is self */
            if (mem.relationshipType_display == "Self") {
                mem.nameFirst = personDoc.nameFirst;
                mem.nameLast = personDoc.nameLast;
            }

            var newObj = {
                "nameFirst":mem.nameFirst,
                "nameLast":mem.nameLast,
                "relationshipType_display":mem.relationshipType_display,
                "income":[],
                "assets":[]
            };

            for (var j = 0; j < incomeDocs.length; j++) {
                if (incomeDocs[j].householdMember_id == mem.id) {
                    newObj.income.push({
                        "incomeType_display": incomeDocs[j].incomeType_display,
                        "amount":incomeDocs[j].amount
                    });
                }
            }
            for (var k = 0; k < assetDocs.length; k++) {
                if (assetDocs[k].householdMember_id == mem.id) {
                    newObj.assets.push({
                        "assetType_display": assetDocs[k].assetType_display,
                        "amount":assetDocs[k].amount
                    });
                }
            }
            householdWithIandA.push(newObj);
        }
    }
    return householdWithIandA;
}

function crawlAnswersLookingForManagedLists(screeningDoc, getIDsORsetAnswers, answerDisplays) {

    for (var i = 0; i < screeningDoc.results.length; i++) {

        var res = screeningDoc.results[i];
        if (res.answers.length == 1) {

            var ans = res.answers[0];
            if (ans.answer == "") {
                //NO ANSWER - ADD TO ARRAY OR SET THE ANSWER
                if (getIDsORsetAnswers=="getIDs") {
                    //BUILD ARRAY OF ANSWER ID'S
                    if (ans.answer_id) { //this was checking length of answerNumber = 36
                        //USE THE ANSWER NUMBER (now answer_id)
                        answerDisplays.push(ans.answer_id);
                    } else {
                        //USE THE QUESTION_ID
                        answerDisplays.push(res.question_id);
                    }
                } else {
                    //SET ANSWER
                    for (var j=0; j<answerDisplays.length; j++) {
                        var ansObj = answerDisplays[j];
                        if (ansObj.id == ans.answer_id) {
                        //if (ansObj.id == ans.answerNumber) {
                            //THIS IS A MANAGED LIST ANSWER
                            ans.answer = ansObj.display;
                            break;
                        } else if (ansObj.id == res.question_id) {
                            //THIS IS A MANAGED LIST CUSTOM ANSWER
                            for (var k=0; k < ansObj.answerOptions.length; k++) {
                                var ansOpt = ansObj.answerOptions[k];
                                if (ansOpt.answerNumber == ans.answerNumber) {
                                    ans.answer = ansOpt.answer;
                                    break;
                                }
                            }
                            break;
                        }
                    }
                }

                /* //THE CODE ABOVE FIXES ALL THIS SHIZZY
                if (ans.answerNumber.length == 36) {

                    if (getIDsORsetAnswers=="getIDs") {
                        answerDisplays.push(ans.answerNumber);
                    } else {

                        for (var j=0; j<answerDisplays.length; j++) {
                            var ansObj = answerDisplays[j];
                            if (ansObj.id==ans.answerNumber) {
                                ans.answer = ansObj.display;
                                break;
                            }
                        }
                    }
                }
                */
            }
        }
    }
}

function getManagedListAnswers (screeningDoc, callback) {
    var answerDisplays = [];
    crawlAnswersLookingForManagedLists(screeningDoc, "getIDs", answerDisplays);
    if (answerDisplays.length > 0) {

        var listDocsQuery = "SELECT META(x).id AS id, x.* FROM deco_db x WHERE META(x).id IN "
            + JSON.stringify(answerDisplays) + " ORDER BY META(x).id";

        db.n1qlRequest(listDocsQuery, function(err, res) {

            console.log('done - listDocsQuery');

            if (err) {
                console.log({err:err, code:"SS1"});
                return;
            }

            crawlAnswersLookingForManagedLists(screeningDoc, "setAnswers", res);
            callback();
        });

    } else callback();
}

var generateScreeningSummary = function (screening_id) {

    var screeningQuery =
        "SELECT META(screening).id AS id, screening.*, client.name AS clientName " +
        "FROM deco_db screening " +
        "JOIN deco_db client ON KEYS screening.client_ids[0] " +
        "WHERE screening.type = 'screening' " +
        "AND META(screening).id = '" + screening_id + "'";

    db.n1qlRequest(screeningQuery, function (err, res) {

        console.log('done - screeningQuery');

        if (err) {
            console.log({err:err, code:"SS2"});
            return;
        }

        if (res.length === 0)
            return;

        var screeningDoc = res[0];

        /* I update the screeningDoc */
        getManagedListAnswers(screeningDoc, function() {

            console.log('done - getManagedListAnswers');

            var person_id = screeningDoc.person_id;
            screeningDoc["id"] = screening_id;

            db.getDoc(person_id, function (err, res) {

                console.log('done - person getDoc: ' + person_id);

                if (err) {
                    console.log({err:err, code:"SS3"});
                    return;
                }

                var personDoc = res.value;

                personDocFunc.personIdsToValuesScreeningSummaryVersion(personDoc, function (personDoc) {

                    console.log('done - personIdsToValues');

                    var questionIds = [];

                    for (var i = 0; i < screeningDoc.results.length; i++) {
                        questionIds.push(screeningDoc.results[i].question_id);
                    }

                    var questionDocsQuery =
                        "SELECT META(x).id AS id, x.* FROM deco_db x WHERE META(x).id IN "
                        + JSON.stringify(questionIds) + " ORDER BY META(x).id";

                    var householdMembersQuery =
                        "SELECT META(x).id AS id, x.nameFirst, x.nameLast, rel.display AS relationshipType_display " +
                        "FROM deco_db x " +
                        "JOIN deco_db rel ON KEYS x.relationshipType_id " +
                        "WHERE x.type = 'householdMember' AND x.person_id = '" + person_id + "' " +
                        "ORDER BY x.nameLast, x.nameFirst";

                    var incomeDocsQuery = "SELECT x.householdMember_id, x.amount, income.display AS incomeType_display, " +
                        "house.nameFirst, house.nameLast " +
                        "FROM deco_db x " +
                        "JOIN deco_db income ON KEYS x.incomeType_id " +
                        "JOIN deco_db house ON KEYS x.householdMember_id " +
                        "WHERE x.type = 'income' AND house.person_id = '" + person_id + "'";

                    var assetsDocsQuery = "SELECT x.householdMember_id, x.amount, asset.display AS assetType_display, " +
                        "house.nameFirst, house.nameLast " +
                        "FROM deco_db x " +
                        "JOIN deco_db asset ON KEYS x.assetType_id " +
                        "JOIN deco_db house ON KEYS x.householdMember_id " +
                        "WHERE x.type = 'asset' AND house.person_id = '" + person_id + "'";



                    var s1 = n1qlRequest(questionDocsQuery);
                    var s2 = n1qlRequest(householdMembersQuery);
                    var s3 = n1qlRequest(incomeDocsQuery);
                    var s4 = n1qlRequest(assetsDocsQuery);

                    var combination = Rx.Observable.zip(s1, s2, s3, s4, function (s1, s2, s3, s4) {
                        return {
                            questionDocs:s1,
                            householdMemberDocs:s2,
                            incomeDocs:s3,
                            assetsDocs:s4
                        };
                    });

                    var finishedObj = {};

                    var logObserver = Rx.Observer.create(

                        function (result) {
                            finishedObj = result;
                        },

                        function (err) {
                            console.log({err:err, code:"SS4"});
                        },

                        function () {

                            var householdMembers = buildHouseholdWithIncomeAssets(finishedObj.householdMemberDocs,
                                finishedObj.incomeDocs, finishedObj.assetsDocs, personDoc);

                            eligPro.buildEligibilityProgramsListScreeningSummaryVersion(screening_id , function (eligReturnObj) {

                                console.log('done - buildEligibilityProgramsList');

                                var clientName = screeningDoc.clientName;
                                delete screeningDoc.clientName;

                                var screeningSummaryDoc = {
                                    person: personDoc,
                                    screening: screeningDoc,
                                    questions: finishedObj.questionDocs,
                                    householdMembers: householdMembers,
                                    eligibility: removeIdsFromEligibilityLists(eligReturnObj),
                                    clientName: clientName,
                                    client_ids: screeningDoc.client_ids,
                                    date_created: moment().format(),
                                    type: "screeningSummary"
                                };

                                var newID = uuid.v1();

                                db.createDoc(newID, screeningSummaryDoc, function (err, res) {

                                    console.log('done - create screeningDoc');

                                    if (err) {
                                        console.log('ERROR WITH SCREENING SUMMARY')
                                    } else console.log('DONE WITH SCREENING SUMMARY');
                                });
                            });
                        }
                    );
                    combination.subscribe(logObserver);
                });
            });
        });
    });
};

var generateScreeningSummaryUserVersion = function (reqObj, resObj, screening_id) {

    var screeningQuery =
        "SELECT META(screening).id AS id, screening.*, client.name AS clientName " +
        "FROM deco_db screening " +
        "JOIN deco_db client ON KEYS screening.client_ids[0] " +
        "WHERE screening.type = 'screening' " +
        "AND META(screening).id = '" + screening_id + "'";

    couchActions.n1qlRequest(screeningQuery, function (err, res) {

        console.log('done - screeningQuery');

        if (err) {
            resObj = response.respond(resObj, {err:err, code:"SS2a"});
            return;
        }

        if (res.length === 0) {
            resObj = response.respond(resObj, {err:err, code:"SS2b"});
            return;
        }

        var screeningDoc = res[0];

        /* I update the screeningDoc */
        getManagedListAnswers(screeningDoc, function() {

            console.log('done - getManagedListAnswers');

            var person_id = screeningDoc.person_id;
            screeningDoc["id"] = screening_id;

            couchActions.getDoc(person_id, function (err, res) {

                console.log('done - person getDoc: ' + person_id);

                if (err) {
                    resObj = response.respond(resObj, {err:err, code:"SS3"});
                    return;
                }

                var personDoc = res.value;

                personDocFunc.personIdsToValues(personDoc, function (personDoc) {

                    console.log('done - personIdsToValues');

                    var questionIds = [];

                    for (var i = 0; i < screeningDoc.results.length; i++) {
                        questionIds.push(screeningDoc.results[i].question_id);
                    }

                    var questionDocsQuery =
                        "SELECT META(x).id AS id, x.* FROM deco_db x WHERE META(x).id IN "
                        + JSON.stringify(questionIds) + " ORDER BY META(x).id";

                    var householdMembersQuery =
                        "SELECT META(x).id AS id, x.nameFirst, x.nameLast, rel.display AS relationshipType_display " +
                        "FROM deco_db x " +
                        "JOIN deco_db rel ON KEYS x.relationshipType_id " +
                        "WHERE x.type = 'householdMember' AND x.person_id = '" + person_id + "' " +
                        "ORDER BY x.nameLast, x.nameFirst";

                    var incomeDocsQuery = "SELECT x.householdMember_id, x.amount, income.display AS incomeType_display, " +
                        "house.nameFirst, house.nameLast " +
                        "FROM deco_db x " +
                        "JOIN deco_db income ON KEYS x.incomeType_id " +
                        "JOIN deco_db house ON KEYS x.householdMember_id " +
                        "WHERE x.type = 'income' AND house.person_id = '" + person_id + "'";

                    var assetsDocsQuery = "SELECT x.householdMember_id, x.amount, asset.display AS assetType_display, " +
                        "house.nameFirst, house.nameLast " +
                        "FROM deco_db x " +
                        "JOIN deco_db asset ON KEYS x.assetType_id " +
                        "JOIN deco_db house ON KEYS x.householdMember_id " +
                        "WHERE x.type = 'asset' AND house.person_id = '" + person_id + "'";



                    var s1 = couchN1qlRequest(questionDocsQuery);
                    var s2 = couchN1qlRequest(householdMembersQuery);
                    var s3 = couchN1qlRequest(incomeDocsQuery);
                    var s4 = couchN1qlRequest(assetsDocsQuery);

                    var combination = Rx.Observable.zip(s1, s2, s3, s4, function (s1, s2, s3, s4) {
                        return {
                            questionDocs:s1,
                            householdMemberDocs:s2,
                            incomeDocs:s3,
                            assetsDocs:s4
                        };
                    });

                    var finishedObj = {};

                    var logObserver = Rx.Observer.create(

                        function (result) {
                            finishedObj = result;
                        },

                        function (err) {
                            resObj = response.respond(resObj, {err:err, code:"SS4a"});
                            return;
                        },

                        function () {

                            var householdMembers = buildHouseholdWithIncomeAssets(finishedObj.householdMemberDocs,
                                finishedObj.incomeDocs, finishedObj.assetsDocs, personDoc);

                            eligPro.buildEligibilityProgramsList(screening_id , function (eligReturnObj) {

                                console.log('done - buildEligibilityProgramsList');

                                var clientName = screeningDoc.clientName;
                                delete screeningDoc.clientName;

                                var screeningSummaryDoc = {
                                    person: personDoc,
                                    screening: screeningDoc,
                                    questions: finishedObj.questionDocs,
                                    householdMembers: householdMembers,
                                    eligibility: removeIdsFromEligibilityLists(eligReturnObj),
                                    clientName: clientName,
                                    client_ids: screeningDoc.client_ids,
                                    date_created: moment().format(),
                                    type: "screeningSummary"
                                };

                                var newID = uuid.v1();

                                couchActions.createDoc(newID, screeningSummaryDoc, function (err, res) {

                                    console.log('done - create screeningDoc');

                                    if (err) {
                                        resObj = response.respond(resObj, {err:err, code:"SS4b"});
                                        return;
                                    }
                                    resObj = response.respond(resObj, {"screeningSummary_id":newID});
                                });
                            });
                        }
                    );
                    combination.subscribe(logObserver);
                });
            });
        });
    });
};

function updateOneScreeningSummary (reqObj, resObj, screening_id) {

    var query = "SELECT META(sum).id as id, " +
        "scr.date_updated, sum.date_created AS date_created " +
        "FROM deco_db sum " +
        "JOIN deco_db scr ON KEYS sum.screening.id " +
        "WHERE sum.type = 'screeningSummary' " +
        "AND sum.screening.id = '"+screening_id+"' " +
        "ORDER BY sum.date_created desc " +
        "LIMIT 1";

    db.n1qlRequest(query, function(err, res) {

        if (err) {
            resObj = response.respond(resObj, {err:err, code:"SS8a"});
            return;
        }

        if (res.length === 0) {
            generateScreeningSummaryUserVersion(reqObj, resObj, screening_id);
            return;
        }

        var data = res[0];
        
        var updated = data.date_updated;
        var created = data.date_created;

        if (!conditionCheck.conditionCheck(updated, created, -1, "moreThanDate")) {
            resObj = response.respond(resObj, {"screeningSummary_id":data.id});
            return;
        }

        generateScreeningSummaryUserVersion(reqObj, resObj, screening_id);
    });
}

function updateScreeningSummary () {

    console.log('start - update screeningSummary (USS)');

    var query = "SELECT sum.screening.id, " +
        "scr.date_updated, MAX(sum.date_created) AS date_created " +
        "FROM deco_db sum " +
        "JOIN deco_db scr ON KEYS sum.screening.id " +
        "WHERE sum.type = 'screeningSummary' " +
        "GROUP BY sum.screening.id";

    db.n1qlRequest(query, function(err, res) {

        console.log('(USS) found all screeningSummary docs');

        if (err) {
            console.log({err:err, code:"SS8"});
            return;
        }

        var data = res;
        for (var i = 0; i < data.length; i++) {

            var updated = data[i].date_updated;
            var created = data[i].date_created;

            if (conditionCheck.conditionCheck(updated, created, -1, "moreThanDate")) {
                console.log('(USS) generating screeningSummary data['+i+']');
                generateScreeningSummary(data[i].id);
            }
        }
    });
}

function createScreeningSummary () {

    console.log('start - create screening summary (CSS)');

    var query = "select meta(scr).id screening_id " +
        "from deco_db scr " +
        "where scr.type ='screening' and " +
        "meta(scr).id not within (select distinct sum.screening.id from deco_db sum where sum.type='screeningSummary')";

    db.n1qlRequest(query, function (err, res) {

        console.log('(CSS) found all screening docs');

        if (err) {
            console.log({err:err, code:"SS9"});
            return;
        }

        var data = res;
        for (var i = 0; i < data.length; i ++) {
            console.log('(CSS) generating screeningSummary data['+i+']');
            generateScreeningSummary(data[i].screening_id);
        }
    });
}

var runScreeningSummary = function () {

    updateScreeningSummary();
    createScreeningSummary();
};

/* PUBLIC API */
module.exports.runScreeningSummary = runScreeningSummary;
module.exports.generateScreeningSummaryUserVersion = updateOneScreeningSummary;
