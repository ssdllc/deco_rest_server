var fs = require('fs');
var uuid = require('node-uuid');
var db = require('../requests/db.js');
var importPath = "/Users/deco_edge/4am-deco/src/import-documents/";

/* Import Documents */

function importJSONfile(jsFile, parentObjectName) {

    var file = importPath + jsFile;
    /* open the file and read it */
    fs.readFile(file, 'utf8', function (err, data) {
        if (err) {
            console.log('Error: ' + err);
            return;
        }
        /* pull the contents of the file into a json object */
        data = JSON.parse(data);

        var recs;
        if (parentObjectName == null) {
            recs = data;

            createDoc(recs);
        }
        else {
            recs = data[parentObjectName];

            /* parse the json object - creating multiple documents */
            for (var i = 0; i < recs.length; i++) {
                var rec = recs[i];
                createDoc(rec);
            }
        }
    });
}

function createDoc(data, id) {

    var newID;

    if (id)
        newID = id;
    else {
        if (data.id)
            newID = data.id;
        else
            newID = uuid.v1();

        data.channels = ["ssd"];
        delete data.id;
    }

    db.createDoc(newID, data, function() {

    });
}

function newImport (path) {

    var program = require(importPath+path);

    for (var i = 0; i < program.incomeAssetDocs.length; i++) {
        createDoc(program.incomeAssetDocs[i]);
    }
}

function createDocWithIdIGive (path, id) {

    var doc = require(importPath+path);
    createDoc(doc, id);
}


function importBaselineData () {

    function upsertDoc (id, doc) {
        delete doc.id;
        db.upsertDoc(id, doc, function(err, res){
            if (err) {
                console.log('json doc failed to upload');
            } else console.log('json doc uploaded');
        });
    }

    function loopAndUpsertDocs (docs) {
        for (var i = 0; i < docs.length; i++) {
            var doc = docs[i];
            upsertDoc(doc.id,doc);
        }
    }

    var docs = require('./baseline.json');
    loopAndUpsertDocs(docs);
}




var lc_import = function(res,data) {

    /* deco Asset and Income data: by Andrew on 9/10/2015 */
    //importJSONfile("9102015_program_AandI/familySizeTypesQMB.json","familySizeTypes");
    //importJSONfile("9102015_program_AandI/totalIncomeTypesQMB.json","totalIncomeTypes");
    //importJSONfile("9102015_program_AandI/familySizeIncomeLimitsTypesQMB.json","familySizeIncomeLimitsTypes");
    //importJSONfile("9102015_program_AandI/totalAssetsTypesQMB.json","totalAssetsTypes");
    //importJSONfile("9102015_program_AandI/assetLimitsTypesQMB.json","assetLimitsTypes");
    //importJSONfile("9102015_program_AandI/coupleStatusTypesQMB.json",null);
    //
    //importJSONfile("9102015_program_AandI/familySizeTypesSSI.json","familySizeTypes");
    //importJSONfile("9102015_program_AandI/totalIncomeTypesSSI.json","totalIncomeTypes");
    //importJSONfile("9102015_program_AandI/familySizeIncomeLimitsTypesSSI.json","familySizeIncomeLimitsTypes");
    //importJSONfile("9102015_program_AandI/totalAssetsTypesSSI.json","totalAssetsTypes");
    //importJSONfile("9102015_program_AandI/assetLimitsTypesSSI.json","assetLimitsTypes");
    //importJSONfile("9102015_program_AandI/coupleStatusTypesSSI.json",null);
    //
    //importJSONfile("9102015_program_AandI/familySizeTypesSSDI.json","familySizeTypes");
    //importJSONfile("9102015_program_AandI/totalIncomeTypesSSDI.json","totalIncomeTypes");
    //importJSONfile("9102015_program_AandI/familySizeIncomeLimitsTypesSSDI.json","familySizeIncomeLimitsTypes");
    //importJSONfile("9102015_program_AandI/totalAssetsTypesSSDI.json","totalAssetsTypes");
    //importJSONfile("9102015_program_AandI/assetLimitsTypesSSDI.json","assetLimitsTypes");
    //importJSONfile("9102015_program_AandI/coupleStatusTypesSSDI.json",null);
    //
    //importJSONfile("9102015_program_AandI/familySizeTypesFAC.json","familySizeTypes");
    //importJSONfile("9102015_program_AandI/totalIncomeTypesFAC.json","totalIncomeTypes");
    //importJSONfile("9102015_program_AandI/familySizeIncomeLimitsTypesFAC.json","familySizeIncomeLimitsTypes");
    //importJSONfile("9102015_program_AandI/coupleStatusTypesFAC.json",null);

    /* deco questions: by Andrew on 9/4/2015 */
    //importJSONfile("questions/deco_criminal.json",null);
    //importJSONfile("questions/deco_dependent.json",null);
    //importJSONfile("questions/deco_homeless.json",null);
    //importJSONfile("questions/deco_immigration.json",null);
    //importJSONfile("questions/deco_insurance.json",null);
    //importJSONfile("questions/deco_medical.json",null);
    //importJSONfile("questions/deco_medicare.json",null);
    //importJSONfile("questions/deco_pregnant.json",null);
    //importJSONfile("questions/deco_ssi.json",null);
    //importJSONfile("questions/deco_stepChild.json",null);

    //importJSONfile("baseline/deco_elig.json",null);

    /* deco import data: by Andrew on 9/2/2015 */
    //importJSONfile("baseline/deco_assetTypeTypes.json","assetTypeTypes");
    //importJSONfile("baseline/deco_incomeTypeTypes.json","incomeTypeTypes");
    //importJSONfile("baseline/deco_relationshipTypeTypes.json","relationshipTypeTypes");

    //importJSONfile("baseline/deco_familySizeTypes.json","familySizeTypes");
    //importJSONfile("baseline/deco_totalIncomeTypes.json","totalIncomeTypes");
    //importJSONfile("baseline/deco_familySizeIncomeLimitsTypes.json","familySizeIncomeLimitsTypes");
    //importJSONfile("baseline/deco_totalAssetsTypes.json","totalAssetsTypes");
    //importJSONfile("baseline/deco_assetLimitsTypes.json","assetLimitsTypes");
    //importJSONfile("baseline/deco_coupleStatusTypes.json","coupleStatusTypes");

    //importJSONfile("baseline/deco_program.json",null);


    /* normal data: by Andrew on 9/1/2015 */
    //importJSONfile("baseline/categoryTypes.json","categoryTypes");
    //importJSONfile("baseline/documentConditionTypes.json","documentConditionTypes");
    //importJSONfile("baseline/citizenshipStatusTypes.json","citizenshipStatusTypes");
    //importJSONfile("baseline/countryCodeTypes.json","countryCodeTypes");
    //importJSONfile("baseline/countryTypes.json","countryTypes");
    //importJSONfile("baseline/educationLevelTypes.json","educationLevelTypes");
    //importJSONfile("baseline/genderTypes.json","genderTypes");
    //importJSONfile("baseline/immigrationStatusTypes.json","immigrationStatusTypes");
    //importJSONfile("baseline/languageTypes.json","languageTypes");
    //importJSONfile("baseline/namePrefixTypes.json","namePrefixTypes");
    //importJSONfile("baseline/nameSuffixTypes.json","nameSuffixTypes");
    //importJSONfile("baseline/phoneTypes.json","phoneTypes");
    //importJSONfile("baseline/raceTypes.json","raceTypes");
    //importJSONfile("baseline/relationshipStatusTypes.json","relationshipStatusTypes");
    //importJSONfile("baseline/relationTypes.json","relationTypes");
    //importJSONfile("baseline/stateTypes.json","stateTypes");
    //importJSONfile("baseline/hospitalTypes.json","hospitalTypes");
    //importJSONfile("baseline/relationshipTypeTypes.json","relationshipTypeTypes");
    //importJSONfile("baseline/programCategoryTypes.json","programCategoryTypes");
    //importJSONfile("baseline/role_admin.json",null);
    //importJSONfile("baseline/user_admin.json",null);
    //importJSONfile("baseline/userCredentials_admin.json",null);
    //importJSONfile("baseline/webSession_admin.json",null);



    //newImport("11182015_deco_upload/DC_ABD.json");
    //newImport("11182015_deco_upload/DC_Childless_Adult.json");
    //newImport("11182015_deco_upload/DC_Pregnant_Woman_&_Newborns.json");
    //newImport("11182015_deco_upload/MD_ABD.json");
    //newImport("11182015_deco_upload/MD_Childless_Adult_NON_CITIZEN.json");
    //newImport("11182015_deco_upload/MD_Childless_Adult.json");
    //newImport("11182015_deco_upload/MD_FAC_NON_CITIZEN.json");
    //newImport("11182015_deco_upload/MD_FAC.json");
    //newImport("11182015_deco_upload/MD_MCHP_ages_1_6.json");
    //newImport("11182015_deco_upload/MD_MCHP_ages_1_6_NON_CITIZEN.json");
    //newImport("11182015_deco_upload/MD_MCHP_Newborns.json");
    //newImport("11182015_deco_upload/MD_MCHP_Newborns_NON_CITIZEN.json");
    //newImport("11182015_deco_upload/MD_MCHP_Premium.json");
    //newImport("11182015_deco_upload/MD_Pregnant_Women_&_Newborns.json");
    //newImport("11182015_deco_upload/MD_Pregnant_Women_&_Newborns_NON_CITIZEN.json");
    //newImport("11182015_deco_upload/MD_QMB.json");
    //newImport("11182015_deco_upload/MD_SSA_SSDI.json");
    //newImport("11182015_deco_upload/MD_SSA_SSI.json");

    //createDocWithIdIGive("md_childless_adult_inmate.json","a99c5790-8a15-11e5-af7a-855a80413181");

    importBaselineData();

};

/* PUBLIC API */
module.exports.getImport = lc_import;