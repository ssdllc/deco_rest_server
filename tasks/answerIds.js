var async = require('async'),
    db = require('../requests/db.js');

function updateDocs (docs) {

    for (var i = 0; i < docs.length; i++) {

        var id = docs[i].id;
        delete docs[i].id;

        db.updateDoc(id, docs[i], function(err, res) {

            if (err) {
                console.log({err:err, code:"ANSWI1"});
                return;
            }

            console.log('DOC UPDATED');
        });
    }
}

function updateAnswerNumberAttribute(prop, res) {
    var objType = Object.prototype.toString.call( res[prop] );
    if(objType == '[object String]') {
        if (res[prop].length === 36) {

            var newProp = prop.substring(0, prop.length-6) + "_id";
            res[newProp] = res[prop];
            delete res[prop];
        }
    }
}

var iterateFlattenedDocsAnswerId = function (res) {
    for (var prop in res) {

        if (prop.endsWith("answerNumber")) {

            updateAnswerNumberAttribute(prop, res)
        }
    }
};

var iterateDocsEqual = function (res) {

    for(var i = 0; i < res.length; i++) {

        var conditionDoc = res[i];

        for (var j = 0; j < conditionDoc.conditions.length; j++) {

            var currentCondition = conditionDoc.conditions[j];

            for (var k = 0; k < currentCondition.subconditions.length; k++) {

                var currentSubcondition = currentCondition.subconditions[k];

                if (currentSubcondition.source === "question") {

                    if (!currentSubcondition.results.hasOwnProperty("equal")) {
                        currentSubcondition.results.equal = true;
                    }
                }
            }
        }
    }

    updateDocs(res);
};

function flattenUnflattenAndUpdate (res, func) {

    res = JSON.flatten(res);

    func(res);

    res = JSON.unflatten(res);

    updateDocs(res);
}

var getAnswerIdDocs = function () {
    var query =
        "SELECT META(x).id AS id, x.* " +
        "FROM deco_db x " +
        "WHERE x.type = 'eligibilityObject' OR x.type = 'question' OR x.type = 'screening'";

    db.n1qlRequest(query, function (err, res) {

        if (err) {
            console.log({err:err, code:"ANSWI2"});
            return;
        }

        flattenUnflattenAndUpdate(res, iterateFlattenedDocsAnswerId);
    });
};

var getEqualDocs = function () {
    var query =
        "SELECT META(x).id AS id, x.* " +
        "FROM deco_db x " +
        "WHERE x.type = 'eligibilityObject' OR x.type = 'question'";

    db.n1qlRequest(query, function (err, res) {

        if (err) {
            console.log({err:err, code:"ANSWI3"});
            return;
        }

        iterateDocsEqual(res);
    });
};

/* PUBLIC API */
module.exports = {
    updateAnswerIDs: getAnswerIdDocs,
    updateEqual: getEqualDocs
};