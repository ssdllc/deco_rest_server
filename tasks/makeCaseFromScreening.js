/**
 * This is a module for making case documents from all existing screening documents
 * @namespace tasks/makeCaseFromScreening
 *
 * */
var db = require('../requests/db.js');
var couchbase = require("couchbase");
const fs = require('fs'),
    Rx = require('rx'),
    uuid = require('node-uuid'),
    csvParser = require('csv-parser'),
    moment = require('moment'),
    config = require('../config.json'),
    validator = require('validator'),
    response = require('../helpers/response.js'),
    couchActions = require('../requests/couchActions.js'),
    create = require('../crud/create/create.js'),
    rxCreateDoc = Rx.Observable.fromNodeCallback(couchActions.createDoc),
    ssn_ignoreList = ["777777777","666666666"];

function getClientDocument(client_id, callback) {
    var client_id = client_id;
    db.getDoc(client_id, function(err, res) {
        if(err) {
            console.log('error getting client document from database');
        }
        if(res) {
            console.log('got the client document from database');
            callback(res);
        }
    })
}

function createCaseDocs(res) {

    const date = moment().format();

    res.forEach(function(x) {
        var caseDocId = uuid.v1();
        var referralDocId = uuid.v1();

        const caseDoc = {
            "client_ids": x.client_ids,
            "person_id": x.person_id,
            "screening_id": x.id,
            "user_id": x.user_id,
            "date_created": date,
            "date_updated": date,
            "type": "case",
            "doc_notes": "october_promotion"
        };

        const referralDoc = {
            "admissionInformation": {
                "accountNumber": x.accountNumber,
                "admissionReferralType_id": '',
                "admitDate": x.admission_date,
                "dischargeDate": '',
                "financialClass": '',
                "insuranceInformation": '',
                "medicalRecordNumber": x.medicalRecordNumber,
                "referralDate": date,
                "totalCharges": '',
                "visitType_id": ''
            },
            "client_ids": x.client_ids,
            "filename": {
                "originalName": '',
                "serverName": ''
            },
            "person_id": x.person_id,
            "type": "referral",
            "user_id": x.user_id,
            "referralSpecifications": {},
            "case_id": caseDocId
        };

        var rxGetClientDocument = Rx.Observable.fromCallback(getClientDocument);
        var getClientDocumentStream = rxGetClientDocument(x.client_ids[0]);
        getClientDocumentStream.subscribe(
            function(data) {
                referralDoc.referralSpecifications = data.value.referralSpecifications;
                console.log();
                //call function to create referral here (?)
                //either that or add to referral object here.. i.e referralDoc.referralSpecifications = data.referralSpecifications
            },
            function(e) {

            },
            function() {
                console.log();
                db.createDoc(caseDocId, caseDoc, function(err, res) {
                    if(err) {
                        console.log('error uploading case document');
                    }
                    if(res) {
                        console.log('case document uploaded');
                    }
                });

                db.createDoc(referralDocId, referralDoc, function(err, res) {
                    if(err) {
                        console.log('error uploading referral document');
                    }
                    if(res) {
                        console.log('referral document uploaded');
                    }
                });
            }
        );



    });
}

function getScreeningDocs() {

    var getScreeningDocsQuery = "SELECT META(x).id AS id, x.* FROM deco_db x WHERE x.type = 'screening'";
    db.n1qlRequest(getScreeningDocsQuery, function (err, res) {

        if (err) {
            console.log('error');
        }

        if (res) {
            createCaseDocs(res);
        }
    });

}

module.exports.makeCaseFromScreening = getScreeningDocs;