var couchActions = require('./couchActions.js');
var response = require('../helpers/response.js');

var simpleN1qlRequest = function (reqObj, resObj, query) {


    couchActions.n1qlRequest(query, function(err, res) {

        if (err) {
            resObj = response.respond(resObj, {err:err, code:"SNR1"});
            return;
        }

        /* respond to client */
        resObj = response.respond(resObj, {"data":res});
    });
};

/* PUBLIC API */
module.exports.setRequest = simpleN1qlRequest;