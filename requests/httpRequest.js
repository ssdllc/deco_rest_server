var request = require('request');

var httpRequest = function (url, jsonObj, callback) {

    /* stringify json */
    jsonObj = JSON.stringify(jsonObj);

    jsonObj = encodeURIComponent(jsonObj);

    jsonObj = "data=" + jsonObj;

    request.post({
        headers: {'content-type' : 'application/x-www-form-urlencoded'},
        url: url,
        body: jsonObj
    }, function(error, response, body){
        callback(error, response, body);
    });

};


/* PUBLIC API */
module.exports.httpRequest = httpRequest;