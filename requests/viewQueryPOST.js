var http = require('http');
var config = require('../config.json');

var post = function (data, callback) {

    var postData = JSON.stringify(data);

    var options = {
        hostname: config.couchbaseIP,
        port: 8092,
        path: '/deco_db/_design/lookUps/_view/nonListQuestionAnswers',
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Content-Length': postData.length
        }
    };

    var str = "";

    var req = http.request(options, (res) => {
        //console.log(`STATUS: ${res.statusCode}`);
        //console.log(`HEADERS: ${JSON.stringify(res.headers)}`);
        res.setEncoding('utf8');
        res.on('data', (chunk) => {
            //console.log(`BODY: ${chunk}`);
            str += `${chunk}`;
        });
        res.on('end', () => {
            //console.log('No more data in response.');
            var data = JSON.parse(str);
            callback(null, data.rows);
        })
    });

    req.on('error', (e) => {
        callback(e, null);
        //console.log(`problem with request: ${e.message}`);
    });

    // write data to request body
    req.write(postData);
    req.end();
};


/* PUBLIC API */
module.exports = {
    post: post
};