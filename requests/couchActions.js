var db = require('./db.js');
var modifyN1ql = require('../helpers/modifyN1ql.js');
var userPermissions = [];

var setUserPermissions = function (permissions) {
    userPermissions = permissions;
};

/* determines if request operation is authorized */
function canIAccess (type, crud, client_ids) {

    for (var i = 0; i < crud.length; i++) {

        var currentCrud = crud[i];
        var currentPermissionSet = userPermissions[currentCrud].permissions;

        var typeGeneralMatchPermissionResult = currentPermissionSet.some(function(perm) {
            if (typeof perm.client_id === "undefined") {
                if (perm.type === type) {
                    return true;
                }
            }
            return false;
        });

        if (typeGeneralMatchPermissionResult || !client_ids) {
            ///PERMISSION GRANTED
            console.log('permission granted bc of general role')
        } else {
            var typeClientMatchPermissionResult = currentPermissionSet.some(function(perm) {
                if (typeof perm.client_id !== "undefined") {
                    if (perm.type === type) {
                        if (!client_ids) {
                            return true;
                        } else if (client_ids.indexOf(perm.client_id) > -1) {
                            return true;
                        } else if (client_ids.length == 0) {
                            return true;
                        }
                    }
                }
                return false;
            });
            if (typeClientMatchPermissionResult) {
                //PERMISSION GRANTED
                console.log('permission granted bc of client role')
            } else {
                //PERMISSION NOT GRANTED
                console.log('PERMISSION DENIED!!!!!!!');
                return false;
            }
        }
    }
    return true; //permission granted
}

/* Goes through all results to determine which docs a user can see */
function limitResults (res) {

    for (var i = 0; i < res.length; i++) {

        var currentResult = res[i];

        // NO SUPPORT FOR THINGS LIKE LIMIT OR OFFSET
        if (!canIAccess(currentResult.value.type, ["read"], currentResult.value.client_ids)) {
            res.splice(i, 1);
            i--; //subtract one since your looping on the array your modifying
        }
    }
    return res;
}

/* Get a doc */
var getDoc = function (id, callback) {

    db.getDoc(id, function(err, res) {

        // if getDoc fails
        if (err) {
            callback(err, null);
            return;
        }

        // if user has access
        if (canIAccess(res.value.type, ["read"], res.value.client_ids)) {
            callback(err, res);
        }
        // if user does not have access
        else {
            callback({err:err, code:"CA1"}, null);
        }
    });
};

/* Create a doc */
var createDoc = function (id, data, callback) {

    // if user has access
    if (canIAccess(data.type, ["create"], data.client_ids)) {
        db.createDoc(id, data, callback);
    }
    // if user does not have access
    else {
        callback({err:null, code:"CA2"}, null);
    }
};

/* Update a doc */
var updateDoc = function(id, data, callback) {

    // if user has access
    if (canIAccess(data.type, ["update"], data.client_ids)) {
        db.updateDoc(id, data, callback);
    }
    // if user does not have access
    else {
        callback({err:null, code:"CA3"}, null);
    }
};

/* Upsert a doc */
var upsertDoc = function(id, data, callback) {

    // if user has access
    if (canIAccess(data.type, ["create","update"], data.client_ids)) {
        db.upsertDoc(id, data, callback);
    }
    // if user does not have access
    else {
        callback({err:null, code:"CA4"}, null);
    }
};



/* Delete a doc */
var deleteDoc = function(id, callback) {

    db.getDoc(id, function(err, res) {

        // if getDoc fails
        if (err) {
            callback(err, null);
            return;
        }

        // if user has access
        if (canIAccess(res.value.type, ["delete"], res.value.client_ids)) {
            db.deleteDoc(id, callback);
        }
        // if user does not have access
        else {
            callback({err:err, code:"CA5"}, null);
        }
    });
};

/* The view query to the database */
var viewQuery = function (query, callback) {

    db.viewQuery(query, function (err, res) {

        // if viewQuery fails
        if (err) {
            callback(err, null);
            return;
        }

        res.value = limitResults(res);

        callback(err, res);
    });
};

/* The n1ql query to the database */
function runN1qlRequest (query, callback) {

    db.n1qlRequest(query, function (err, res) {

        // if n1qlRequest fails
        if (err) {
            callback(err, null);
            return;
        }

        //consider displaying information such as how many documents were updated or deleted

        callback(err, res);
    });
}

/* Determines how to build n1ql query to database */
var n1qlRequest = function  (query, callback) {

    var newQuery = modifyN1ql.getAuthEngineQuery(query, userPermissions);
    runN1qlRequest(newQuery, callback);
};

var n1qlRequestAgg = function  (query, aggregate, callback) {

    var newQuery = modifyN1ql.getAuthEngineAggregateQuery(query, aggregate, userPermissions);
    runN1qlRequest(newQuery, callback);
};

var n1qlRequestCla = function  (query, clauses, callback) {

    var newQuery = modifyN1ql.getAuthEngineQuery(query, userPermissions);
    newQuery = modifyN1ql.addClausesToQuery(newQuery, clauses);
    runN1qlRequest(newQuery, callback);
};

var n1qlRequestAggCla = function  (query, aggregate, clauses, callback) {

    var newQuery = modifyN1ql.getAuthEngineAggregateQuery(query, aggregate, userPermissions);
    newQuery = modifyN1ql.addClausesToQuery(newQuery, clauses);
    runN1qlRequest(newQuery, callback);
};




/* PUBLIC API */

module.exports = {
    getDoc: getDoc,
    createDoc: createDoc,
    updateDoc: updateDoc,
    upsertDoc: upsertDoc,
    deleteDoc: deleteDoc,
    viewQuery: viewQuery,
    ViewQueryTemp: db.ViewQueryTemp,
    n1qlRequest: n1qlRequest,
    n1qlRequestAgg: n1qlRequestAgg,
    n1qlRequestCla: n1qlRequestCla,
    n1qlRequestAggCla: n1qlRequestAggCla,
    setUserPermissions: setUserPermissions
};