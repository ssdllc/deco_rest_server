var config = require('../config.json');
var N1qlQuery = require('couchbase').N1qlQuery;
var couchbase = require("couchbase");
var ViewQueryTemp = couchbase.ViewQuery;

/* Reference to Couchbase Server */
dc_cluster = new couchbase.Cluster(config.couchbaseIP+':8091');


/* function that opens the bucket and talks to the database */
function dataAccess (type, callback, queryOrId, data) {

    var deco_db = dc_cluster.openBucket('deco_db', function(err) {
        if (err)
            callback(err);


        if (data) {
            deco_db[type](queryOrId, data, function(err, res) {
                callback(err, res);
            });
        } else {
            deco_db[type](queryOrId, function(err, res) {
                callback(err, res);
            });
        }
    });
}

/* Get a doc */
var getDoc = function (id, callback) {
    console.log('getDoc: ' + id);
    dataAccess('get', callback, id);
};

/* Create a doc */
var createDoc = function (id, data, callback) {
    dataAccess('insert', callback, id, data);
};

/* Update a doc */
var updateDoc = function(id, data, callback) {
    dataAccess('replace', callback, id, data);
};

/* Upsert a doc */
var upsertDoc = function(id, data, callback) {
    dataAccess('upsert', callback, id, data);
};

/* Delete a doc */
var deleteDoc = function(id, callback) {
    console.log('deleteDoc: ' + id);
    dataAccess('remove', callback, id);
};

/* View query to the database */
var viewQuery = function (query, callback) {
    console.log('viewQuery: ' + query);
    dataAccess('query', callback, query);
};

/* N1ql query to the database */
var n1qlRequest = function  (query, callback) {
    // .consistency(N1qlQuery.Consistency.REQUEST_PLUS); <-- this is what makes stale = false
    console.log('n1qlRequest: ' + query);

    query = N1qlQuery.fromString(query).consistency(N1qlQuery.Consistency.REQUEST_PLUS);

    dataAccess('query', callback, query);
};


/* PUBLIC API */
module.exports = {
    getDoc: getDoc,
    createDoc: createDoc,
    updateDoc: updateDoc,
    upsertDoc: upsertDoc,
    deleteDoc: deleteDoc,
    viewQuery: viewQuery,
    ViewQueryTemp: ViewQueryTemp,
    n1qlRequest: n1qlRequest
};