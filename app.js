/* Initialize global variables and functions */
require('./init.js');

var config = require('./config.json');
var createDoc = require('./crud/create/create.js');
var deleteDoc = require('./crud/delete.js');
var readDoc = require('./crud/read.js');
var updateDoc = require('./crud/update.js');
var response = require('./helpers/response.js');
var importDocs = require('./tasks/import.js');
var screeningSummary = require('./tasks/screeningSummary.js');
var incomeAssetReport = require('./tasks/incomeAssetReport.js');
var makeCaseFromScreening = require('./tasks/makeCaseFromScreening.js');
var dataPromotion = require('./tasks/dataPromotion.js');
var xmlImport = require('./endPoints/BonSecours/xmlImport.js');
const csvImport = require('./endPoints/csvImport.js');
var login = require('./endPoints/login.js');
var applicationTemplates = require('./endPoints/applicationTemplates.js');
var personReview = require('./endPoints/personReview.js');
var programReview = require('./endPoints/programReview.js');
var manageEligibility = require('./endPoints/manageEligibility.js');
var answerIds = require('./tasks/answerIds.js');
var modifyN1ql = require('./helpers/modifyN1ql.js');
var multer = require('multer');
var express = require('express');
var bodyParser = require('body-parser');
var session = require('express-session');
var CouchbaseStore = require('connect-couchbase')(session);
var moment = require('moment');

var couchbaseStore = new CouchbaseStore({
    bucket:"deco_db",
    host: config.couchbaseIP + ":8091",
    connectionTimeout: 4000,
    operationTimeout: 4000,
    cachefile: '',
    ttl: 86400,
    prefix: 'sess'
});

couchbaseStore.on('connect', function() {
    console.log("Couchbase Session store is ready for use");
});


couchbaseStore.on('disconnect', function() {
    console.log("An error occurred connecting to Couchbase Session Storage");
    //hopefully pm2 restarts me after this fails
    process.exit();
});

var app = express();

app.use(bodyParser.urlencoded({ extended: false }));

// if nginxIP exists and https is true
// then set trust proxy to nginxIP
if (config.nginxIP && config.https) {
    app.set('trust proxy', function (ip) {
        if (ip === config.nginxIP) return true; // trusted IP
        else return false;
    });
}

/* used for session management */
app.use(session({
    store: couchbaseStore,
    name: "Cogent-Session",
    secret: config.sessionSecret,
    cookie: {
        httpOnly: true,
        secure: config.https,
        expires: false, //cookie will remain for the duration of the user-agent
        maxAge: 20*60*1000 //stay alive for 20 minutes
    },
    resave: false, //false because i think couchbase implements the touch method
    saveUninitialized: true //not sure on this one, might be false because it is a login session, might save after i save the user_id to session?
}));

/* Sets the header of the response */
function setHeaders(req, res) {

    var allowOrigin = config.AccessControlAllowOrigin;

    if (allowOrigin.active === true) {
        var origins = allowOrigin.ips;
        for(var i=0;i<origins.length;i++){
            var origin = origins[i];
            if(req.headers.origin.indexOf(origin) > -1) {
                res.setHeader('Access-Control-Allow-Origin', req.headers.origin);
                break;
            }
            //else, tough cookies.
        }
    }
    res.setHeader("Access-Control-Allow-Credentials","true");
}

/* This functions makes sure that the request is valid and sets header */
function isValidRequest(req,res) {

    if(!req.body.hasOwnProperty('data')) {
        res.statusCode = 400;
        res.send('Error 400: Post syntax incorrect.');
        return false;
    } else {
        setHeaders(req,res);
        return true;
    }
}

function authenticateAuthorizeAndValidateRequest (req, res, callback) {

    console.log(req.originalUrl + " -- request received -- " + tools.timeStamp());
    setHeaders(req,res);

    var endPoint = req.originalUrl.substr(1);
    var resObj = { res: res, responded: false };

    login.loggedIn2(req, function (err, bool) {

        if (err) {
            resObj = response.respond(resObj, err);
            return;
        }


        if (bool) {

            // user has access to end point
            if (login.checkUsersEndPointAccess(endPoint)) {
                callback(req, resObj); // <-- i dont like that. maybe just use res, maybe make global
            }
            // user does not have access to end point
            else {
                resObj = response.respond(resObj, {err:"User does not have access to this api", code:"A1"});
            }
        }
        else
            resObj = response.respond(resObj, {"Error":"Session expired. Please log in again."});
    });
}


/* REST ENDPOINTS */
app.post('/create', function(req, res) {
    console.log("create Request Recieved: "+tools.timeStamp());
    if (isValidRequest(req,res)) {

        var resObj = { res: res, responded: false };

        login.loggedIn(req, function (bool) {

            if (bool)
                createDoc.getCreate(resObj,req);
            else
                resObj = response.respond(resObj, {"Error":"Session expired. Please log in again."});
        });
    }
});
app.post('/read', function(req, res) {
    console.log("read Request Recieved: "+tools.timeStamp());
    if (isValidRequest(req,res)) {

        var resObj = { res: res, responded: false };

        login.loggedIn(req, function (bool) {

            if (bool)
                readDoc.getRead(resObj,req);
            else
                resObj = response.respond(resObj, {"Error":"Session expired. Please log in again."});
        });
    }
});
app.post('/update', function(req, res) {
    console.log("update Request Recieved: "+tools.timeStamp());
    if (isValidRequest(req,res)) {

        var resObj = { res: res, responded: false };

        login.loggedIn(req, function (bool) {

            if (bool)
                updateDoc.getUpdate(resObj,req);
            else
                resObj = response.respond(resObj, {"Error":"Session expired. Please log in again."});
        });
    }
});
app.post('/delete', function(req, res) {
    console.log("delete Request Recieved: "+tools.timeStamp());
    if (isValidRequest(req,res)) {

        var resObj = { res: res, responded: false };

        login.loggedIn(req, function (bool) {

            if (bool)
                deleteDoc.getDelete(resObj,req);
            else
                resObj = response.respond(resObj, {"Error":"Session expired. Please log in again."});
        });
    }
});

app.post('/personReview', function(req, res) {
    authenticateAuthorizeAndValidateRequest(req, res, function (req, resObj) {
        personReview.getReview(req, resObj);
    });
});

app.post('/programReview', function(req, res) {
    authenticateAuthorizeAndValidateRequest(req, res, function (req, resObj) {
        programReview.getReview(req, resObj);
    });
});

app.post('/manageEligibility', function(req, res) {
    authenticateAuthorizeAndValidateRequest(req, res, function (req, resObj) {
        manageEligibility.getEligibility(req, resObj);
    });
});

app.post('/updatePersonClients', function(req, res) {
    authenticateAuthorizeAndValidateRequest(req, res, function (req, resObj) {
        updateDoc.updatePersonClients(req, resObj);
    });
});

app.post('/applicationTemplateUpload', function (req, res) {
    console.log("applicationTemplateUpload Request Recieved: "+tools.timeStamp());
    setHeaders(req,res);

    var resObj = { res: res, responded: false };
    var _prefix = moment().format('YYYYMMDD-HHmmss');

    var storage = multer.diskStorage({
        destination: function (req, file, cb) {
            cb(null, config.pathToParentFolder + 'deco_pdfs/template_pdfs/')
        },
        filename: function (req, file, cb) {
            cb(null, _prefix + '-' + file.originalname.replace(/\s+/g, ''));
        }
    });

    var upload = multer({ storage: storage }).single('0');


    login.loggedIn(req, function (bool) { // name='0'

        if (bool) {
            upload(req, res, function (err) {
                if (err) {
                    console.log(err);
                    response.respond(resObj, "upload failed!", true, true);
                    return;
                }
                console.log('originalName: ' + resObj.res.req.file.originalname);
                console.log('filename: ' + resObj.res.req.file.filename);

                applicationTemplates.uploadPDF(resObj.res.req, resObj);
            });
        }
        else
            resObj = response.respond(resObj, {"Error":"Session expired. Please log in again."});
    });
});

app.get('/downloadPDF', function(req, res) {

    console.log("downloadPDF Request Recieved: "+tools.timeStamp());

    var resObj = { res: res, responded: false };

    login.loggedIn(req, function (bool) {

        if (bool) {

            applicationTemplates.fillPDF(req.query.person_id, req.query.applicationTemplateFilename, req, resObj);
        }
        else
            resObj = response.respond(resObj, {"Error":"Session expired. Please log in again."});
    });
});

// DATA PROMOTION
app.get('/exportRoleDefinitions', function(req, res) {

    var roleDefinitionIds = [
        "0c684970-024c-11e6-bcd7-23140172a65e",
        "22e8be60-024b-11e6-bcd7-23140172a65e",
        "31f4ac30-4854-11e6-b7f1-dd2182fd1b2e",
        "676f79f0-0653-11e6-bcd7-23140172a65e",
        "99ca1790-df0f-11e5-84f0-932a74faa389",
        "c781df40-0b08-11e6-9a82-69a7cedb3f5f",
        "dddf6d70-0b08-11e6-9a82-69a7cedb3f5f",
        "e1435f80-0653-11e6-bcd7-23140172a65e",
        "ee833940-0b08-11e6-9a82-69a7cedb3f5f"
    ];

    dataPromotion.roleDefinitions(roleDefinitionIds);
    res.send('done');
});

app.get('/exportPrograms', function(req, res) {

    var programIds = ["9ff4ef90-21dc-11e6-a86b-d31fe63a2ac5","180f7fc0-22b6-11e6-86ab-21e70956171c","02b59e70-21d0-11e6-b852-9581fab01f31","1e540170-2370-11e6-b4a5-437dca0defcd","35bd5ea0-21e1-11e6-a86b-d31fe63a2ac5","72505610-20f6-11e6-bdf0-eb8fb132b7e2","5a87deb0-1eb0-11e6-bcc1-195b15c05e4c","fe66b080-21ec-11e6-a86b-d31fe63a2ac5","056cf440-21e6-11e6-a86b-d31fe63a2ac5","7c570350-21c5-11e6-b852-9581fab01f31","ba46c240-20f3-11e6-bdf0-eb8fb132b7e2","321b36f0-22b8-11e6-86ab-21e70956171c","920ee510-2359-11e6-b4a5-437dca0defcd","2c37fab0-2354-11e6-86ab-21e70956171c","7d806a30-235c-11e6-b4a5-437dca0defcd","efc14ff0-21f0-11e6-a86b-d31fe63a2ac5","145ea000-22a6-11e6-a86b-d31fe63a2ac5","8a2a8b90-21da-11e6-a86b-d31fe63a2ac5","6d88f2a0-241a-11e6-8248-354316229eca","e7607620-21b3-11e6-b852-9581fab01f31","3f4a6cc0-21b8-11e6-b852-9581fab01f31","192715b0-21c3-11e6-b852-9581fab01f31","2955f0e0-21c9-11e6-b852-9581fab01f31","a8223e80-1eb1-11e6-bcc1-195b15c05e4c","0adf5e60-1eba-11e6-bcc1-195b15c05e4c","b5669620-21e7-11e6-a86b-d31fe63a2ac5","6dae7de0-1957-11e6-9c5b-550071c70c48","fa82b310-1c5b-11e6-9c5b-550071c70c48","1e587e70-1e03-11e6-bcc1-195b15c05e4c","be7c09e0-1eb6-11e6-bcc1-195b15c05e4c","5fe54150-2373-11e6-b4a5-437dca0defcd","169b9660-2414-11e6-b4a5-437dca0defcd","e7f2ee80-2bed-11e6-8248-354316229eca"];

    dataPromotion.programs(programIds);
    res.send('done');
});

app.get('/exportWizards', function(req, res) {

    var wizardIds = ["c2886790-2da2-11e6-8d8d-e966d0858db4"];

    dataPromotion.wizards(wizardIds);
    res.send('done');
});

app.get('/exportClients', function(req, res) {

    var clientIds = [
        'f8c4fad0-9a0d-11e6-a1d8-03280de3497d',
        '52560e90-9a0e-11e6-a1d8-03280de3497d',
        'f307c090-9a0e-11e6-a1d8-03280de3497d',
        '9db0fb20-9a0e-11e6-a1d8-03280de3497d',
        '35cfb310-9a0f-11e6-a1d8-03280de3497d',
        '6e9934a0-9a0f-11e6-a1d8-03280de3497d',
        '5ab25650-9a10-11e6-a1d8-03280de3497d',
        '8729da00-9a10-11e6-a1d8-03280de3497d',
        'aa4ee430-9a10-11e6-a1d8-03280de3497d',
        'dd2f5600-9a10-11e6-a1d8-03280de3497d',
        '89ff0c90-96fb-11e6-a1d8-03280de3497d',
        'c095fd60-5fea-11e6-8ffe-55ac2df275f2',
        '4d631740-9a11-11e6-a1d8-03280de3497d',
        '74903c30-9a11-11e6-a1d8-03280de3497d',
        'b65e22e0-6ae0-11e6-8d2b-ffa989ded9b5',
        '9bf98b60-6ae0-11e6-8d2b-ffa989ded9b5'
    ];

    dataPromotion.clients(clientIds);
    res.send('done');
});

app.get('/exportListsAndBaselines', function(req, res) {

    dataPromotion.listsAndBaselines();
    res.send('done');
});

app.get('/exportBaselineImport', function(req, res) {
    dataPromotion.baselineImport();
    res.send('done');
});

// http://10.1.10.242:3414/promoteData/2016-02-11T10:46:24-05:00?files[]=questions&files[]=programs&files[]=eligibility&files[]=incomeAssets&files[]=roleDefinitions&files[]=permissionSets&files[]=baselineImport
// http://10.1.10.242:3414/promoteData/2016-02-11T10:55:38-05:00?files[]=questions&files[]=wizards
app.get('/promoteData/:consistentDate', function(req, res) {
    dataPromotion.import(req.params.consistentDate, req.query.files);
    res.send('done');
});



app.get('/import', function(req, res) {
    importDocs.getImport();
    res.send('done');
});

app.get('/screeningSummary', function(req, res) {
    console.log("screeningSummary Request Recieved: "+tools.timeStamp());
    screeningSummary.runScreeningSummary();
    res.send('done');
});

app.get('/incomeAssetReport', function(req, res) {
    incomeAssetReport.getReport();
    res.send('done');
});

app.post('/xmlUpload', function (req, res) {
    console.log("xmlUpload Request Recieved: "+tools.timeStamp());
    setHeaders(req,res);

    var resObj = { res: res, responded: false };
    var upload = multer({dest: config.pathToParentFolder+'deco_uploads/'}).single('0');

    login.loggedIn(req, function (bool) { // name='0'

        if (bool) {
            upload(req, res, function (err) {
                if (err) {
                    response.respond(resObj, "upload failed!", true, true);
                    return;
                }

                console.log('file received, processing now...');
                //is upload successful if it uploads nothing? if so will need error res here

                if (resObj.res.req.file.originalname.endsWith(".csv")) {
                    csvImport.parseCSV(resObj.res.req.file.originalname,
                        resObj.res.req.file.filename,
                        resObj
                    );
                } else {
                    xmlImport.xml2js(resObj.res.req.file.originalname,
                        resObj.res.req.file.filename,
                        resObj
                    );
                }
            });
        }
        else
            resObj = response.respond(resObj, {"Error":"Session expired. Please log in again."});
    });
});

app.post('/login', function(req, res) {
    console.log("login Request Recieved: "+tools.timeStamp());
    setHeaders(req,res);

    var resObj = { res: res, responded: false };
    var data = JSON.parse(req.body.data);

    login.login(data, function (err, res, userDoc) {

        // if res is true and I receive a userDoc
        // then you are logged in
        if (res && userDoc) {

            login.getUserPermissions(userDoc.id, function (err, res) {

                if (err) {
                    resObj = response.respond(resObj, {err:err, code:"A2"});
                    return;
                }

                req.session.loginTime = moment();
                req.session.lastRequestTime = moment();
                req.session.user_id = userDoc.id;
                req.session.client_id = "";
                req.session.person_id = "";
                req.session.userPermissions = modifyN1ql.createUserWhereClauses(res);
                req.session.userPermissionsClient = {};

                login.setLongerSessionTimeout(req, userDoc.endPoints);

                // pass userPermissions to couchActions for authorization purposes
                login.giveAuthEngineUserPermissions(req.session.userPermissions);

                resObj = response.respond(resObj, {user: userDoc});
            });
        } else resObj = response.respond(resObj, err);
    });
});

app.post('/logout', function(req, res) {
    console.log("logout Request Recieved: "+tools.timeStamp());
    setHeaders(req,res);
    login.logout(req, res);
});

app.get('/updateAnswers', function(req, res) {
    answerIds.updateAnswerIDs();
    res.send('done');
});

app.get('/updateEquals', function(req, res) {
    answerIds.updateEqual();
    res.send('done');
});

//app.get('/makeCaseFromScreening', function(req, res) {
//    makeCaseFromScreening.makeCaseFromScreening();
//    res.send('done');
//});

/* Launch Express Server on port 3414 */
app.listen(process.env.PORT || 3414);
