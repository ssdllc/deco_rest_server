var uuid = require('node-uuid');
var db = require('../../requests/db.js');
var couchActions = require('../../requests/couchActions.js');
var response = require('../../helpers/response.js');
var queries = require('./queries.js');
var moment = require('moment');
var bcrypt = require('bcrypt-nodejs');

/* Create Documents */
function createDoc(newID, data, resObj, callback) {

    /* create Doc with newID */
    couchActions.createDoc(newID, data, function(err, res) {

        if (err) {
            resObj = response.respond(resObj, {err:err, code:"C1"});
            return;
        }
        console.log("Document created "+tools.timeStamp());
        callback();
    });

}

function createUser (bool, newID, data, hash, resObj) {

    // if bool = true, then email is unique and u can create him
    if (bool) {

        /* create userDoc */
        createDoc(newID, data, resObj, function () {

            var user_id = newID;
            var userCredentialsDoc = {"type":"userCredentials", "hashed_password":hash, "user_id":user_id};
            newID = uuid.v1();

            /* create passwordDoc */
            createDoc(newID, userCredentialsDoc, resObj, function () {

                var roleAssignmentDoc = {
                    type: "roleAssignment",
                    clientRoles: [],
                    generalRoles: [],
                    user_id: user_id
                };
                newID = uuid.v1();

                createDoc(newID, roleAssignmentDoc, resObj, function () {
                    resObj = response.respond(resObj, {"id": user_id});
                });
            });
        });
    } else
        resObj = response.respond(resObj, {"error": "Email is not unique"});
}

function createPerson (res, newID, data, resObj) {

    // if person does not exist
    if (res.length < 1) {

        createDoc(newID, data, resObj, function () {

            createHouseholdMemberSelf(newID, data.client_ids, resObj, function() {
                resObj = response.respond(resObj, {"id": newID});
            });
        });

    } else
        resObj = response.respond(resObj,
            {
                "person_id": res[0].id,
                "nameFirst": res[0].nameFirst,
                "nameLast": res[0].nameLast,
                "dateOfBirth": res[0].dateOfBirth,
                "address": res[0].address[0],
                "message": "SSN already exists"
            }
        );
}

function isEmailUnique (email, resObj, callback) {

    couchActions.n1qlRequest(queries.emailQuery(email), function (err, res) {

        if (err) {
            resObj = response.respond(resObj, {err:err, code:"C2"});
            return;
        }

        // if there are any results with this email then it is not unique
        if (res.length > 0)
            callback(false);
        else
            callback(true);
    });
}

function selectPersonBySsn (ssn, callback){

    db.n1qlRequest(queries.ssnQuery(ssn), function(err, res) {
        callback(err, res);
    });
}

var createHouseholdMemberSelf = function (id, client_ids, resObj, callback) {

    var query = "SELECT META(x).id AS id FROM deco_db x WHERE x.type = 'list' AND " +
        "x.listName = 'relationshipType' AND x.`value` = 'self'";

    couchActions.n1qlRequest(query, function(err, res) {

        if (err) {
            resObj = response.respond(resObj, {err:err, code:"C3"});
            return;
        }

        var listName = res;


        var householdMember = {
            "nameFirst": "",
            "nameLast": "",
            "dateOfBirth": "",
            "relationshipType_id": listName[0].id,
            "person_id": id,
            "client_ids": client_ids,
            "type": "householdMember",
            "channels": ["ssd"]
        };

        var newID = uuid.v1();

        createDoc(newID, householdMember, resObj, function (err, res) {

            if (err) {
                console.log("FAILED TO CREATE HOUSEHOLD MEMBER");
            }

            callback();
        });
    });
};

/* set up objects before pushing to database */
function createSetUp(data, resObj, req) {

    /* new id */
    var newID = uuid.v1();

    /* all docs need a date attr */
    data["date_created"] = moment().format();
    data["date_updated"] = moment().format();



    if (data.type === "person") {

        selectPersonBySsn(data.ssn, function (err, res) {

            if (err) {
                resObj = response.respond(resObj, {err:err, code:"C4"});
                return;
            }

            createPerson(res, newID, data, resObj);
        });

    } else if (data.type === "householdMember") {

        couchActions.getDoc(data.person_id, function (err, res) {

            if (err) {
                resObj = response.respond(resObj, {err:err, code:"C5"});
                return;
            }

            data["client_ids"] = res.value.client_ids;

            createDoc(newID, data, resObj, function () {
                resObj = response.respond(resObj, {"id": newID});
            });
        });

    } else if (data.type === "asset" || data.type === "income") {

        couchActions.getDoc(data.householdMember_id, function (err, res) {

            if (err) {
                resObj = response.respond(resObj, {err:err, code:"C6"});
                return;
            }

            data["client_ids"] = res.value.client_ids;

            createDoc(newID, data, resObj, function () {
                resObj = response.respond(resObj, {"id": newID});
            });
        });
    }
    else if (data.type === "screening") {

        data["user_id"] = req.session.user_id;

        createDoc(newID, data, resObj, function () {
            resObj = response.respond(resObj, {"id": newID});
        });

    } else if (data.type === "program") {

        if (data.stateDisplay)
            delete data.stateDisplay;

        createDoc(newID, data, resObj, function () {
            resObj = response.respond(resObj, {"id": newID});
        });

    } else if (data.type === "user") {

        if (data.password) {

            bcrypt.hash(data.password, null, null, function (err, hash) {

                if (err) {
                    console.log("bcrypt.hash: " + err);
                }

                /* set password attr in userDoc to null */
                data.password = "";

                isEmailUnique(data.email, resObj, function (bool) {

                    createUser(bool, newID, data, hash, resObj);
                });
            });
        } else {
            resObj = response.respond(resObj, "Password field needs to be filled out in order to create a user", true, true);
        }
    } else if (data.type === "client") {

        data["client_ids"] = [];
        data.client_ids.push(newID);

        createDoc(newID, data, resObj, function () {
            resObj = response.respond(resObj, {"id": newID});
        });
    } else {
        createDoc(newID, data, resObj, function () {
            resObj = response.respond(resObj, {"id": newID});
        });
    }

}

/* Local Create */
var lc_create = function(resObj,req) {
    var reqObj = JSON.parse(req.body.data);
    var newVals = (reqObj.data) ? reqObj.data[0] : {};

    if (typeof newVals.client_ids === "undefined") {
        newVals.client_ids = [];
    }

    newVals.channels = ["ssd"];
    delete newVals.id;

    createSetUp(newVals, resObj, req);
};


/* PUBLIC API */
module.exports.getCreate = lc_create;
module.exports.createHouseholdMemberSelf = createHouseholdMemberSelf;