
var emailQuery = function (email) {

    return "SELECT META(x).id AS id, x.* " +
        "FROM deco_db x " +
        "WHERE x.type = 'user' " +
        "AND x.email = '" + email + "'";
};

var ssnQuery = function (ssn) {

    return "SELECT META(x).id AS id, x.* " +
        "FROM deco_db x " +
        "WHERE x.type = 'person' " +
        "AND x.ssn = '" + ssn + "' " +
        "AND x.ssn <> ''";
};


/* PUBLIC API */
module.exports = {
    emailQuery: emailQuery,
    ssnQuery: ssnQuery
};