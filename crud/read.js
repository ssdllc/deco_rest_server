var couchActions = require('../requests/couchActions.js');
var db = require('../requests/db.js');
var simpleN1qlRequest = require('../requests/simpleN1qlRequest.js');
var eligProList = require('../endPoints/screening/eligibilityProgramsLists.js');
var question = require('../endPoints/screening/questionsCollectionRequest.js');
var applicationTemplates = require('../endPoints/applicationTemplates.js');
var xmlImport = require('../endPoints/BonSecours/xmlImport.js');
var datatables = require('../endPoints/datatables.js');
var login = require('../endPoints/login.js');
var screening = require('../endPoints/screening/screening.js');
var conditionDoc = require('../endPoints/screening/conditionDoc.js');
var personDocFunc = require('../helpers/personDocFunc.js');
var response = require('../helpers/response.js');
var screeningSummary = require('../tasks/screeningSummary.js');


//LOOP ON QUERY RESULTS (KEY, VALUE)
//BUILD A NEW ARRAY OF ONLY THE VALUES (Documents)
function getValues(qResults) {
    var retValues = [];
    for (var i=0; i < qResults.length; i++) {
        var rec = qResults[i];
        var newRec = rec.value.value[1];
        newRec["id"] = rec.value.value[0];
        retValues.push(newRec);
    }
    return retValues;
}

var lc_read = function(resObj,req) {
    console.log("Enter local read "+tools.timeStamp());
    var reqObj = JSON.parse(req.body.data);

    switch(reqObj.target) {
        case "importedXMLDocsDatatable":

            var fieldsToSearch = ["filename.originalName","y.nameFirst || ' ' || y.nameLast","z.display"];
            var whereClause = "x.type = 'xmlUploadHistory'";

            datatables.buildDatatable(reqObj, resObj, fieldsToSearch, whereClause);

            break;
        case "XMLDebugDatatable":

            var fieldsToSearch = ["message","y.nameFirst || ' ' || y.nameLast"];
            var whereClause = "x.type = 'debug'";

            datatables.buildDatatable(reqObj, resObj, fieldsToSearch, whereClause);

            break;
        case "tempPersonsDatatable":

            var fieldsToSearch = ["nameFirst","nameLast","ssn"];
            var whereClause = "x.type = 'tempPerson'";

            datatables.buildDatatable(reqObj, resObj, fieldsToSearch, whereClause);

            break;
        case "tempScreeningsDatatable":

            var fieldsToSearch = ["medicalRecordNumber","accountNumber"];
            var whereClause = "x.type = 'tempScreening'";

            datatables.buildDatatable(reqObj, resObj, fieldsToSearch, whereClause);

            break;
        case "personsDatatable":

            /* Initial variables specific to this datatable */
            //var fieldsToSearch = ["nameFirst","nameLast","ssn","dateOfBirth","email[0].emailAddress","phone[0].phoneNumber"];
            var fieldsToSearch = [];
            var whereClause = "per.type = 'person'";
            var datatable = { target : "personsDatatable" };

            if (!reqObj.data[0].ssn == "")
                datatable["ssn"] = reqObj.data[0].ssn;
            if (!reqObj.data[0].nameFirst == "")
                datatable["nameFirst"] = reqObj.data[0].nameFirst;
            if (!reqObj.data[0].nameLast == "")
                datatable["nameLast"] = reqObj.data[0].nameLast;
            if (!reqObj.data[0].dateOfBirth == "")
                datatable["dateOfBirth"] = reqObj.data[0].dateOfBirth;

            if (reqObj.data[0].client_id)
                whereClause += " AND '" + reqObj.data[0].client_id + "' IN per.client_ids";

            /*
             IF SSN.length > 8 THEN
               exact match on SSN QUERY ****NEEDS DB

             ELSE IF DOB.length > 0 AND nameLast.length > 0
               exact match on nameLast AND DOB ****NEEDS DB

             ELSE
               do normal query ***couchActions
             */


            if (reqObj.data[0].ssn.length > 8) {
                whereClause += " AND per.ssn = '"+datatable.ssn+"'";
                datatables.buildExactMatchPersonsDatatable(reqObj, resObj, fieldsToSearch, whereClause);
            }
            else if (reqObj.data[0].dateOfBirth.length > 0 && reqObj.data[0].nameLast.length > 0) {
                whereClause += " AND per.dateOfBirth = '"+datatable.dateOfBirth+"' AND  LOWER(per.nameLast) = LOWER('"+datatable.nameLast+"')";
                datatables.buildExactMatchPersonsDatatable(reqObj, resObj, fieldsToSearch, whereClause);
            }
            else
                datatables.buildDatatable(reqObj, resObj, fieldsToSearch, whereClause, datatable);

            break;
        case "usersDatatable":

            /* Initial variables specific to this datatable */
            var fieldsToSearch = ["nameFirst","nameLast","email"];
            var whereClause = "x.type = 'user'";

            datatables.buildDatatable(reqObj, resObj, fieldsToSearch, whereClause);

            break;
        case "programsDatatable":

            /* Initial variables specific to this datatable */

            var fieldsToSearch = ["title","description","state.`value`"];
            var whereClause = "x.type = 'program'";

            datatables.buildDatatable(reqObj, resObj, fieldsToSearch, whereClause);

            break;
        case "questionsDatatable":

            /* Initial variables specific to this datatable */
            var fieldsToSearch = ["question"];
            var whereClause = "x.type = 'question'";

            datatables.buildDatatable(reqObj, resObj, fieldsToSearch, whereClause);

            break;
        case "wizardsDatatable":

            /* Initial variables specific to this datatable */
            var fieldsToSearch = ["title","description"];
            var whereClause = "x.type = 'wizard'";

            datatables.buildDatatable(reqObj, resObj, fieldsToSearch, whereClause);

            break;
        case "listsDatatable":

            /* Initial variables specific to this datatable */
            var fieldsToSearch = ["listName"];
            var whereClause = "x.type = 'list'";

            datatables.buildDatatable(reqObj, resObj, fieldsToSearch, whereClause);

            break;
        case "listItemsDatatable":

            /* Initial variables specific to this datatable */
            var fieldsToSearch = ["display","`value`"];
            var whereClause = "x.listName = '" + reqObj.data[0].listName + "'";

            datatables.buildDatatable(reqObj, resObj, fieldsToSearch, whereClause);

            break;
        case "screeningDatatable":

            /* Initial variables specific to this datatable */
            var fieldsToSearch = ["accountNumber","medicalRecordNumber","y.nameFirst || ' ' || y.nameLast"];
            var whereClause = "x.type = 'screening' AND x.person_id = '" + reqObj.data[0].id + "' AND x.client_ids[0] = '" + req.session.client_id + "'";

            datatables.buildDatatable(reqObj, resObj, fieldsToSearch, whereClause);

            break;
        case "applicationsTemplatesDatatable":

            /* Initial variables specific to this datatable */
            var fieldsToSearch = ["fileName","displayName","state.display || ' ' || pro.title"];
            var whereClause = "x.type = 'applicationTemplate'";

            datatables.buildDatatable(reqObj, resObj, fieldsToSearch, whereClause);

            break;
        case "baselineDatatable":

            var fieldsToSearch = ["title","year"];
            var whereClause = "x.type = 'baseline'";

            datatables.buildDatatable(reqObj, resObj, fieldsToSearch, whereClause);

            break;
        case "permissionSetDatatable":

            var fieldsToSearch = ["title"];
            var whereClause = "x.type = 'permissionSet'";

            datatables.buildDatatable(reqObj, resObj, fieldsToSearch, whereClause);

            break;
        case "roleDefinitionDatatable":

            var fieldsToSearch = ["title"];
            var whereClause = "x.type = 'roleDefinition'";

            datatables.buildDatatable(reqObj, resObj, fieldsToSearch, whereClause);

            break;
        case "clientsDatatable":

            var fieldsToSearch = ["name","abbreviation"];
            var whereClause = "x.type = 'client'";
            //var whereClause = "x.type = 'client' AND META(x).id WITHIN (select y.workingClients from deco_db y where y.type = 'roleAssignment' and y.user_id = '" + req.session.user_id + "')";  // + JSON.stringify(req.session);

            datatables.buildDatatable(reqObj, resObj, fieldsToSearch, whereClause);

            break;
        case "chainsDatatable":

            var fieldsToSearch = ["name", "abbreviation"];
            var whereClause = "x.type = 'chain'";

            datatables.buildDatatable(reqObj, resObj, fieldsToSearch, whereClause);

            break;
        case "commitTempXMLData":

            xmlImport.commitTempData(reqObj.data[0].filename, resObj);

            break;
        case "discardTempXMLData":

            xmlImport.discardTempData(reqObj.data[0].filename, resObj);

            break;
        case "highestListItem":

            var query = "SELECT max(x.displayOrder) AS highest, x.listName FROM deco_db x " +
                "WHERE x.type = 'list' AND x.listName = '" + reqObj.data[0].listName + "' GROUP BY x.listName";

            db.n1qlRequest(query, function(err, res) {

                if (err) {
                    resObj = response.respond(resObj, {err:err, code:"R0"});
                    return;
                }

                /* respond to client */
                resObj = response.respond(resObj, {"data":res});
            });

            break;
        case "modelRequest":
            if(reqObj.data) {
                if (reqObj.data.length > 0) {

                    var id = reqObj.data[0].id;

                    /* Get model with specified id */
                    couchActions.getDoc(id, function(err, res) {

                        if (err) {
                            resObj = response.respond(resObj, {err:err, code:"R1"});
                            return;
                        }

                        var doc = res.value;
                        doc["id"] = id; //add id for frontend

                        /* need the workingClients array on the user doc */
                        var query = "SELECT x.workingClients " +
                            "FROM deco_db x " +
                            "WHERE x.type = 'roleAssignment' " +
                            "AND x.user_id = '"+req.session.user_id+"'";
                        db.n1qlRequest(query, function (err, res) {

                            if (err) {
                                resObj = response.respond(resObj, {err:err, code:"R2"});
                                return;
                            }


                            if (doc.client_ids) {
                                doc.client_ids = personDocFunc.removeClientIds(doc.client_ids, res[0].workingClients);
                            }

                            resObj = response.respond(resObj, doc);
                        });
                    });
                }
            }
            break;
        case "personModelRequest":
            if(reqObj.data) {
                if (reqObj.data.length > 0) {

                    var id = reqObj.data[0].id;

                    /* Get model with specified id */
                    db.getDoc(id, function(err, res) {

                        if (err) {
                            resObj = response.respond(resObj, {err:err, code:"R1"});
                            return;
                        }

                        var doc = res.value;
                        doc["id"] = id; //add id for frontend

                        /* need the workingClients array on the user doc */
                        var query = "SELECT x.workingClients " +
                            "FROM deco_db x " +
                            "WHERE x.type = 'roleAssignment' " +
                            "AND x.user_id = '"+req.session.user_id+"'";
                        db.n1qlRequest(query, function (err, res) {

                            if (err) {
                                resObj = response.respond(resObj, {err:err, code:"R2"});
                                return;
                            }


                            if (doc.client_ids) {
                                doc.client_ids = personDocFunc.removeClientIds(doc.client_ids, res[0].workingClients);
                            }

                            resObj = response.respond(resObj, doc);
                        });
                    });
                }
            }
            break;
        case "collectionRequest":

            var ids = reqObj.data[0].ids;

            var query = "SELECT META(x).id, x.* FROM deco_db x WHERE META(x).id IN "
                + JSON.stringify(ids);

            simpleN1qlRequest.setRequest(reqObj, resObj, query);

            break;
        case "questionsCollectionRequest":

            question.questionsCollectionRequest(reqObj, resObj);

            break;
        case "undisplayedQuestions":

            screening.undisplayedQuestions(reqObj, resObj);

            break;
        case "questionsOfProgram":

            var viewQuery = couchActions.ViewQueryTemp.from('lookUps', 'questionsOfPrograms')
                .range(reqObj.data[0].program_id,reqObj.data[0].program_id)
                .stale(couchActions.ViewQueryTemp.Update.BEFORE);

            couchActions.viewQuery(viewQuery, function(err, res) {

                if (err) {
                    resObj = response.respond(resObj, {err:err, code:"R6"});
                    return;
                }

                var results = res;
                var questionIds = [];

                for (var i = 0; i < results.length; i++) {
                    questionIds.push(results[i].value.value);
                }

                var query = "SELECT META(x).id, x.* FROM deco_db x WHERE META(x).id IN "
                    + JSON.stringify(questionIds);

                simpleN1qlRequest.setRequest(reqObj, resObj, query);

            });

            break;
        case "questionsOfCategory":

            var query = "SELECT META(x).id AS id, x.* FROM deco_db x " +
                "WHERE x.type = 'question' " +
                "AND x.category_id = '" + reqObj.data[0].category_id + "'";

            simpleN1qlRequest.setRequest(reqObj, resObj, query);

            break;
        case "summaryOfScreening":

            var query = "SELECT META(x).id AS id, x.date_created FROM deco_db x " +
                "WHERE x.type = 'screeningSummary' " +
                "AND x.screening.id = '" + reqObj.data[0].screening_id + "' " +
                "ORDER BY x.date_created desc";

            simpleN1qlRequest.setRequest(reqObj, resObj, query);

            break;
        case "householdOfPerson":

            var query = "SELECT META(x).id AS id, x.nameFirst, x.nameLast, x.person_id, x.relationshipType_id, rel.display AS relationshipType_display " +
                "FROM deco_db x " +
                "JOIN deco_db rel ON KEYS x.relationshipType_id " +
                "WHERE x.type = 'householdMember' AND x.person_id = '" + reqObj.data[0].person_id + "' " +
                "ORDER BY x.nameLast, x.nameFirst";

            simpleN1qlRequest.setRequest(reqObj, resObj, query);

            break;
        case "incomeOfHousehold":

            var query = "SELECT META(x).id AS id, x.amount, x.incomeType_id, x.householdMember_id, x.type, x.date_updated, x.date_created FROM deco_db x " +
                "WHERE x.type = 'income' " +
                "AND x.householdMember_id = '" + reqObj.data[0].householdMember_id + "'";

            simpleN1qlRequest.setRequest(reqObj, resObj, query);

            break;
        case "assetsOfHousehold":

            var query = "SELECT META(x).id AS id, x.amount, x.assetType_id, x.householdMember_id, x.type, x.date_updated, x.date_created FROM deco_db x " +
                "WHERE x.type = 'asset' " +
                "AND x.householdMember_id = '" + reqObj.data[0].householdMember_id + "'";

            simpleN1qlRequest.setRequest(reqObj, resObj, query);

            break;
        case "incomeAssetsEligibility":

            var query = "SELECT META(x).id AS id, x.* FROM deco_db x " +
                "WHERE x.criteriaType = 'incomeAssetsEligibility' " +
                "AND x.program_id = '" + reqObj.data[0].program_id + "'";

            simpleN1qlRequest.setRequest(reqObj, resObj, query);

            break;
        case "documentsOfType":

            //need some drastic overhaul

            var whereClause;
            var type;
            var orderby = "";

            for (var i=0; i < reqObj.data.length; i++) {

                var l_obj = reqObj.data[i];

                for (var propertyName in l_obj) {

                    if (propertyName == "orderby") {
                        orderby = "ORDER BY LOWER(x." + l_obj[propertyName].type + ") " + l_obj[propertyName].ordering;
                        break;
                    }

                    if (propertyName == "type")
                        type = l_obj[propertyName];

                    if (i == 0)
                        whereClause = "WHERE x." + propertyName + " = '" + l_obj[propertyName] + "'";
                    else
                        whereClause += " AND x." + propertyName + " = '" + l_obj[propertyName] + "'";
                }

                if (type === "eligibilityObject") {
                    resObj = response.respond(resObj, {err:"can't use eligibilityObject", code:"R7"});
                }

                if (type) {
                    whereClause += " AND x.type = '" + type + "'";
                }

            }

            var query = "SELECT META(x).id AS id, x.* FROM deco_db x " + whereClause + " " + orderby;

            couchActions.n1qlRequest(query, function(err, res){

                if (err) {
                    resObj = response.respond(resObj, {err:err, code:"R8"});
                    return;
                }

                var data = res;
                var retObj = {};

                var dataArray = new Array;

                for (var i = 0; i < data.length; i++) {
                    dataArray.push(data[i]);
                }

                retObj[type] = dataArray;

                resObj = response.respond(resObj, retObj);
            });

            break;
        case "documentsOfListName":
            if(reqObj.data) {
                if(reqObj.data.length > 0) {


                    var retObj = {};

                    var totalResponses = reqObj.data.length;
                    var countResponses = 0;

                    for (var i=0; i < reqObj.data.length; i++) {
                        var data = reqObj.data[i];
                        if(data.listName) {

                            var query = couchActions.ViewQueryTemp.from('lookUps', 'documentsOfListName')
                                .range([data.listName],[data.listName,999])
                                .stale(couchActions.ViewQueryTemp.Update.BEFORE);
                            couchActions.viewQuery(query, function(err, res) {

                                if (err) {
                                    resObj = response.respond(resObj, {err:err, code:"R9"});
                                    return;
                                }

                                var resValues = getValues(res);

                                if (resValues[0])
                                    retObj[resValues[0].listName] = resValues;
                                else
                                    retObj[0] = resValues;

                                countResponses++;
                                if(countResponses >= totalResponses) {
                                    resObj = response.respond(resObj, retObj);
                                }
                            });
                        }
                    }
                }
            }
            break;
        case "screeningSummaryDirect":

            var query = "SELECT META(x).id AS id " +
                "FROM deco_db x " +
                "WHERE x.type = 'screeningSummary' " +
                "AND x.screening.medicalRecordNumber = '" + reqObj.data[0].medicalRecordNumber + "' " +
                "AND x.screening.accountNumber = '" + reqObj.data[0].accountNumber + "' " +
                "ORDER BY x.date_created desc " +
                "LIMIT 1";

            simpleN1qlRequest.setRequest(reqObj, resObj, query);

            break;
        case "generateScreeningSummary":

            screeningSummary.generateScreeningSummaryUserVersion(reqObj, resObj, reqObj.data[0].screening_id);

            break;
        case "listNames":

            var query = "SELECT META(x).id AS id, x.listName FROM deco_db x WHERE x.type = 'list' GROUP BY x.listName";

            simpleN1qlRequest.setRequest(reqObj, resObj, query);

            break;
        case "programsWithStateDisplay":

            var query = "SELECT META(x).id AS id, x.*, state.`value` AS stateAbbreviation " +
                "FROM deco_db x " +
                "JOIN deco_db state ON KEYS x.state_id " +
                "WHERE x.type = 'program' " +
                "ORDER BY state.displayOrder, x.title";

            simpleN1qlRequest.setRequest(reqObj, resObj, query);

            break;
        case "eligibilityProgramsLists":

            eligProList.buildEligibilityProgramsList(reqObj.data[0].id, function (eligReturnObj) {
                resObj = response.respond(resObj, eligReturnObj);
            });

            break;
        case "getAllDocumentTypes":

            var query = "SELECT META(x).id AS id, x.documentTypes " +
                "FROM deco_db x " +
                "WHERE x.type = 'permissionSetDocumentTypes'";

            simpleN1qlRequest.setRequest(reqObj, resObj, query);
            
            break;
        case "getCurrentUser":

            login.getCurrentUser(req, resObj);

            break;
        case "setCurrentClient":
            req.session.client_id = reqObj.data[0].client_id;

            req.session.userPermissionsClient = login.getModifiedPermissionsArrays(req.session.userPermissions, req.session.client_id);
            resObj = response.respond(resObj, {permissions: req.session.userPermissionsClient});

            break;
        case "setCurrentPerson":
            req.session.person_id = reqObj.data[0].person_id;
            resObj = response.respond(resObj, {success:"person_id set"});

            break;
        default:
            resObj = response.respond(resObj, {"error":"target is not correct!"});
    }
    return "read";
};

/* PUBLIC API */
module.exports.getRead = function(resObj,req) {
    lc_read(resObj,req);
};
