var couchActions = require('../requests/couchActions.js');
var db = require('../requests/db.js');
var personDocFunc = require('../helpers/personDocFunc.js');
var response = require('../helpers/response.js');
var async = require('async');
var moment = require('moment');
var bcrypt = require('bcrypt-nodejs');

/* set up objects before pushing to database */
function updateSetUp (data, id, resObj, req) {

    /* all docs need a date attr */
    data["date_updated"] = moment().format();

    if (data.type === "screening") {

        data["user_id"] = req.session.user_id;

        couchActions.updateDoc(id, data, function (err, res) {

            if (err) {
                resObj = response.respond(resObj, {err:err, code:"U1"});
                return;
            }

            resObj = response.respond(resObj, {"updated": "ok"});
        });

    } else if (data.type === "program") {

        if (data.stateDisplay)
            delete data.stateDisplay;

        couchActions.updateDoc(id, data, function (err, res) {

            if (err) {
                resObj = response.respond(resObj, {err:err, code:"U2"});
                return;
            }

            resObj = response.respond(resObj, {"updated": "ok"});
        });

    } else if (data.type === "person") {

        var updateIdsQuery =
            "SELECT ARRAY_AGG(ids.id) AS idArr, ids.person_id AS id " +
            "FROM (" +
                "SELECT distinct meta(ai).id AS id, h.person_id " +
                "FROM deco_db ai " +
                "JOIN deco_db h ON KEYS ai.householdMember_id " +
                "WHERE (ai.type='asset' OR ai.type='income') " +
                "AND h.type='householdMember' " +
                "UNION " +
                "SELECT distinct meta(h).id AS id, h.person_id " +
                "FROM deco_db h " +
                "WHERE h.type='householdMember'" +
            ") as ids " +
            "WHERE ids.person_id = '" + id + "'";

        db.n1qlRequest(updateIdsQuery, function (err, res) {

            if (err) {
                resObj = response.respond(resObj, {err:err, code:"U3"});
                return;
            }

            var updateQuery =
                "UPDATE deco_db x " +
                "SET x.client_ids = " + JSON.stringify(data.client_ids) + " " +
                "WHERE META(x).id IN " + JSON.stringify(res[0].idArr);

            async.parallel([
                function(callback) {
                    db.updateDoc(id, data, function(err, res) {
                        callback(err, res);
                    });
                },
                function(callback){
                    db.n1qlRequest(updateQuery, function (err, res) {
                        callback(err, res)
                    });
                }
            ], function(err, res) {

                if (err) {
                    resObj = response.respond(resObj, {err:err, code:"U4"});
                    return;
                }

                resObj = response.respond(resObj, {"updated": "ok"});
            });
        });

    } else if (data.type === "user") {

        if (data.password) {

            bcrypt.hash(data.password, null, null, function(err, hash) {

                if (err) {
                    resObj = response.respond(resObj, {err:err, code:"U5"});
                    return;
                }

                couchActions.updateDoc(id, data, function(err, res) {

                    if (err) {
                        resObj = response.respond(resObj, {err:err, code:"U6"});
                        return;
                    }

                    var query = "UPDATE deco_db x " +
                        "SET x.hashed_password = '" + hash + "' " +
                        "WHERE x.type = 'userCredentials' " +
                        "AND x.user_id = '" + id + "'";

                    couchActions.n1qlRequest(query, function(err, res) {

                        if (err) {
                            resObj = response.respond(resObj, {err:err, code:"U7"});
                            return;
                        }

                        resObj = response.respond(resObj, {"updated": "ok"});
                    });

                });
            });
        } else {
            couchActions.updateDoc(id, data, function (err, res) {

                if (err) {
                    resObj = response.respond(resObj, {err:err, code:"U8"});
                    return;
                }

                resObj = response.respond(resObj, {"updated": "ok"});
            });
        }

        /* set password attr in userDoc to null */
        data.password = "";

    } else {
        couchActions.updateDoc(id, data, function (err, res) {

            if (err) {
                resObj = response.respond(resObj, {err:err, code:"U9"});
                return;
            }

            resObj = response.respond(resObj, {"updated": "ok"});
        });
    }
}

function addClientIds (newClient_id, originalClient_ids) {

    var client_ids = [];

    if (newClient_id && originalClient_ids) {
        if (originalClient_ids.length > 0) {
            if (originalClient_ids.indexOf(newClient_id) === -1) {
                originalClient_ids.push(newClient_id);
                client_ids = originalClient_ids;
            } else
                client_ids = originalClient_ids;
        }

    } else if (!newClient_id && originalClient_ids) {
        client_ids = originalClient_ids;

    } // else return client_ids as empty array

    return client_ids;
}

var lc_update = function (resObj,req) {
    var reqObj = JSON.parse(req.body.data);
    var data = (reqObj.data) ? reqObj.data[0] : {};


    if (typeof data.client_ids === "undefined") {
        data.client_ids = [];
    }


    var id = data.id;
    delete data.id;


    if (data.type === "person") {
        db.getDoc(id, function(err, res) {

            if (err) {
                resObj = response.respond(resObj, {err:err, code:"U10"});
                return;
            }

            data['client_ids'] = addClientIds(req.session.client_id, res.value.client_ids);

            updateSetUp(data, id, resObj, req)
        });
    } else {
        couchActions.getDoc(id, function(err, res) {

            if (err) {
                resObj = response.respond(resObj, {err:err, code:"U10"});
                return;
            }

            data['client_ids'] = addClientIds(req.session.client_id, res.value.client_ids);

            updateSetUp(data, id, resObj, req)
        });
    }
};

var setNewClientIdsForPerson = function (newClient_ids, oldClient_ids, workingClients) {

    // set arr to all elements in oldClient_ids that are not in workingClients
    var arr = oldClient_ids.filter( function( el ) {
        return workingClients.indexOf( el ) < 0;
    });

    // add arr to newClient_ids
    return newClient_ids.concatNoDupes(arr);
};

var updatePersonClients = function (req, resObj) {

    var person_id = req.body.person_id;
    var newClient_ids = req.body.client_ids;
    var workingClients = loggedInUser.workingClients;

    //turn into array if not one already
    if (typeof newClient_ids === 'string' || newClient_ids instanceof String) {
        newClient_ids = [newClient_ids];
    }

    if (newClient_ids) {
        if (newClient_ids.length <= 0) {
            resObj = response.respond(resObj, {err:"client_ids array is empty, not allowed", code:"U11"});
            return;
        }
    }

    // do n1ql request to test ids are client_ids
    var clientCheckQuery =
        "SELECT META(x).id AS id " +
        "FROM deco_db x " +
        "WHERE x.type = 'client' " +
        "AND META(x).id IN " + JSON.stringify(newClient_ids);


    async.parallel({
        person: function (callback) {
            db.getDoc(person_id, function (err, res) {

                if (err) {
                    callback({err: err, code: "U12"}, null);
                    return;
                }

                callback(err, res.value);
            });
        },
        clients: function (callback) {
            couchActions.n1qlRequest(clientCheckQuery, function (err, res) {

                if (err) {
                    callback({err: err, code: "U13"}, null);
                    return;
                }

                if (res.length !== newClient_ids.length) {
                    callback({err: "Not all ids are client_ids OR there are repeat ids", code: "U14"}, null);
                    return;
                }

                callback(err, res);
            });
        }
    }, function(err, res) {

        if (err) {
            resObj = response.respond(resObj, err);
            return;
        }

        var person = res.person;
        var oldClient_ids = person.client_ids;


        newClient_ids = setNewClientIdsForPerson(newClient_ids, oldClient_ids, workingClients);

        person['client_ids'] = newClient_ids;

        updateSetUp(person, person_id, resObj, req);
    });
};

/* PUBLIC API */
module.exports.getUpdate = lc_update;
module.exports.updatePersonClients = updatePersonClients;