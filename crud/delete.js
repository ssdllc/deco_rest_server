var couchActions = require('../requests/couchActions.js');
var db = require('../requests/db.js');
var response = require('../helpers/response.js');

function deleteDoc (id, resObj) {

    couchActions.deleteDoc(id, function(err, res) {

        if (err) {
            resObj = response.respond(resObj, {err:err, code:"D1"});
        }
        resObj = response.respond(resObj, {"deleted": "ok"});
        console.log("Document deleted "+tools.timeStamp());
    });
}

function canIDeleteCaseCheck (results, doc) {

    //if (results[0]) { 
    //    /* if person has a guarantor of self, let them delete */
    //    if (results.length == 1) {
    //        if (results[0].id ==  results[0].key)
    //            callback(true);
    //        else callback(false);
    //    }
    //    /* delete a user even though they have a webSession Doc and a userCredentials Doc */
    //    else if (results.length == 2) {
    //        if (results[0].value == "webSession") {
    //            if (results[1].value == "userCredentials")
    //                callback(true);
    //        } else if (results[0].value == "userCredentials") {
    //            if (results[1].value == "webSession")
    //                callback(true);
    //        } else callback(false);
    //    } else callback(false);
    //} else callback(true);

    /* if there are any possible orphans */
    if (results[0]) {
        
        if (doc.type == "person") {
            /* delete a person even though they have a householdMember */
            if (results.length == 1) {
                if (results[0].value == "householdMember")
                //orphaning asset and income and householdMember records
                    return true;
            }
            
            else if (results.length == 2) {

                /* delete a person even though they have a householdMember and guarantor self */
                if (results[0].value == "householdMember") {
                    if (results[1].id ==  results[1].key)
                        return true;
                } else if (results[0].id ==  results[0].key) {
                    if (results[1].value == "householdMember")
                        return true;
                }
            }
        }


        //worksss !!!!!!!!!!!
        //else if (doc.type == "user") {
        //    if (results.length == 2) {
        //
        //        /* delete a user even though they have a webSession Doc and a userCredentials Doc */
        //        if (results[0].value == "webSession") {
        //            if (results[1].value == "userCredentials")
        //                return true;
        //        } else if (results[0].value == "userCredentials") {
        //            if (results[1].value == "webSession")
        //                return true;
        //        }
        //    }
        //}

        return false;
    
    /* no possible orphans */
    } else return true;
}

function canIDelete (id, doc, callback) {

    var query = couchActions.ViewQueryTemp.from('idSearch', 'foreignKeys')
        .range(id,id)
        .stale(couchActions.ViewQueryTemp.Update.BEFORE);

    db.viewQuery(query, function(err, res) {

        if (err) {
            callback({err:err, code:"D2"});
            return;
        }

        if (canIDeleteCaseCheck(res, doc))
            callback(true);
        else callback(false);
    });
}

var lc_delete = function(resObj, req) {
    var reqObj = JSON.parse(req.body.data);
    var id = (reqObj.data) ? reqObj.data[0].id : {};
    
    couchActions.getDoc(id, function(err, res) {

        if (err) {
            resObj = response.respond(resObj, {err:err, code:"D3"});
            return;
        }

        var doc = res.value;

        console.log(doc.type);
    
        canIDelete(id, doc, function (canDelete) {

            //if (canDelete === true) {

                // REMOVE ME WHEN WE DECIDE TO USE canDelete again //
                if (canDelete.code === "D2") {
                    resObj = response.respond(resObj, canDelete);
                }
                // REMOVE ME WHEN WE DECIDE TO USE canDelete again //
            
                if (doc.type === "user") {

                    /* delete both webSesson and userCredentials docs that include this user id */
                    var query = "DELETE FROM deco_db x WHERE x.user_id = '" + id + "' " +
                        "AND (x.type = 'userCredentials' OR x.type = 'roleAssignment')";

                    couchActions.n1qlRequest(query, function (err, res) {

                        if (err) {
                            resObj = response.respond(resObj, {err:err, code:"D4"});
                            return;
                        }

                        deleteDoc(id, resObj);
                    });

                } else if (doc.type === "person") {

                    //var query = "DELETE FROM deco_db x WHERE x.person_id = '" + id + "'";
                    //"AND (x.type = 'householdMember')"; // OR assetdocs/incomedocs...)//same as above

                    //not deleting assets and income, probably orphaning docs

                    //couchActions.n1qlRequest(query, function (err, res) {

                    //if (err) {
                    //resObj = response.respond(resObj, {err:err, code:"D5"});
                    //return;
                    //}

                    deleteDoc(id, resObj);
                    //});
                    
                } else if (doc.type === "program") {

                    /* delete both the eligibilityObject and ALL incomeAsset docs that include this program id */
                    var query = "DELETE FROM deco_db x WHERE x.program_id = '" + id + "' " +
                        "AND (x.type = 'eligibilityObject' OR x.criteriaType = 'incomeAssetsEligibility')";

                    couchActions.n1qlRequest(query, function (err, res) {

                        if (err) {
                            resObj = response.respond(resObj, {err:err, code:"D4"});
                            return;
                        }

                        deleteDoc(id, resObj);
                    });

                } else deleteDoc(id, resObj);

            //} else {
            //    if (canDelete.code === "D2") {
            //        resObj = response.respond(resObj, canDelete);
            //    } else {
            //        deleteDoc(id, resObj);
            //        /*response.respond(resObj, {"failed": "By deleting this document, " +
            //         "you would orphan other documents in the database. " +
            //         "You must delete the other documents before deleting this one."})*/
            //    }
            //}
        });
    });
};

/* PUBLIC API */
module.exports.getDelete = lc_delete;