const couchActions = require('../requests/couchActions.js');
const db = require('../requests/db.js');
const response = require('../helpers/response.js');
const Rx = require('rx');
const dbN1qlRequest = Rx.Observable.fromNodeCallback(db.n1qlRequest);
const n1qlRequestAgg = Rx.Observable.fromNodeCallback(couchActions.n1qlRequestAgg);
const n1qlRequestCla = Rx.Observable.fromNodeCallback(couchActions.n1qlRequestCla);

/* create/get queries, initiate the function to send a request, respond to client with data */
var buildExactMatchPersonsDatatable = function (reqObj, resObj, fieldsToSearch, whereClause) {

    console.log("Enter buildDatatable "+tools.timeStamp());

    var aoDataDictionary = createAoDataDictionary(reqObj);

    /* Setting and running data query */

    var query =
        "SELECT META(per).id AS id, per.nameFirst, per.nameLast, per.dateOfBirth, SUBSTR(per.ssn, -4) AS ssnl4, " +
        "per.address[0].street || ' ' ||  per.address[0].unitNumber || ' ' || " +
        "CASE WHEN per.address[0].state_id = '' " +
        "THEN per.address[0].zip " +
        "ELSE " +
        "st.display || ' ' || per.address[0].zip " +
        "END AS primaryAddress, " +
        "CASE WHEN per.guarantor.relation_id = '' " +
        "THEN 'None' " +
        "ELSE " +
        "CASE WHEN rel.display != 'Self' " +
        "THEN rel.display || ': ' || guar.nameFirst || ' ' || guar.nameLast " +
        "ELSE rel.display " +
        "END " +
        "END " +
        "AS guarantorDisplay " +
        "FROM deco_db per " +
        "LEFT JOIN deco_db guar ON KEYS per.guarantor.person_id " +
        "LEFT JOIN deco_db rel ON KEYS per.guarantor.relation_id " +
        "LEFT JOIN deco_db st ON KEYS per.address[0].state_id " +
        "WHERE "
        + whereClause
        + setGroupBy(reqObj)
        + setOrderBy(whereClause, aoDataDictionary, reqObj)
        + setPagination(aoDataDictionary, reqObj);
    var recordCountQuery = "SELECT COUNT(per.nameFirst) as count FROM deco_db per WHERE " + whereClause;

    var s1 = dbN1qlRequest(query);
    var s2 = dbN1qlRequest(recordCountQuery);

    var combination = Rx.Observable.zip(s1, s2, function (s1, s2) {
        return {
            data: s1,
            recordCount: s2
        };
    });

    var finishedObj = {};

    var logObserver = Rx.Observer.create(

        function (result) {
            finishedObj = result;
        },

        function (err) {
            resObj = response.respond(resObj, {err:err, code:"DT0"});
        },

        function () {

            var data = finishedObj.data;
            var recordCount = finishedObj.recordCount;

            /* respond to client */
            if(recordCount.length > 0)
                resObj = response.respond(resObj, {"recordCount":recordCount[0].count,"data":data});
            else
                resObj = response.respond(resObj, {"recordCount":0,"data":data});
        }
    );
    combination.subscribe(logObserver);
};

/* create/get queries, initiate the function to send a request, respond to client with data */
var buildDatatable = function (reqObj, resObj, fieldsToSearch, whereClause, datatable) {

    console.log("Enter buildDatatable "+tools.timeStamp());

    var aoDataDictionary = createAoDataDictionary(reqObj);

    /* Setting and running data query */
    if (datatable) {
        var query = createSelectStatement(reqObj, resObj, aoDataDictionary, fieldsToSearch, whereClause, datatable);
        var recordCountQuery = createRecordCountSelectStatement(reqObj, resObj, aoDataDictionary,
            fieldsToSearch, whereClause, datatable);
    }
    else {
        var query = createSelectStatement(reqObj, resObj, aoDataDictionary, fieldsToSearch, whereClause);
        var recordCountQuery = createRecordCountSelectStatement(reqObj, resObj, aoDataDictionary,
            fieldsToSearch, whereClause);
    }

    var s1 = n1qlRequestCla(query, setPagination(aoDataDictionary, reqObj));
    var s2 = n1qlRequestAgg(recordCountQuery, "count");

    var combination = Rx.Observable.zip(s1, s2, function (s1, s2) {
        return {
            data: s1,
            recordCount: s2
        };
    });

    var finishedObj = {};

    var logObserver = Rx.Observer.create(

        function (result) {
            finishedObj = result;
        },

        function (err) {
            resObj = response.respond(resObj, {err:err, code:"DT1"});
        },

        function () {

            var data = finishedObj.data;
            var recordCount = finishedObj.recordCount;

            /* one time case for listsDatatable */
            if (data.length > 0) {
                if (data[0].hasOwnProperty("items")) {
                    if (data[0].items == 0)
                        data = {};
                }
            }

            /* respond to client */
            if(recordCount.length > 0)
                resObj = response.respond(resObj, {"recordCount":recordCount[0].count,"data":data});
            else
                resObj = response.respond(resObj, {"recordCount":0,"data":data});
        }
    );
    combination.subscribe(logObserver);
};

function toLowerString (whereClause, fieldname) {

    var lowerString = "LOWER(x.";

    if (whereClause.startsWith("per.type = 'person'")) {
        lowerString = "LOWER(per.";
    }
    else if (fieldname == "state.`value`"
        || fieldname == "y.nameFirst || ' ' || y.nameLast"
        || fieldname == "state.display || ' ' || pro.title"
        || fieldname == "z.display") {
        lowerString = "LOWER(";
    }

    return lowerString
}

function setSearch (whereClause, aoDataDictionary, fieldsToSearch, datatable, reqObj) {

    var searchString = "";

    /* the value returned from search field */
    var searchParam =  reqObj.aoData[aoDataDictionary.search].value.value;

    /* If there are items in the search field create wildcard query */
    if (!(searchParam == "")) {

        /* change searchParam to lowercase */
        searchParam = searchParam.toLowerCase();

        /* Create whereClause */
        for(var j = 0; j < fieldsToSearch.length; j++) {

            var currentField = fieldsToSearch[j];
            var lowerString = toLowerString(whereClause, currentField) + currentField;

            if((j==0) && ((j+1) == fieldsToSearch.length)) //first and last run, only need AND
                searchString += " AND (" + lowerString + ") LIKE '%" + searchParam + "%')";
            else if(j==0) //first run, need AND and OR
                searchString += " AND (" + lowerString + ") LIKE '%" + searchParam + "%' OR";
            else if((j+1) == fieldsToSearch.length) //last run, don't need OR or AND
                searchString += " " + lowerString + ") LIKE '%" + searchParam + "%')";
            else //middle run, need only OR
                searchString += " " + lowerString + ") LIKE '%" + searchParam + "%' OR";
        }
    }

    /* Need to rethink some of this, this is a one time filter for persons Datatable, goes on top of aoData */
    if (datatable) {
        if (datatable.target == "personsDatatable") {
            for (var key in datatable) {
                if (datatable.hasOwnProperty(key) && key != "target") {
                    var like = datatable[key].toLowerCase();
                    searchString += " AND " + toLowerString(whereClause, null) + key + ") LIKE '%" + like + "%'";
                }
            }
        }
    }

    return searchString;
}

function setOrderBy (whereClause, aoDataDictionary, reqObj) {

    var orderByString = " ORDER BY ";

    /* if on last run don't return a comma, otherwise return a comma */
    function commaOrNotString () {
        if (reqObj.aoData[aoDataDictionary.order].value.length == (i+1))
            return " ";
        else
            return ", ";
    }

    function editOrderParam (orderParam) {
        if (whereClause.startsWith("per.type = 'person'")) {
            return "per." + orderParam;
        } else if (orderParam == "programDisplay"
            || orderParam == "hospitalDisplay"
            || orderParam == "stateAbbreviation"
            || orderParam == "userDisplay") {
            return orderParam;
        } else if (orderParam == "value") {
            return "`" + orderParam + "`";
        } else return "x." + orderParam;
    }

    for (var i = 0; i < reqObj.aoData[aoDataDictionary.order].value.length; i++) {

        /* asc vs desc value */
        var ordering = reqObj.aoData[aoDataDictionary.order].value[i].dir;

        /* the index for the name ordered by */
        var tempOrderIndex = reqObj.aoData[aoDataDictionary.order].value[i].column;

        /* determines whether or not the column type is a number, if number need to change query to orderBy properly */
        var columnType = reqObj.aoData[aoDataDictionary.columns].value[tempOrderIndex].name;

        /* uses the index from above to retrieve the name ordered by */
        var orderParam = reqObj.aoData[aoDataDictionary.columns].value[tempOrderIndex].data;

        orderParam = editOrderParam(orderParam);

        if(columnType == "number")
            orderByString += orderParam + " " + ordering + commaOrNotString();
        else
            orderByString += "LOWER(" + orderParam + ") " + ordering + commaOrNotString();
    }

    return orderByString;
}

function setPagination (aoDataDictionary, reqObj) {

    /* the value returned from # of models per page in drop down menu */
    var lengthParam = reqObj.aoData[aoDataDictionary.length].value;

    /* the value returned from # of models per page in drop down menu */
    var startParam = reqObj.aoData[aoDataDictionary.start].value;

    var paginationString = "LIMIT "+ lengthParam + " OFFSET "+ startParam;

    return paginationString;
}

function setGroupBy (reqObj) {

    if (reqObj.target == "listsDatatable")
        return " GROUP BY x.listName";
    else return "";
}

function createAoDataDictionary (reqObj) {

    if (reqObj.aoData) {

        var aoDataDictionary = {};

        for (var i = 0; i < reqObj.aoData.length; i++) {
            aoDataDictionary[reqObj.aoData[i].name] = i;
        }

    }

    return aoDataDictionary;
}

function createRecordCountSelectStatement (reqObj, resObj, aoDataDictionary, fieldsToSearch, whereClause, datatable) {

    if (reqObj.target == "personsDatatable") {
        var recordCountQuery = "SELECT META(per).id AS id FROM deco_db per WHERE " + whereClause;
    } else if (reqObj.target == "listsDatatable") {
        var recordCountQuery = "SELECT META(x).id AS id FROM deco_db x WHERE x.type = 'list' GROUP BY x.listName";
    } else if (reqObj.target == "tempPersonsDatatable" || reqObj.target == "tempScreeningsDatatable"
        || reqObj.target == "XMLDebugDatatable") {
        var recordCountQuery = "SELECT META(x).id AS id " +
            "FROM deco_db x " +
            "WHERE x.filename.originalName = '" + reqObj.data[0].filename + "' " +
            "AND x.user_id = '" + resObj.res.req.session.user_id + "' " +
            "AND " + whereClause;
    } else if (reqObj.target == "programsDatatable") {
        var recordCountQuery = "SELECT META(x).id AS id " +
            "FROM deco_db x " +
            "LEFT JOIN deco_db state ON KEYS x.state_id " +
            "WHERE " + whereClause;
    } else {
        var recordCountQuery = "SELECT META(x).id AS id FROM deco_db x WHERE " + whereClause;
    }

    /*
     * Adding search to query string
     * Adding group by to query string
     * */

    recordCountQuery += setSearch(whereClause, aoDataDictionary, fieldsToSearch, datatable, reqObj);

    return recordCountQuery;
}

/* creates dynamic select statements for datatables */
function createSelectStatement (reqObj, resObj, aoDataDictionary, fieldsToSearch, whereClause, datatable) {

    if (reqObj.target == "personsDatatable")
        var query =
            "SELECT META(per).id AS id, per.nameFirst, per.nameLast, per.dateOfBirth, SUBSTR(per.ssn, -4) AS ssnl4, " +
            "per.address[0].street || ' ' ||  per.address[0].unitNumber || ' ' || " +
            "CASE WHEN per.address[0].state_id = '' " +
            "THEN per.address[0].zip " +
            "ELSE " +
            "st.display || ' ' || per.address[0].zip " +
            "END AS primaryAddress, " +
            "CASE WHEN per.guarantor.relation_id = '' " +
            "THEN 'None' " +
            "ELSE " +
            "CASE WHEN rel.display != 'Self' " +
            "THEN rel.display || ': ' || guar.nameFirst || ' ' || guar.nameLast " +
            "ELSE rel.display " +
            "END " +
            "END " +
            "AS guarantorDisplay " +
            "FROM deco_db per " +
            "LEFT JOIN deco_db guar ON KEYS per.guarantor.person_id " +
            "LEFT JOIN deco_db rel ON KEYS per.guarantor.relation_id " +
            "LEFT JOIN deco_db st ON KEYS per.address[0].state_id " +
            "WHERE " + whereClause;
    else if(reqObj.target == "listsDatatable")
        var query = "SELECT count(x.listName) AS items, x.listName, META(x).id as id FROM deco_db x WHERE x.type = 'list'";
    else if (reqObj.target == "screeningDatatable")
        var query =
            "SELECT META(x).id AS id, x.*, " +
            "CASE WHEN y.nameFirst OR y.nameLast " +
            "THEN y.nameFirst || ' ' || y.nameLast " +
            "ELSE '' " +
            "END AS userDisplay " +
            "FROM deco_db x " +
            "LEFT JOIN deco_db y " +
            "ON KEYS x.user_id WHERE " + whereClause;
    else if (reqObj.target == "applicationsTemplatesDatatable")
        var query =
            "SELECT META(x).id AS id, x.*, " +
            "CASE WHEN state.display OR pro.title " +
            "THEN state.display || ' ' || pro.title " +
            "ELSE '' " +
            "END AS programDisplay " +
            "FROM deco_db x " +
            "LEFT JOIN deco_db pro ON KEYS x.program_id " +
            "LEFT JOIN deco_db state ON KEYS pro.state_id " +
            "WHERE " + whereClause;
    else if (reqObj.target == "programsDatatable")
        var query =
            "SELECT META(x).id AS id, x.*, " +
            "CASE WHEN state.`value` " +
            "THEN state.`value` " +
            "ELSE '' " +
            "END AS stateAbbreviation " +
            "FROM deco_db x " +
            "LEFT JOIN deco_db state ON KEYS x.state_id " +
            "WHERE " + whereClause;
    else if (reqObj.target == "tempPersonsDatatable" || reqObj.target == "tempScreeningsDatatable")
        var query =
            "SELECT META(x).id AS id, x.* " +
            "FROM deco_db x " +
            "WHERE x.filename.originalName = '" + reqObj.data[0].filename + "' " +
            "AND x.user_id = '" + resObj.res.req.session.user_id + "' " +
            "AND " + whereClause;
    else if (reqObj.target == "XMLDebugDatatable")
        var query =
            "SELECT META(x).id AS id, x.*, " +
            "CASE WHEN y.nameFirst OR y.nameLast " +
            "THEN y.nameFirst || ' ' || y.nameLast " +
            "ELSE '' " +
            "END AS userDisplay " +
            "FROM deco_db x " +
            "LEFT JOIN deco_db y ON KEYS x.user_id " +
            "WHERE x.filename.originalName = '" + reqObj.data[0].filename + "' " +
            "AND x.user_id = '" + resObj.res.req.session.user_id + "' " +
            "AND " + whereClause;
    else if (reqObj.target == "importedXMLDocsDatatable")
        var query =
            "SELECT META(x).id AS id, x.*, " +
            "CASE WHEN y.nameFirst OR y.nameLast " +
            "THEN y.nameFirst || ' ' || y.nameLast " +
            "ELSE '' " +
            "END AS userDisplay, " +
            "CASE WHEN z.name " +
            "THEN z.name " +
            "ELSE '' " +
            "END AS hospitalDisplay " +
            "FROM deco_db x " +
            "LEFT JOIN deco_db y ON KEYS x.user_id " +
            "LEFT JOIN deco_db z ON KEYS x.hospital_id " +
            "WHERE " + whereClause;
    else if (reqObj.target == "chainsDatatable")
        var query =
            "SELECT META(x).id AS id, x.name, x.abbreviation, " +
            "x.address[0].address1 || ' ' ||  x.address[0].address2 || ' ' || " +
            "CASE WHEN x.address[0].state_id " +
            "THEN st.display || ' ' || x.address[0].zip " +
            "ELSE " +
            "x.address[0].zip " +
            "END AS primaryAddress, " +
            "x.phone[0].phoneNumber AS phoneNumber, " +
            "x.person_identifier_length " +
            "FROM deco_db x " +
            "LEFT JOIN deco_db st ON KEYS x.address[0].state_id " +
            "WHERE " + whereClause;
    else
        var query = "SELECT META(x).id AS id, x.* FROM deco_db x WHERE " + whereClause;

    /*
     * Adding search to query string
     * Adding group by to query string
     * Adding order by to query string
     * Adding limits & offsets (pagination) to query string
     * */

    query += setSearch(whereClause, aoDataDictionary, fieldsToSearch, datatable, reqObj)
        + setGroupBy(reqObj)
        + setOrderBy(whereClause, aoDataDictionary, reqObj);

    return query;
}


/* PUBLIC API */
module.exports.buildDatatable = buildDatatable;
module.exports.buildExactMatchPersonsDatatable = buildExactMatchPersonsDatatable;