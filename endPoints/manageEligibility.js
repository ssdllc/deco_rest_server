var couchActions = require('../requests/couchActions.js'),
    response = require('../helpers/response.js');

var getEligibility = function (req, resObj) {

    var program_id = req.body.program_id;

    var query =
        "SELECT META(x).id AS id, x.* " +
        "FROM deco_db x " +
        "WHERE x.type = 'eligibilityObject' " +
        "AND x.program_id = '" + program_id + "'";

    couchActions.n1qlRequest(query, function (err, res) {

        if (err) {
            resObj = response.respond(resObj, {err:err, code:"MELIG1"});
            return;
        }

        if (res.length === 0) {
            resObj.res.json({});
            resObj.responded = true;
            return;
        }

        resObj = response.respond(resObj, res[0]);
    });
};

/* PUBLIC API */
module.exports = {
    getEligibility: getEligibility
};