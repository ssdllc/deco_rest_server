var couchActions = require('../requests/couchActions.js');
var db = require('../requests/db.js');
var response = require('../helpers/response.js');
var async = require('async');
var bcrypt = require('bcrypt-nodejs');
var moment = require('moment');

var giveAuthEngineUserPermissions = function (userPermissions) {

    // pass userPermissions to couchActions for authorization purposes
    couchActions.setUserPermissions(userPermissions);
};

var getUserPermissions = function (user_id, callback) {

    var createPrivilegesQuery =
        "SELECT DISTINCT psids.client_id, cr AS `type` " +
        "FROM (" +
            "SELECT DISTINCT rda.client_id AS client_id, ps.`create` AS `create`, ps.read AS read, ps.`update` AS `update`, ps.`delete` AS `delete` " +
            "FROM (" +
                "SELECT DISTINCT ra.client_id, rd.permissionSet_ids " +
                "FROM (" +
                    "SELECT DISTINCT cr.client_id, cr.role_id as role_id " +
                    "FROM deco_db ra " +
                    "UNNEST ra.clientRoles cr " +
                    "WHERE ra.type = 'roleAssignment' AND ra.user_id = '" + user_id + "'" +
                    "UNION " +
                    "SELECT DISTINCT gr as role_id " +
                    "FROM deco_db ra " +
                    "UNNEST ra.generalRoles gr " +
                    "WHERE ra.type = 'roleAssignment' AND ra.user_id = '" + user_id + "') ra " +
                "JOIN deco_db rd ON KEYS ra.role_id) rda " +
            "UNNEST rda.permissionSet_ids psid " +
            "JOIN deco_db ps ON KEYS psid) psids " +
        "UNNEST psids.`create` cr";

    var readPrivilegesQuery =
        "SELECT DISTINCT psids.client_id, re AS `type` " +
        "FROM (" +
            "SELECT DISTINCT rda.client_id AS client_id, ps.`create` AS `create`, ps.read AS read, ps.`update` AS `update`, ps.`delete` AS `delete` " +
            "FROM (" +
                "SELECT DISTINCT ra.client_id, rd.permissionSet_ids " +
                "FROM (" +
                    "SELECT DISTINCT cr.client_id, cr.role_id as role_id " +
                    "FROM deco_db ra " +
                    "UNNEST ra.clientRoles cr " +
                    "WHERE ra.type = 'roleAssignment' AND ra.user_id = '" + user_id + "'" +
                    "UNION " +
                    "SELECT DISTINCT gr as role_id " +
                    "FROM deco_db ra " +
                    "UNNEST ra.generalRoles gr " +
                    "WHERE ra.type = 'roleAssignment' AND ra.user_id = '" + user_id + "') ra " +
                "JOIN deco_db rd ON KEYS ra.role_id) rda " +
            "UNNEST rda.permissionSet_ids psid " +
            "JOIN deco_db ps ON KEYS psid) psids " +
        "UNNEST psids.`read` re";

    var updatePrivilegesQuery =
        "SELECT DISTINCT psids.client_id, up AS `type` " +
        "FROM (" +
            "SELECT DISTINCT rda.client_id AS client_id, ps.`create` AS `create`, ps.read AS read, ps.`update` AS `update`, ps.`delete` AS `delete` " +
            "FROM (" +
                "SELECT DISTINCT ra.client_id, rd.permissionSet_ids " +
                "FROM (" +
                    "SELECT DISTINCT cr.client_id, cr.role_id as role_id " +
                    "FROM deco_db ra " +
                    "UNNEST ra.clientRoles cr " +
                    "WHERE ra.type = 'roleAssignment' AND ra.user_id = '" + user_id + "'" +
                    "UNION " +
                    "SELECT DISTINCT gr as role_id " +
                    "FROM deco_db ra " +
                    "UNNEST ra.generalRoles gr " +
                    "WHERE ra.type = 'roleAssignment' AND ra.user_id = '" + user_id + "') ra " +
                "JOIN deco_db rd ON KEYS ra.role_id) rda " +
            "UNNEST rda.permissionSet_ids psid " +
            "JOIN deco_db ps ON KEYS psid) psids " +
        "UNNEST psids.`update` up";

    var deletePrivilegesQuery =
        "SELECT DISTINCT psids.client_id, de AS `type` " +
        "FROM (" +
            "SELECT DISTINCT rda.client_id AS client_id, ps.`create` AS `create`, ps.read AS read, ps.`update` AS `update`, ps.`delete` AS `delete` " +
            "FROM (" +
                "SELECT DISTINCT ra.client_id, rd.permissionSet_ids " +
                "FROM (" +
                    "SELECT DISTINCT cr.client_id, cr.role_id as role_id " +
                    "FROM deco_db ra " +
                    "UNNEST ra.clientRoles cr " +
                    "WHERE ra.type = 'roleAssignment' AND ra.user_id = '" + user_id + "'" +
                    "UNION " +
                    "SELECT DISTINCT gr as role_id " +
                    "FROM deco_db ra " +
                    "UNNEST ra.generalRoles gr " +
                    "WHERE ra.type = 'roleAssignment' AND ra.user_id = '" + user_id + "') ra " +
                "JOIN deco_db rd ON KEYS ra.role_id) rda " +
            "UNNEST rda.permissionSet_ids psid " +
            "JOIN deco_db ps ON KEYS psid) psids " +
        "UNNEST psids.`delete` de";

    async.parallel({
        create: function(callback){
            db.n1qlRequest(createPrivilegesQuery, function (err, res) {
                callback(err, {"permissions":res, "whereClause":""});
            })
        },
        read: function(callback){
            db.n1qlRequest(readPrivilegesQuery, function (err, res) {
                callback(err, {"permissions":res, "whereClause":""});
            })
        },
        update: function(callback){
            db.n1qlRequest(updatePrivilegesQuery, function (err, res) {
                callback(err, {"permissions":res, "whereClause":""});
            })
        },
        delete: function(callback){
            db.n1qlRequest(deletePrivilegesQuery, function (err, res) {
                callback(err, {"permissions":res, "whereClause":""});
            })
        }
    }, function(err, res) {
        callback(err, res);
    });
};

var getModifiedPermissionsArrays = function (userPermissions, client_id) {

    var retObj = {
        create: [],
        read: [],
        update: [],
        delete: []
    };

    for (var propertyName in userPermissions) {

        var currentCrudArray = userPermissions[propertyName].permissions;

        for (var i = 0; i < currentCrudArray.length; i++) {

            var currentPermission = currentCrudArray[i];

            // if retObj does not have this type already then push
            if (retObj[propertyName].indexOf(currentPermission.type) === -1) {
                if (!currentPermission.hasOwnProperty("client_id")) {
                    retObj[propertyName].push(currentPermission.type);
                }
                else if (currentPermission.client_id === client_id) {
                    retObj[propertyName].push(currentPermission.type);
                }
            }
        }
    }

    return retObj;
};

var getCurrentUser = function (req, resObj) {

    loggedIn(req, function (bool) {

        if (bool) {

            var userQuery =
                "SELECT META(`user`).id AS id, `role`.workingClients, `role`.endPoints, `user`.nameFirst, `user`.nameLast " +
                "FROM deco_db `role` " +
                "JOIN deco_db `user` ON KEYS `role`.user_id " +
                "WHERE `role`.user_id = '" + req.session.user_id + "' " +
                "AND `role`.type = 'roleAssignment'";

            var clientQuery =
                "SELECT META(x).id AS id, x.name " +
                "FROM deco_db x " +
                "WHERE META(x).id = '"+req.session.client_id+"' " +
                "AND x.type = 'client'";

            var personQuery =
                "SELECT META(x).id AS id, x.nameFirst, x.nameLast " +
                "FROM deco_db x " +
                "WHERE META(x).id = '"+req.session.person_id+"' " +
                "AND x.type = 'person'";

            async.parallel({
                user: function(callback){
                    db.n1qlRequest(userQuery, function (err, res) {
                        callback(err, res);
                    })
                },
                //auth engine prevent possible access
                client: function(callback){
                    couchActions.n1qlRequest(clientQuery, function (err, res) {
                        callback(err, res);
                    })
                },
                //auth engine prevent possible access
                person: function(callback){
                    couchActions.n1qlRequest(personQuery, function (err, res) {
                        callback(err, res);
                    })
                }
            }, function(err, res) {

                if (err) {
                    resObj = response.respond(resObj, {err:err, code:"L1"});
                    return;
                }

                res['permissions'] = req.session.userPermissionsClient;
                resObj = response.respond(resObj, res);

            });
        }
        else
            resObj = response.respond(resObj, {"Error":"Session expired. Please log in again."});
    });
};

var loggedIn = function (req, callback) {

    // session does exist
    if (req.session && req.session.user_id) {

        // update lastRequestTime
        req.session.lastRequestTime = moment();

        // pass userPermissions to couchActions for authorization purposes
        giveAuthEngineUserPermissions(req.session.userPermissions);

        callback(true);
    }
    // session does not exist
    else callback(false);
};

var checkUsersEndPointAccess = function (endPoint) {

    if (loggedInUser.endPoints.indexOf(endPoint) !== -1)
        return true;

    return false;
};

var setLongerSessionTimeout = function (req, endPoints) {

    if (endPoints.indexOf("manageEligibility") !== -1)
        req.session.cookie.maxAge = 120*60*1000; //stay alive for 2 hours
};

var loggedIn2 = function (req, callback) {

    // session does exist
    if (req.session && req.session.user_id) {

        // update lastRequestTime
        req.session.lastRequestTime = moment();
        // pass userPermissions to couchActions for authorization purposes
        giveAuthEngineUserPermissions(req.session.userPermissions);

        var loggedInUserQuery =
            "SELECT META(`user`).id AS id, `role`.workingClients, `role`.endPoints, `user`.nameFirst, `user`.nameLast " +
            "FROM deco_db `role` " +
            "JOIN deco_db `user` ON KEYS `role`.user_id " +
            "WHERE `role`.user_id = '" + req.session.user_id + "' " +
            "AND `role`.type = 'roleAssignment'";

        db.n1qlRequest(loggedInUserQuery, function(err, res) {

            if (err) {
                callback({err:err, code:"L2"}, null);
                return;
            }

            loggedInUser = res[0];
            
            callback(null, true);
        });
    }
    // session does not exist
    else callback(null, false);
};

var login = function (data, callback) {

    if (data.username && data.password) {

        var userQuery =
            "SELECT META(x).id AS id, x.nameFirst, x.nameLast " +
            "FROM deco_db x " +
            "WHERE x.email = '"+data.username+"' " +
            "AND x.type = 'user'";

        db.n1qlRequest(userQuery, function (err, res) {

            if (err) {
                callback({err:err, code:"L3"}, null);
                return;
            }

            var user = res[0];

            /* if user exists */
            if (user) {

                var userCredentialsQuery =
                    "SELECT x.hashed_password " +
                    "FROM deco_db x " +
                    "WHERE x.user_id = '" + user.id + "' " +
                    "AND x.type = 'userCredentials'";

                var roleAssignmentsQuery =
                    "SELECT x.workingClients, x.endPoints " +
                    "FROM deco_db x " +
                    "WHERE x.user_id = '" + user.id + "' " +
                    "AND x.type = 'roleAssignment'";

                async.parallel({
                    userCredentials: function(callback){
                        db.n1qlRequest(userCredentialsQuery, function (err, res) {
                            if (err) {
                                callback({err:err, code:"L4"}, null);
                                return;
                            }
                            callback(err, res);
                        })
                    },
                    roleAssignment: function(callback){
                        db.n1qlRequest(roleAssignmentsQuery, function (err, res) {
                            if (err) {
                                callback({err:err, code:"L5"}, null);
                                return;
                            }
                            callback(err, res);
                        })
                    }
                }, function(err, res) {

                    if (err) {
                        callback(err, null);
                        return;
                    }

                    var userCredentials = res.userCredentials;
                    var roleAssignment = res.roleAssignment;

                    user["workingClients"] = roleAssignment[0].workingClients;
                    user["endPoints"] = roleAssignment[0].endPoints;

                    bcrypt.compare(data.password, userCredentials[0].hashed_password, function(err, res) {

                        /* deleting passwords */
                        delete data.password;
                        delete userCredentials[0].hashed_password;

                        if (err) {
                            callback({err:err, code:"L6"}, null);
                            return;
                        }

                        // if passwords do not match
                        if (!res) {
                            callback({Error:"Incorrect username and/or password."}, null); //incorrect password
                            return;
                        }

                        callback(err, res, user);
                    });

                });

            } else callback({Error:"Incorrect username and/or password."}, null); //incorrect username
        });
    } else callback({Error:"You need both a username and password to login"}, null);
};

var logout = function (req, res) {

    req.session.destroy(function(err) {

        if (err && err.message !== "The key does not exist on the server") {
            res.send({err:err, code:"L7"});
            return;
        }

        res.send({"Success":"Logout successful"});
    });
};

/* PUBLIC API */
module.exports = {
    logout: logout,
    login: login,
    loggedIn: loggedIn,
    loggedIn2: loggedIn2,
    checkUsersEndPointAccess: checkUsersEndPointAccess,
    setLongerSessionTimeout: setLongerSessionTimeout,
    getCurrentUser: getCurrentUser,
    getUserPermissions: getUserPermissions,
    getModifiedPermissionsArrays: getModifiedPermissionsArrays,
    giveAuthEngineUserPermissions: giveAuthEngineUserPermissions
};
