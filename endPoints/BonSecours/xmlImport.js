var fs = require('fs'),
    uuid = require('node-uuid'),
    xml2js = require('xml2js'),
    moment = require('moment'),
    validator = require('validator'),
    config = require('../../config.json'),
    response = require('../../helpers/response.js'),
    couchActions = require('../../requests/couchActions.js'),
    create = require('../../crud/create/create.js'),
    listNames,
    processed = {
        "persons":0,
        "screenings":0
    },
    filename = {
        "originalName":"",
        "serverName":""
    },
    ssn_ignoreList = ["777777777","666666666"];

function createHouseholdMemberDocs (tempPerson, resObj, callback) {

    /* if there are no tempPersons being uploaded */
    if (tempPerson.length == 0) {
        callback();
    } else {

        var done = 0;

        for (var i = 0; i < tempPerson.length; i++) {
            create.createHouseholdMemberSelf(tempPerson[i].id, tempPerson[i].client_ids, resObj, function () {

                done++;

                if (done === tempPerson.length)
                    callback();
            })
        }
    }
}

/* changes temp data to real data */
function commitTempData (filename, resObj) {

    var tempPersonIDquery = "SELECT META(x).id AS id, x.client_ids " +
        "FROM deco_db x " +
        "WHERE x.type = 'tempPerson' " +
        "AND x.user_id = '" + resObj.res.req.session.user_id + "' " +
        "AND x.filename.originalName = '" + filename + "'";

    /* grab all tempPerson ids */
    couchActions.n1qlRequest(tempPersonIDquery, function(err, res) {

        if (err) {
            respond(resObj, {err:err, code:"XI1"});
            return;
        }

        var tempPerson = res;

        var changeTypeQuery = "UPDATE deco_db x " +
            "SET x.type = " +
            "CASE WHEN x.type = 'tempPerson' " +
            "THEN 'person' " +
            "ELSE 'screening' " +
            "END " +
            "WHERE (x.type = 'tempPerson' OR x.type = 'tempScreening') " +
            "AND x.user_id = '" + resObj.res.req.session.user_id + "' " +
            "AND x.filename.originalName = '" + filename + "'";

        /* change all docs with temp type to normal type */
        couchActions.n1qlRequest(changeTypeQuery, function (err, res) {

            if (err) {
                respond(resObj, {err:err, code:"XI2"});
                return;
            }

            /* creates householdMemberDocs for newly created persons */
            createHouseholdMemberDocs(tempPerson, resObj, function() {

                var setCommittedOnQuery = "UPDATE deco_db x " +
                    "SET x.committedOn = '" + moment().format() + "' " +
                    "WHERE x.type = 'xmlUploadHistory' " +
                    "AND x.user_id = '" + resObj.res.req.session.user_id + "' " +
                    "AND x.filename.originalName = '" + filename + "'";

                /* sets commitedOn attr do the xmlUploadHistory Doc */
                couchActions.n1qlRequest(setCommittedOnQuery, function (err, res) {

                    if (err) {
                        respond(resObj, {err:err, code:"XI3"});
                        return;
                    }

                    respond(resObj, {"Success":"Documents commited"});
                });
            });
        });
    });
}

/* discards all temp data */
function discardTempData (originalFilename, resObj) {

    filename.originalName = originalFilename;

    deleteDocsOfSameFileName(resObj, function () {
        respond(resObj, {"Success":"Documents discarded"});
    });
}

function personWithMoreInfo (person1,person2) {
    if (person1.ssn && !person2.ssn) {
        return person1;
    } else if (person2.ssn && !person1.ssn) {
        return person2;
    } else if (person1.nameFirst && person1.nameLast && person1.dateOfBirth
        && (!person2.nameFirst || !person2.nameLast || !person2.dateOfBirth)) {
        return person1;
    } else if (person2.nameFirst && person2.nameLast && person2.dateOfBirth
        && (!person1.nameFirst || !person1.nameLast || !person1.dateOfBirth)) {
        return person2;
    } else if (person1.nameFirst && person1.nameLast && (!person2.nameFirst || !person2.nameLast)) {
        return person1;
    } else if (person2.nameFirst && person2.nameLast && (!person1.nameFirst || !person1.nameLast)) {
        return person2;
    } else {
        return person1;
    }
}

function samePersonCheck (person1, person2) {

    if (person1.ssn && person2.ssn) {
        if(person1.ssn == person2.ssn) {
            return personWithMoreInfo(person1,person2);
        } else {
            return null;
        }

    } else if (person1.nameFirst && person1.nameLast && person1.dateOfBirth
        && person2.nameFirst && person2.nameLast && person2.dateOfBirth) {

        if(person1.nameFirst == person2.nameFirst && person1.nameLast == person2.nameLast
            && person1.dateOfBirth == person2.dateOfBirth) {
            return personWithMoreInfo(person1,person2);
        } else {
            return null;
        }

    } else if (person1.nameFirst && person1.nameLast && person2.nameFirst && person2.nameLast) {
        if(person1.nameFirst == person2.nameFirst && person1.nameLast == person2.nameLast) {
            return personWithMoreInfo(person1,person2);
        } else {
            return null;
        }

    } else {
        return null;
    }
}

function escapeChars (str) {
    if (str)
        return str.replace(/'/g, "''");
    else return str;
}

/* does real person exist in couchbase */
function doesPersonExist (person, resObj, callback) {

    var ssn = escapeChars(person.ssn);
    var nameFirst =  escapeChars(person.nameFirst);
    var nameLast = escapeChars(person.nameLast);
    var dateOfBirth = escapeChars(person.dateOfBirth);

    var query;

    if (ssn && nameFirst && nameLast && dateOfBirth) {
        query = "SELECT META(x).id AS id, x.* FROM deco_db x " +
            "WHERE x.type = 'person' " +
            "AND ((x.ssn = '" + ssn + "') " +
            "OR (x.nameFirst = '" + nameFirst + "' " +
            "AND x.nameLast = '" + nameLast + "' " +
            "AND x.dateOfBirth = '" + dateOfBirth + "'))";

        couchActions.n1qlRequest(query, function (err, res) {

            if (err) {
                respond(resObj, {err:err, code:"XI4"});
                return;
            }

            var result = res;

            if(result.length == 0) {
                callback(-1);
            } else callback(result[0]);
        });
    } else if (ssn && nameLast) {
        query = "SELECT META(x).id AS id, x.* FROM deco_db x " +
            "WHERE x.type = 'person' " +
            "AND x.ssn = '" + ssn + "' " +
            "AND x.nameLast = '" + nameLast + "'";

        couchActions.n1qlRequest(query, function (err, res) {

            if (err) {
                respond(resObj, {err:err, code:"XI5"});
                return;
            }

            var result = res;

            if(result.length == 0) {
                callback(-1);
            } else callback(result[0]);
        });
    } else if (nameFirst && nameLast && dateOfBirth) {
        query = "SELECT META(x).id AS id, x.* FROM deco_db x " +
            "WHERE x.type = 'person' " +
            "AND x.nameFirst = '" + nameFirst + "' " +
            "AND x.nameLast = '" + nameLast + "' " +
            "AND x.dateOfBirth = '" + dateOfBirth + "'";

        couchActions.n1qlRequest(query, function (err, res) {

            if (err) {
                respond(resObj, {err:err, code:"XI6"});
                return;
            }

            var result = res;

            if(result.length == 0) {
                callback(-1);
            } else callback(result[0]);
        });
    } else {
        var debugMessage = "Person: " + nameFirst + " " + nameLast + " ignored because missing minimum information";

        createDebugDoc("createDoc", person, debugMessage, resObj, function() {
            callback(-2);
        });
    }
}

/* does real screening exist in couchbase */
function doesScreeningExist (screening, callback) {

    var accountNumber = escapeChars(screening.accountNumber);
    var medicalRecordNumber = escapeChars(screening.medicalRecordNumber);
    var firstClientId = escapeChars(screening.client_ids[0]);

    var query = "SELECT META(x).id AS id FROM deco_db x " +
        "WHERE x.type = 'screening' " +
        "AND x.accountNumber = '" + accountNumber + "' " +
        "AND x.medicalRecordNumber = '" + medicalRecordNumber + "' " +
        "AND x.client_ids[0] = '" + firstClientId + "'";

    couchActions.n1qlRequest(query, function (err, res) {

        if (err) {
            respond(resObj, {err:err, code:"XI7"});
            return;
        }

        var result = res;

        if(result.length == 0) {
            callback(-1);
        } else callback(result[0].id);
    });
}

function getAttributeId (listValue, listName) {

    for (var i = 0; i < listNames.length; i++) {

        console.log(listNames[i].listName);
        console.log(listName);
        console.log('-------------------------');
        if (listNames[i].listName == listName) {
            if (listNames[i].value.toLowerCase() == listValue) {
                return listNames[i].id;
            }
        }
    }

    console.log('ERROR: Attribute ID not found: '+listValue);
    return "";
}

function bonSecoursAttrId (xmlValue, listName) {

    /* lower case data */
    xmlValue = xmlValue.toLowerCase();

    if (listName == "relationshipStatus") {

        switch(xmlValue) {
            case "s":
                return getAttributeId ("single", listName);
            case "m":
                return getAttributeId ("married", listName);
            case "d":
                return getAttributeId ("divorced", listName);
            default: console.log('ERROR: relationshipStatus letter not supported');
        }
    } else if (listName == "country") {
        if (xmlValue == "us")
            getAttributeId ("usa", listName);
    } else return getAttributeId (xmlValue, listName);

    return "";
}

function checkSSNIgnoreList (ssn) {

    if (ssn_ignoreList.indexOf(ssn) >= 0)
        return null;
    else
        return ssn;
}

function generateScreeningDoc (placement, client_id, user_id) {

    var screening = {
        "displayConditions": true,
        "date_created": "",
        "date_updated": "",
        "user_id": user_id,
        "filename": filename,
        "person_id": "",
        "type": "tempScreening",
        "results": [
        ],
        "accountNumber": "",
        "admission_date":"",
        "medicalRecordNumber": "",
        "aoPlacementNumber":"",
        "client_ids": [],
        "state_id": "",
        "channels": [
            "ssd"
        ]
    };

    screening.date_created = moment().format();
    screening.date_updated = moment().format();

    if (placement.Pat_Acct_Num)
        screening.accountNumber = placement.Pat_Acct_Num[0].trim();
    else screening.accountNumber = "none provided";

    if (placement.Admit_Dt)
        screening.admission_date = placement.Admit_Dt[0].trim();

    if (placement.Med_Rec_Num)
        screening.medicalRecordNumber = placement.Med_Rec_Num[0].trim();
    else screening.medicalRecordNumber = "none provided";

    if (placement.Aoplc_Num)
        screening.aoPlacementNumber = placement.Aoplc_Num[0].trim();
    else screening.aoPlacementNumber = "none provided";

    if (client_id)
        screening.client_ids.push(client_id);
    screening.state_id = bonSecoursAttrId("MD", "state");


    return screening;
}

function generatePersonDoc (placement, type, client_id, user_id) {

    var person = {
        "type": "tempPerson",
        "namePrefix_id": "",
        "nameFirst": "",
        "nameMiddle": "",
        "nameLast": "",
        "nameSuffix_id": "",
        "nameMaiden": "",
        "ssn": "",
        "user_id": user_id,
        "filename": filename,
        "gender_id": "",
        "dateOfBirth": "",
        "dateOfDeath": "",
        "homeless": false,
        "address": [
            {
                "residence": true,
                "mailingAddress": true,
                "addressTitle": "",
                "street": "",
                "unitNumber": "",
                "city": "",
                "state_id": "",
                "zip": "",
                "zipPlus4": "",
                "country_id": ""
            }
        ],
        "email": [
            {
                "emailTitle": "",
                "emailAddress": ""
            }
        ],
        "phone": [
            {
                "phoneType_id": "",
                "phoneNumber": ""
            }
        ],
        "client_ids": [ client_id ],
        "language_id": "",
        "educationLevel_id": "",
        "race_id": "",
        "citizenshipStatus_id": "",
        "relationshipStatus_id": "",
        "immigrationStatus_id": "",
        "veteran": false,
        "dateOfEntry": "",
        "greenCardIssueDate": "",
        "countryOfOrigin_id": "",
        "employerJobTitle": "",
        "employerCompanyName": "",
        "guarantor": {
            "person_id": "",
            "relation_id": ""
        },
        "channels": [
            "ssd"
        ]
    };

    var patient = {};

    if (placement[type])
        patient = placement[type][0];

    if (patient.Fnm)
        person.nameFirst = patient.Fnm[0].trim();
    if (patient.Mi)
        person.nameMiddle = patient.Mi[0].trim();
    if (patient.Lnm)
        person.nameLast = patient.Lnm[0].trim();
    if (patient.Dob)
        person.dateOfBirth = moment(patient.Dob[0].trim()).format('MM/DD/YYYY');
    if (patient.Sex)
        person.gender_id = bonSecoursAttrId(patient.Sex[0].trim(), "gender");
    if (patient.Ssn)
        person.ssn = checkSSNIgnoreList(patient.Ssn[0].trim());
    if (patient.Marital_Stat_Cd)
        person.relationshipStatus_id = bonSecoursAttrId(patient.Marital_Stat_Cd[0].trim(), "relationshipStatus");
    if (patient.Addr1)
        person.address[0].street = patient.Addr1[0].trim();
    if (patient.Addr2) {
        if (patient.Addr2[0].trim())
            person.address[0].street += ", " + patient.Addr2[0].trim();
    }
    if (patient.City)
        person.address[0].city = patient.City[0].trim();
    if (patient.State)
        person.address[0].state_id = bonSecoursAttrId(patient.State[0].trim(), "state");
    if (patient.Postal_Cd) {
        var trim = patient.Postal_Cd[0].trim();
        var splitString = trim.split("-");
        person.address[0].zip = splitString[0];
        person.address[0].zipPlus4 = splitString[1];
    }
    if (patient.Country_Cd)
        person.address[0].country_id = bonSecoursAttrId(patient.Country_Cd[0].trim(), "country");
    if (patient.Phone) {
        person.phone[0].phoneNumber = patient.Phone[0].trim();
        person.phone[0].phoneType_id = bonSecoursAttrId("home", "phoneType");
    }
    if (patient.Phone_Ext) {
        if (patient.Phone_Ext[0].trim())
            person.phone[0].phoneNumber += " ext " + patient.Phone_Ext[0].trim();
    }
    if (patient.Work_Phone) {
        person.phone.push({
            "phoneType_id": bonSecoursAttrId("work", "phoneType"),
            "phoneNumber": patient.Work_Phone[0].trim()
        });
    }
    if (patient.Work_Phone_Ext) {
        if (patient.Work_Phone_Ext[0].trim())
            person.phone[1].phoneNumber += " ext " + patient.Work_Phone_Ext[0].trim();
    }

    return person;
}

function respond (resObj, data, send, err) {

    /* reset globals */
    processed = {
        "persons":0,
        "screenings":0
    };
    filename = {
        "originalName":"",
        "serverName":""
    };

    if (send && err)
        resObj = response.respond(resObj, data, true, true);
    else
        resObj = response.respond(resObj, data);

}

function createXMLUploadHistoryDoc (resObj) {
    var xmlUploadHistory = {
        "filename": filename,
        "date_created": moment().format(),
        "user_id": resObj.res.req.session.user_id,
        "processed": processed,
        "committedOn": "",
        "client_id": "",
        "type": "xmlUploadHistory",
        "channels": [
            "ssd"
        ]
    };

    if (resObj.res.req.body.client_id)
        xmlUploadHistory.client_id = resObj.res.req.body.client_id;

    return xmlUploadHistory;
}

function createDebugDoc (functionName, data, message, resObj, callback) {

    var debugDoc = {
        "date_created": moment().format(),
        "functionName": functionName,
        "data":data,
        "message":message,
        "user_id":resObj.res.req.session.user_id,
        "filename":filename,
        "type":"debug"
    };

    var id = uuid.v1();

    createDoc(id, debugDoc, resObj, function () {
        callback();
    });
}

function createDoc (id, doc, resObj, callback) {

    /* creating doc in Couchbase */
    couchActions.createDoc(id, doc, function (err, res) {

        if (err) {
            createDebugDoc("createDoc", doc, err.message, resObj, function() {
                callback();
            });
        //} else  {
        //
        //    if (doc.type == "person") {
        //
        //        create.createHouseholdMemberSelf(id, resObj, function () {
        //            callback();
        //        })
            } else callback();
        //}
    });
}

function updateDoc (id, doc, resObj, callback) {

    /* creating doc in Couchbase */
    couchActions.updateDoc(id, doc, function (err, res) {

        if (err) {
            createDebugDoc("updateDoc", doc, err.message, resObj, function() {
                callback();
            });
        } else callback();
    });
}

function deleteDocsOfSameFileName (resObj, callback) {

    var query = "DELETE FROM deco_db x " +
        "WHERE (x.type = 'debug' OR x.type = 'tempPerson' " +
        "OR x.type = 'tempScreening' OR x.type = 'xmlUploadHistory') " +
        "AND x.user_id = '" + resObj.res.req.session.user_id  + "' " +
        "AND x.filename.originalName = '" + filename.originalName + "'";

    couchActions.n1qlRequest(query, function (err, res) {

        if (err) {
            respond(resObj, {err:err, code:"XI8"});
            return;
        }

        callback();
    });
}

function canRespond (dictionary) {

    for (var i = 0; i < dictionary.length; i++) {
        if (dictionary[i].complete == false)
            return false;
    }

    return true;
}

function admComplete (adm, dictionary, resObj) {

    adm.complete = true;

    /* Checks every dictionary index to see if all responses have been made */
    if (canRespond(dictionary)) {

        /* set and create xmlUploadHistory Doc */
        var xmlUploadHistoryDoc = createXMLUploadHistoryDoc(resObj);

        var id = uuid.v1();

        createDoc(id, xmlUploadHistoryDoc, resObj, function() {

            respond(resObj, {"filename":filename.originalName});
        });
    }
}

function createScreening(adm, patientID, dictionary, resObj) {

    var screening = adm.screening;

    /* add screening information */
    screening.person_id = patientID;

    doesScreeningExist(screening, function (id) {

        /* if screeningDoc does not exist in Couchbase */
        if (id == -1) {

            /* new ID */
            var screeningID = uuid.v1();

            /* creating screeningDoc in Couchbase */
            createDoc(screeningID, screening, resObj, function () {

                processed.screenings++;

                admComplete(adm, dictionary, resObj);
            });
        } else admComplete(adm, dictionary, resObj);
    });
}

function createPatient (adm, guarantorID, dictionary, resObj) {


    var xmlPatient = adm.person;

    doesPersonExist(xmlPatient, resObj, function (patient) {

        var patientID = patient.id;

        /* if personDoc does not exist in Couchbase */
        if (patient == -1) {

            /* new ID */
            patientID = uuid.v1();

            /* add guarantor information */
            if (guarantorID) {

                if (guarantorID == "self") {
                    xmlPatient.guarantor.person_id = patientID;
                    xmlPatient.guarantor.relation_id = bonSecoursAttrId("self", "relation");
                } else {
                    xmlPatient.guarantor.person_id = guarantorID;
                    xmlPatient.guarantor.relation_id = bonSecoursAttrId("unknown", "relation");
                }
            }

            /* creating personDoc in Couchbase */
            createDoc(patientID, xmlPatient, resObj, function () {

                processed.persons++;

                createScreening(adm, patientID, dictionary, resObj);

            });
        } else if (patient == -2) {
            /* not enough information on patient */
            admComplete(adm, dictionary, resObj);
        } else {
            /* real person exists */
            createScreening(adm, patientID, dictionary, resObj);
        }
    });

}

function createGuarantor (adm, dictionary, resObj) {

    var samePatient = samePersonCheck(adm.person,adm.guarantor);

    if (samePatient) {

        adm.person = samePatient;
        createPatient(adm, "self", dictionary, resObj)

    } else {

        var guarantor = adm.guarantor;

        doesPersonExist(guarantor, resObj, function (guar) {

            /* if personDoc does not exist in Couchbase AND makes sure the doc is not empty */
            if ((guar == -1) && (guarantor.nameFirst != "" && guarantor.nameLast != "")) {

                /* new ID */
                var guarantorID = uuid.v1();

                /* creating personDoc (guarantor) in Couchbase */
                createDoc(guarantorID, guarantor, resObj, function () {

                    processed.persons++;

                    createPatient(adm, guarantorID, dictionary, resObj);
                });
            } else createPatient(adm, "", dictionary, resObj);
        });
    }

}


function processAdmDict (dictionary, resObj) {

    if (dictionary.length !== 0) {
        for (var i = 0; i < dictionary.length; i++) {
            createGuarantor(dictionary[i], dictionary, resObj);
        }
    } else respond(resObj, {"XmlUploadComplete":"No records in the dictionary"});
}

function generateAdmDict (xml, resObj) {

    var dictionary = [];
    var client_id = "";

    if (resObj.res.req.body.client_id)
        client_id = resObj.res.req.body.client_id;

    for (var i = 0; i < xml.Vendor.length; i++) {

        var currentVendor = xml.Vendor[i];

        if (currentVendor.Placement) {

            for (var j = 0; j < currentVendor.Placement.length; j++) {

                var currentPlacement = currentVendor.Placement[j];

                /* generating respective Documents */
                var user_id = resObj.res.req.session.user_id;
                var guarantor = generatePersonDoc(currentPlacement, "Responsible", client_id, user_id);
                var person = generatePersonDoc(currentPlacement, "Patient", client_id, user_id);
                var screening = generateScreeningDoc(currentPlacement, client_id, user_id);

                /* updating id dictionary */
                dictionary.push({"guarantor":guarantor, "person":person, "screening":screening, "complete":false});
            }
        }
    }
    processAdmDict(dictionary, resObj);
}

function setUpRequests (xml, resObj) {

    if (resObj.res.req.body.client_id) {

        deleteDocsOfSameFileName(resObj, function () {

            var query = "SELECT META(x).id AS id, x.* " +
                "FROM deco_db x " +
                "WHERE x.type = 'list' AND " +
                "(x.listName = 'gender' OR x.listName = 'relationshipStatus' " +
                "OR x.listName = 'state' OR x.listName = 'country' " +
                "OR x.listName = 'phoneType' OR x.listName = 'relation')";

            couchActions.n1qlRequest(query, function (err, res) {

                if (err) {
                    respond(resObj, {err:err, code:"XI9"});
                    return;
                }

                listNames = res;
                generateAdmDict(xml, resObj)
            });
        });

    } else response.respond(resObj, {"Unsuccessful":"We need a client_id to import documents"});
}

var xml2jsObj = function (originalFileName, xmlFileName, resObj) {

    var parser = new xml2js.Parser();

    fs.readFile(config.pathToParentFolder + 'deco_uploads/' + xmlFileName, function(err, data) {
       parser.parseString(data, function (err, result) {

           filename.originalName = originalFileName;
           filename.serverName = xmlFileName;

           setUpRequests(result.Data, resObj);
       });
    });
};


/* PUBLIC API */
module.exports.xml2js = xml2jsObj;
module.exports.commitTempData = commitTempData;
module.exports.discardTempData = discardTempData;