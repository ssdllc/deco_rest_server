var async = require('async'),
    couchActions = require('../requests/couchActions.js'),
    response = require('../helpers/response.js'),
    idsToValues = require('../helpers/idsToValues.js');

function getData (program_id, callback) {

    var programQuery =
        "SELECT META(program).id AS id, program.state_id, program.description, " +
        "program.title, program.category_id, program.dateStart, program.dateEnd, " +
        "program.client_ids, program.applicationDescription, program.applicationWebsites, clients.name clientTItle" +
        "FROM deco_db program " +
        "LEFT JOIN deco_db clients ON KEYS program.client_ids[0] " +
        "WHERE program.type = 'program' " +
        "AND META(program).id = '" + program_id + "'";

    var eligibilityQuery =
        "SELECT META(elig).id AS id, elig.conditionType, elig.conditions " +
        "FROM deco_db elig " +
        "WHERE elig.type = 'eligibilityObject' " +
        "AND elig.program_id = '" + program_id + "'";

    var incomeAssetQuery =
        "SELECT META(incomeAsset).id AS id, incomeAsset.type, incomeAsset.baseline_id, " +
        "incomeAsset.doesCount, incomeAsset.limitType, incomeAsset.limits, incomeAsset.relationshipType_ids " +
        "FROM deco_db incomeAsset " +
        "WHERE incomeAsset.criteriaType = 'incomeAssetsEligibility' " +
        "AND incomeAsset.program_id = '" + program_id + "'";

    async.parallel({
        program: function(callback){
            couchActions.n1qlRequest(programQuery, function(err, res) {
                if (err) {
                    callback({err:err, code:"PROR1"}, null);
                    return;
                }
                callback(err, res[0]);
            });
        },
        eligibilityObject: function(callback){
            couchActions.n1qlRequest(eligibilityQuery, function(err, res) {
                if (err) {
                    callback({err:err, code:"PROR2"}, null);
                    return;
                }
                callback(err, res[0]);
            });
        },
        incomeAsset: function(callback){
            couchActions.n1qlRequest(incomeAssetQuery, function(err, res) {
                if (err) {
                    callback({err:err, code:"PROR3"}, null);
                    return;
                }
                callback(err, res);
            });
        }
    }, function(err, res) {
        callback(err, res);
    });
}

var getReview = function (req, resObj) {

    var program_id = req.body.program_id;

    getData(program_id, function (err, res) {

        if (err) {
            resObj = response.respond(resObj, err);
            return;
        }

        idsToValues.replaceJSON(res, function(err, res) {

            if (err) {
                resObj = response.respond(resObj, err);
                return;
            }

            resObj = response.respond(resObj, res);
        });
    });
};

/* PUBLIC API */
module.exports = {
    getReview: getReview
};