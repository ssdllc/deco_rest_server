var conditionDoc = require('./conditionDoc.js');
var incomeAndAssets = require('./incomeAndAssets.js');
var couchActions = require('../../requests/couchActions.js');
var db = require('../../requests/db.js');
const Rx = require('rx');
const getDoc = Rx.Observable.fromNodeCallback(couchActions.getDoc);
const n1qlRequest = Rx.Observable.fromNodeCallback(couchActions.n1qlRequest);
const db_getDoc = Rx.Observable.fromNodeCallback(db.getDoc);
const db_n1qlRequest = Rx.Observable.fromNodeCallback(db.n1qlRequest);

function checkEligibilities (screeningDoc, personDoc, householdDocs, aANDiDocs, baselineDictionary, dictionary, currentElig, eligReturnObj) {

    var isElig;

    var aANDiEligibilityStatus = incomeAndAssets.testIncomeAndAssets(householdDocs, aANDiDocs, baselineDictionary, dictionary, currentElig.program_id);

    if (aANDiEligibilityStatus) {
        var programEligibilityStatus = conditionDoc.testConditions(currentElig, personDoc, screeningDoc);

        switch (programEligibilityStatus) {
            case "NMI":
                isElig = "NMI";
                break;
            case true:
                isElig = "E";
                break;
            case false:
                isElig = "I";
                break;
            default:
                console.log('ERROR elig programs list');
        }
    } else {
        isElig = "I";
    }

    // if program is ineligible or needs more information
    if (isElig === "I" || isElig === "NMI") {

        // if federal program "all states" id OR person's state_id is empty then push
        if (currentElig.list_id === "45796ef0-e3da-11e5-9824-97df5e36f9db" || personDoc.address[0].state_id == "") {
            eligReturnObj[isElig].push({"title":currentElig.programTitle, "program_id":currentElig.program_id});
        }
        // if person is in the same state as program then push
        else if (personDoc.address[0].state_id === currentElig.programState_id)
            eligReturnObj[isElig].push({"title":currentElig.programTitle, "program_id":currentElig.program_id});

    } else {
        eligReturnObj[isElig].push({"title":currentElig.programTitle, "program_id":currentElig.program_id});
    }
}

function iterateProgramsForEligibility (screeningDoc, eligDocs, personDoc, householdDocs, aANDiDocs, baselineDictionary, incomeAssetsEligibilityDocs, callback) {

    var eligReturnObj = {"E":[],"I":[],"NMI":[]};

    /* creating dictionary for all income and asset eligibility information */
    var dictionary = {};
    for (var i = 0; i < incomeAssetsEligibilityDocs.length; i++) {
        dictionary[incomeAssetsEligibilityDocs[i].program_id + "," + incomeAssetsEligibilityDocs[i].type] = incomeAssetsEligibilityDocs[i];
    }

    /* looping through each program's eligibility */
    for (var j = 0; j < eligDocs.length; j++) {

        var currentElig = eligDocs[j];

        checkEligibilities(screeningDoc, personDoc, householdDocs, aANDiDocs, baselineDictionary, dictionary,
            currentElig, eligReturnObj);
    }

    callback(eligReturnObj);
}

function createBaselineDictionary (baselineDocs) {

    var baselineDictionary = {};
    for (var i = 0; i < baselineDocs.length; i++) {
        baselineDictionary[baselineDocs[i].id] = baselineDocs[i];
    }
    return baselineDictionary;
}

var requestInformation = function (screening_id, callback) {

    couchActions.getDoc(screening_id, function (err, res) {

        if (err) {
            callback({err:err, code:"EPL1"});
            return;
        }

        var screeningDoc = res.value;
        var person_id = screeningDoc.person_id;
        var eligibilityQuery = "SELECT META(x).id AS id, x.*, pro.title AS programTitle, pro.state_id AS programState_id, META(li).id  AS list_id"
            + " FROM deco_db x"
            + " JOIN deco_db pro ON KEYS x.program_id"
            + " JOIN deco_db li ON KEYS pro.state_id"
            + " WHERE x.type = 'eligibilityObject'"
            + " ORDER BY x.program_id";
        var householdQuery =
            "SELECT META(x).id AS id, x.relationshipType_id " +
            "FROM deco_db x " +
            "WHERE x.type = 'householdMember' " +
            "AND x.person_id = '" + person_id + "'";
        var assetsAndIncomeQuery =
            "SELECT h.relationshipType_id, META(x).id AS id, x.type, x.amount, x.assetType_id, x.incomeType_id " +
            "FROM deco_db x " +
            "JOIN deco_db h ON KEYS x.householdMember_id " +
            "WHERE (x.type = 'asset' OR x.type = 'income') " +
            "AND h.type = 'householdMember' " +
            "AND h.person_id = '" + person_id + "'";
        var baselineQuery =
            "SELECT META(x).id AS id, x.* " +
            "FROM deco_db x " +
            "WHERE x.type = 'baseline'";
        var incomeAssetsEligibilityQuery =
            "SELECT META(x).id AS id, x.* " +
            "FROM deco_db x " +
            "WHERE x.criteriaType = 'incomeAssetsEligibility'";

        var s1 = n1qlRequest(eligibilityQuery);
        var s2 = getDoc(person_id);
        var s3 = n1qlRequest(householdQuery);
        var s4 = n1qlRequest(assetsAndIncomeQuery);
        var s5 = n1qlRequest(baselineQuery);
        var s6 = n1qlRequest(incomeAssetsEligibilityQuery);


        var combination = Rx.Observable.zip(s1, s2, s3, s4, s5, s6, function (s1, s2, s3, s4, s5, s6) {
            return {
                eligDocs:s1,
                personDoc:s2.value,
                householdDocs:s3,
                aANDiDocs:s4,
                baselineDocs:s5,
                incomeAssetsEligibilityDocs:s6
            };
        });

        var finishedObj = {};

        var logObserver = Rx.Observer.create(

            function (result) {
                finishedObj = result;
            },

            function (err) {
                callback({err:err, code:"EPL2"});
            },

            function () {

                var baselineDictionary = createBaselineDictionary(finishedObj.baselineDocs);

                iterateProgramsForEligibility
                (
                    screeningDoc,
                    finishedObj.eligDocs,
                    finishedObj.personDoc,
                    finishedObj.householdDocs,
                    finishedObj.aANDiDocs,
                    baselineDictionary,
                    finishedObj.incomeAssetsEligibilityDocs,
                    callback
                );
            }
        );
        combination.subscribe(logObserver);
    });
};

var requestInformationScreeningSummaryVersion = function (screening_id, callback) {

    couchActions.getDoc(screening_id, function (err, res) {

        if (err) {
            callback({err:err, code:"EPL3"});
            return;
        }

        var screeningDoc = res.value;
        var person_id = screeningDoc.person_id;
        var eligibilityQuery = "SELECT META(x).id AS id, x.*, pro.title AS programTitle, pro.state_id AS programState_id, META(li).id  AS list_id"
            + " FROM deco_db x"
            + " JOIN deco_db pro ON KEYS x.program_id"
            + " JOIN deco_db li ON KEYS pro.state_id"
            + " WHERE x.type = 'eligibilityObject'"
            + " ORDER BY x.program_id";
        var householdQuery =
            "SELECT META(x).id AS id, x.relationshipType_id " +
            "FROM deco_db x " +
            "WHERE x.type = 'householdMember' " +
            "AND x.person_id = '" + person_id + "'";
        var assetsAndIncomeQuery =
            "SELECT h.relationshipType_id, META(x).id AS id, x.type, x.amount, x.assetType_id, x.incomeType_id " +
            "FROM deco_db x " +
            "JOIN deco_db h ON KEYS x.householdMember_id " +
            "WHERE (x.type = 'asset' OR x.type = 'income') " +
            "AND h.type = 'householdMember' " +
            "AND h.person_id = '" + person_id + "'";
        var baselineQuery =
            "SELECT META(x).id AS id, x.* " +
            "FROM deco_db x " +
            "WHERE x.type = 'baseline'";
        var incomeAssetsEligibilityQuery =
            "SELECT META(x).id AS id, x.* " +
            "FROM deco_db x " +
            "WHERE x.criteriaType = 'incomeAssetsEligibility'";

        var s1 = db_n1qlRequest(eligibilityQuery);
        var s2 = db_getDoc(person_id);
        var s3 = db_n1qlRequest(householdQuery);
        var s4 = db_n1qlRequest(assetsAndIncomeQuery);
        var s5 = db_n1qlRequest(baselineQuery);
        var s6 = db_n1qlRequest(incomeAssetsEligibilityQuery);


        var combination = Rx.Observable.zip(s1, s2, s3, s4, s5, s6, function (s1, s2, s3, s4, s5, s6) {
            return {
                eligDocs:s1,
                personDoc:s2.value,
                householdDocs:s3,
                aANDiDocs:s4,
                baselineDocs:s5,
                incomeAssetsEligibilityDocs:s6
            };
        });

        var finishedObj = {};

        var logObserver = Rx.Observer.create(

            function (result) {
                finishedObj = result;
            },

            function (err) {
                callback({err:err, code:"EPL4"});
            },

            function () {

                var baselineDictionary = createBaselineDictionary(finishedObj.baselineDocs);

                iterateProgramsForEligibility
                (
                    screeningDoc,
                    finishedObj.eligDocs,
                    finishedObj.personDoc,
                    finishedObj.householdDocs,
                    finishedObj.aANDiDocs,
                    baselineDictionary,
                    finishedObj.incomeAssetsEligibilityDocs,
                    callback
                );
            }
        );
        combination.subscribe(logObserver);
    });
};

/* PUBLIC API */
module.exports.buildEligibilityProgramsList = requestInformation;
module.exports.buildEligibilityProgramsListScreeningSummaryVersion = requestInformationScreeningSummaryVersion;
