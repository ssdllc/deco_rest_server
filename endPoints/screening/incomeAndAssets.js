var determineLimitFromBaseline = function (familySizeIncomeLimit, familySize, baselineDoc, max, incomeOrAsset) {

    //sort baselineDoc.amounts[] for familySize, this should really happen on CREATE OR UPDATE of DOC
    baselineDoc.amounts.sort(function(a, b){
        return a.familySize-b.familySize;
    });

    //divide by 12 for monthly if we are testing income
    function monthlyOrNot (amount) {
        //if (incomeOrAsset === "income")
        //    return amount / 12;
        //else return amount;

        return amount;
    }

    function setLimits (currentBaseline) {
        familySizeIncomeLimit.min = 0;
        familySizeIncomeLimit.max = monthlyOrNot(currentBaseline.amount) * (max/100);
    }

    //var currentBaseline = baselineDoc.amounts[baselineDoc.amounts.length-1];

    var currentBaseline;

    // if familySize is higher then baselineDoc familySize, take highest baselineDoc familySize *see above line*
    //if (familySize > currentBaseline.familySize) {
    //    setLimits(currentBaseline);
    //}
    // else loop until we find a match
   // else {
        for (var i = 0; i < baselineDoc.amounts.length; i++) {

            currentBaseline = baselineDoc.amounts[i];
            if (currentBaseline.familySize === familySize) {
                setLimits(currentBaseline);
                return familySizeIncomeLimit;
            }
        }

        // if we can't find the baselineDoc familySize assume highest familySize
        currentBaseline = baselineDoc.amounts[baselineDoc.amounts.length-1];
        setLimits(currentBaseline);
  //  }
    return familySizeIncomeLimit;
};

var calculateFamilySizeAssetLimit_7 = function (familySize, dictionary, program_id) {

    // type = familySizeIncomeLimit
    // find limit for familySize as calculated in step 1
    // familySizeIncomeLimit is product

    var familySizeAssetLimit = { "min":0, "max":9999999999999999 };
    var familySizeAssetLimitDoc = dictionary[program_id + ",familySizeAssetLimits"];

    for (var i = 0; i < familySizeAssetLimitDoc.limits.length; i++) {

        var currentLimit = familySizeAssetLimitDoc.limits[i];
        if (currentLimit.familySize == familySize) {

            familySizeAssetLimit.min = currentLimit.min;
            familySizeAssetLimit.max = currentLimit.max;
        }
    }
    return familySizeAssetLimit;
};

var calculateDisregardThroughDoesCountLoop = function (totalDisregard, disregardDoc, aANDiDoc, type) {

    var typeId = type + "Type_id";

    /* doesCount loop */
    for (var k = 0; k < disregardDoc.doesCount.length; k++) {

        var currentDoesCount = disregardDoc.doesCount[k];
        if (currentDoesCount.relationshipType_id == aANDiDoc.relationshipType_id
            && currentDoesCount[typeId] == aANDiDoc[typeId]) {

            if (currentDoesCount.fixedOrPercent == "percent")
                totalDisregard += aANDiDoc.amount * (currentDoesCount.amount/100);
            else {
                // if aANDiDoc.amount is less than or equal to disregard then use aANDiDoc.amount as disregard value
                if (aANDiDoc.amount <= currentDoesCount.amount)
                    totalDisregard += aANDiDoc.amount;
                // if currentDisregard is less than aANDiDoc.amount then use currentDisregard as disregard value
                else
                    totalDisregard += currentDoesCount.amount;
            }
            break;
        }
    }
    return totalDisregard;
};

var calculateAssetDisregard_6 = function (aANDiDocs, dictionary, program_id) {

    /* look at type = assetDisregard and total up asset Disregard amount */

    var totalAssetsDisregards = {
        disregard: 0,
        overallDisregard: {
            fixedOrPercent: "",
            amount: 0
        }
    };

    if (dictionary[program_id + ",assetDisregards"]) {
        var totalAssetsDisregardDoc = dictionary[program_id + ",assetDisregards"];

        /* income and assets loop */
        for (var i = 0; i < aANDiDocs.length; i++) {

            if (aANDiDocs[i].type == "asset") {

                var currentAssetsDoc = aANDiDocs[i];

                totalAssetsDisregards.disregard += calculateDisregardThroughDoesCountLoop(totalAssetsDisregards.disregard,
                    totalAssetsDisregardDoc, currentAssetsDoc, "asset");
            }
        }

        totalAssetsDisregards.overallDisregard.fixedOrPercent = totalAssetsDisregardDoc.overallFixedOrPercent;
        if (totalAssetsDisregardDoc.overallAmount)
            totalAssetsDisregards.overallDisregard.amount = totalAssetsDisregardDoc.overallAmount;
    }
    return totalAssetsDisregards;
};

var calculateTotalAssets_5 = function (aANDiDocs, dictionary, program_id) {

    /* if relationship and assetType combination exists in type = totalAssets then add it */

    var totalAssets = 0;
    var totalAssetsDoc = dictionary[program_id + ",totalAssets"];

    /* income and assets loop */
    for (var i = 0; i < aANDiDocs.length; i++) {

        if (aANDiDocs[i].type == "asset") {

            var currentAssetDoc = aANDiDocs[i];

            /* doesCount loop */
            for (var k = 0; k < totalAssetsDoc.doesCount.length; k++) {

                var currentDoesCount = totalAssetsDoc.doesCount[k];
                if (currentDoesCount.relationshipType_id == currentAssetDoc.relationshipType_id
                    && currentDoesCount.assetType_id == currentAssetDoc.assetType_id) {

                    //TC MADE THIS CHANGE 9/7/2015
                    totalAssets += currentAssetDoc.amount; // totalAssetsDoc.amount;
                    break;
                }
            }
        }
    }
    return totalAssets;
};

var test_4t7t = function (familySize, total, disregards) {

    // if familySizeIncomeLimit > totalIncome - totalIncomeDisregard then eligible else ineligible
    // if familySizeAssetLimit > totalAssets - totalAssetsDisregard then eligible else ineligible

    var final;

    if (disregards.overallDisregard.fixedOrPercent === "percent")
        final = total - (disregards.disregard + (total * (disregards.overallDisregard.amount / 100)));
    else
        final = total - (disregards.disregard + disregards.overallDisregard.amount);

    if (final < 0)
        final = 0;

    if (familySize.min <= final && familySize.max >= final)
        return true;
    else return false;
};

var calculateFamilySizeIncomeLimit_4 = function (familySize, dictionary, baselineDictionary, program_id) {

    // type = familySizeIncomeLimit
    // find limit for familySize as calculated in step 1
    // familySizeIncomeLimit is product

    var familySizeIncomeLimit = { "min":0, "max":9999999999999999 };
    var familySizeIncomeLimitDoc = dictionary[program_id + ",familySizeIncomeLimits"];

    if (familySizeIncomeLimitDoc.limitType === "baseline") {

        var limit = familySizeIncomeLimitDoc.limits[0].max;

        familySizeIncomeLimit = determineLimitFromBaseline(familySizeIncomeLimit, familySize,
            baselineDictionary[familySizeIncomeLimitDoc.baseline_id], limit, "income");

    } else {
        for (var i = 0; i < familySizeIncomeLimitDoc.limits.length; i++) {

            var currentLimit = familySizeIncomeLimitDoc.limits[i];
            if (currentLimit.familySize === familySize) {

                familySizeIncomeLimit.min = currentLimit.min;
                familySizeIncomeLimit.max = currentLimit.max;
                break;
            }
        }
    }
    return familySizeIncomeLimit;
};

var calculateIncomeDisregards_3 = function (aANDiDocs, dictionary, program_id) {

    /* look at type = incomeDisregard and total up income Disregard amount */

    var totalIncomeDisregards = {
        disregard: 0,
        overallDisregard: {
            fixedOrPercent: "",
            amount: 0
        }
    };

    if(dictionary[program_id + ",incomeDisregards"]) {

        var totalIncomeDisregardDoc = dictionary[program_id + ",incomeDisregards"];

        /* income and assets loop */
        for (var i = 0; i < aANDiDocs.length; i++) {

            if (aANDiDocs[i].type == "income") {

                var currentIncomeDoc = aANDiDocs[i];

                totalIncomeDisregards.disregard += calculateDisregardThroughDoesCountLoop(totalIncomeDisregards.disregard,
                    totalIncomeDisregardDoc, currentIncomeDoc, "income");
            }
        }

        totalIncomeDisregards.overallDisregard.fixedOrPercent = totalIncomeDisregardDoc.overallFixedOrPercent;
        if (totalIncomeDisregardDoc.overallAmount)
            totalIncomeDisregards.overallDisregard.amount = totalIncomeDisregardDoc.overallAmount;
    }
    return totalIncomeDisregards;
};

var calculateTotalIncome_2 = function (aANDiDocs, dictionary, program_id) {

    /* if relationship and incomeType combination exists in type = totalIncome then add it */

    var totalIncome = 0;
    var totalIncomeDoc = dictionary[program_id + ",totalIncome"];

    /* income and assets loop */
    for (var i = 0; i < aANDiDocs.length; i++) {

        if (aANDiDocs[i].type == "income") {

            var currentIncomeDoc = aANDiDocs[i];

            /* doesCount loop */
            for (var k = 0; k < totalIncomeDoc.doesCount.length; k++) {

                var currentDoesCount = totalIncomeDoc.doesCount[k];
                if (currentDoesCount.relationshipType_id == currentIncomeDoc.relationshipType_id
                    && currentDoesCount.incomeType_id == currentIncomeDoc.incomeType_id) {

                    totalIncome += currentIncomeDoc.amount;
                    break;
                }
            }
        }
    }
    return totalIncome;
};

var calculateFamilySize_1 = function (householdDocs, dictionary, program_id) {

    // get all householdMembers for person
    // count householdMembers that have a relation in type = familySize

    var familySize = 0;
    var relationIdsArray = dictionary[program_id + ",familySize"].relationshipType_ids;

    for (var i = 0; i < householdDocs.length; i++) {

        var householdMember = householdDocs[i];
        for (var j = 0; j < relationIdsArray.length; j++) {
            if (householdMember.relationshipType_id == relationIdsArray[j])
                familySize++;
        }
    }
    return familySize;
};

function incomeAssetsEligibilityCriteriaExists (dictionary, program_id, type) {

    if (type == "familySize") {
        if (dictionary.hasOwnProperty(program_id + ",familySize"))
            return true;
    }
    if (type == "income") {
        if (dictionary.hasOwnProperty(program_id + ",totalIncome")
            //&& dictionary.hasOwnProperty(program_id + ",incomeDisregards")
            && dictionary.hasOwnProperty(program_id + ",familySizeIncomeLimits")) {

            return true;
        }
    } else if (type == "assets") {
        if (dictionary.hasOwnProperty(program_id + ",totalAssets")
            //&& dictionary.hasOwnProperty(program_id + ",assetDisregards")
            //&& dictionary.hasOwnProperty(program_id + ",coupleStatus")
            && dictionary.hasOwnProperty(program_id + ",familySizeAssetLimits")) {

            return true;
        }
    }
    return false;
}

function testAssets (fs, aANDiDocs, dictionary, program_id) {

    if (incomeAssetsEligibilityCriteriaExists(dictionary, program_id, "assets")) {

        /* fs = familySize, ta = totalAssets */
        var ta = calculateTotalAssets_5(aANDiDocs, dictionary, program_id);
        var taDisregard = calculateAssetDisregard_6(aANDiDocs, dictionary, program_id);
        var fsAssetLimit = calculateFamilySizeAssetLimit_7(fs, dictionary, program_id);

        /* test eligible or ineligible */
        return test_4t7t(fsAssetLimit, ta, taDisregard);

    } else return true; //eligible no criteria to test
}

function testIncome (fs, aANDiDocs, dictionary, baselineDictionary, program_id) {

    if (incomeAssetsEligibilityCriteriaExists(dictionary, program_id, "income")) {

        /* fs = familySize, ti = totalIncome */
        var ti = calculateTotalIncome_2(aANDiDocs, dictionary, program_id);
        var tiDisregard = calculateIncomeDisregards_3(aANDiDocs, dictionary, program_id);
        var fsIncomeLimit = calculateFamilySizeIncomeLimit_4(fs, dictionary, baselineDictionary, program_id);

        /* test eligible or ineligible */
        return test_4t7t(fsIncomeLimit, ti, tiDisregard);

    } else return true; //eligible no criteria to test
}

var getFamilySize = function (householdDocs, dictionary, program_id) {

    if (incomeAssetsEligibilityCriteriaExists(dictionary, program_id, "familySize")) {

        /* fs = familySize */
        var fs = calculateFamilySize_1(householdDocs, dictionary, program_id);
        return fs;
    } else return true; //eligible no criteria to test
};

var testIncomeAndAssets = function (householdDocs, aANDiDocs, baselineDictionary, dictionary, program_id) {

    /* fs = familySize */
    var fs = getFamilySize(householdDocs, dictionary, program_id);

    var incomeResults = testIncome(fs, aANDiDocs, dictionary, baselineDictionary, program_id);
    var assetsResults = false;

    /* if person passes test OR there is no income information - do assets */
    /* if person fails test AND there is income information - return false */
    if (incomeResults) {
        assetsResults = testAssets(fs, aANDiDocs, dictionary, program_id);
    }

    return (incomeResults && assetsResults);
};

/* PUBLIC API */
module.exports.determineLimitFromBaseline = determineLimitFromBaseline;
module.exports.calculateFamilySizeAssetLimit_7 = calculateFamilySizeAssetLimit_7;
module.exports.calculateAssetDisregard_6 = calculateAssetDisregard_6;
module.exports.calculateTotalAssets_5 = calculateTotalAssets_5;
module.exports.test_4t7t = test_4t7t;
module.exports.calculateFamilySizeIncomeLimit_4 = calculateFamilySizeIncomeLimit_4;
module.exports.calculateIncomeDisregards_3 = calculateIncomeDisregards_3;
module.exports.calculateTotalIncome_2 = calculateTotalIncome_2;
module.exports.calculateFamilySize_1 = calculateFamilySize_1;
module.exports.getFamilySize = getFamilySize;

module.exports.testIncomeAndAssets = testIncomeAndAssets;
