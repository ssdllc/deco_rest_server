var personDocFunc = require('../../helpers/personDocFunc.js');
var conditionDoc = require('./conditionDoc.js');
var couchActions = require('../../requests/couchActions.js');
var response = require('../../helpers/response.js');
const Rx = require('rx');
const getDoc = Rx.Observable.fromNodeCallback(couchActions.getDoc);
const n1qlRequest = Rx.Observable.fromNodeCallback(couchActions.n1qlRequest);

function sort (questionDocs, questionIds) {

    var newQuestionDocs = [];

    for (var i = 0; i < questionIds.length; i++) {
        for (var j = 0; j < questionDocs.length; j++) {
            if (questionDocs[j].id == questionIds[i])
                newQuestionDocs.push(questionDocs[j]);
        }
    }
    return newQuestionDocs;
}

function sortScreeningQuestions (questionsArray) {

    var maxIndex = -1;
    var maxNumberOfRuns = questionsArray.length * 2;

    for (var i = 0; (i < questionsArray.length) && (maxNumberOfRuns > 0); i++) {

        var questionDoc = questionsArray[i];

        /* loop through conditions */
        for (var j = 0; j < questionDoc.conditions.length; j++) {

            var currentCondition = questionDoc.conditions[j];

            /* loop through subconditions */
            for (var k = 0; k < currentCondition.subconditions.length; k++) {

                var currentSubcondition = currentCondition.subconditions[k];

                if (currentSubcondition.source == "question") {

                    var conditionId = currentSubcondition.criteria.question_id;

                    /* we are looping only AFTER current index (i) */
                    for (var l = i+1; l < questionsArray.length; l++) {
                        if (questionsArray[l].id == conditionId) {

                            /* we need this i think because of multiple subconditions and conditions
                            * attempting to set maxIndex to a smaller number */
                            if (maxIndex < l)
                                maxIndex = l;
                        }
                    }
                }
            }
        }

        if (maxIndex > -1) {
            questionsArray.move(i, maxIndex);
            i--;
            maxIndex = -1;
        }

        maxNumberOfRuns--;
    }

    return questionsArray;
}

var questionsCollectionRequest = function (reqObj, resObj) {

    if (reqObj.data[0].hasOwnProperty("wizard_id")) {

        var wizard_id = reqObj.data[0].wizard_id;
        var person_id = reqObj.data[0].person_id;
        var screening_id = reqObj.data[0].screening_id;

        var source1 = getDoc(wizard_id);
        var source2 = getDoc(person_id);
        var source3 = getDoc(screening_id);

        var combination = Rx.Observable.zip(source1, source2, source3, function (s1, s2, s3) {
            return {wizard:s1.value, person:s2.value, screening:s3.value};
        });

        var finishedObj = {};

        var logObserver = Rx.Observer.create(

            function (result) {
                finishedObj = result;
            },

            function (err) {
                resObj = response.respond(resObj, {err:err, code:"QCR1"});
            },

            function () {

                var query = "SELECT META(x).id, x.* FROM deco_db x WHERE META(x).id IN "
                    + JSON.stringify(finishedObj.wizard.question_ids);

                couchActions.n1qlRequest(query, function(err, res) {

                    if (err) {
                        resObj = response.respond(resObj, {err:err, code:"QCR4"});
                        return;
                    }

                    var questionDocs = res;
                    var questionDocs = sort(questionDocs, finishedObj.wizard.question_ids);

                    var questionsArray = [];

                    for (var i = 0; i < questionDocs.length; i++) {

                        var conditions = true;

                        //var conditions;

                        //if (screeningDoc.displayConditions == true)
                        //    conditions = conditionDoc.testConditions(questionDocs[i], personDoc, screeningDoc);
                        //else
                        //    conditions = true;

                        /* true and "NMI" go in false does not */
                        if (conditions) {
                            questionsArray.push(questionDocs[i]);
                        }
                    }

                    resObj = response.respond(resObj, {"data":questionsArray});

                });
            }
        );
        combination.subscribe(logObserver);
    }
    else if (reqObj.data[0].hasOwnProperty("program_id")) {

        var person_id = reqObj.data[0].person_id;
        var screening_id = reqObj.data[0].screening_id;
        var eligibilityQuery = "SELECT META(x).id AS id, x.* FROM deco_db x WHERE x.type = 'eligibilityObject' AND x.program_id = '"
            + reqObj.data[0].program_id + "'";

        var source1 = getDoc(person_id);
        var source2 = getDoc(screening_id);
        var source3 = n1qlRequest(eligibilityQuery);

        var combination = Rx.Observable.zip(source1, source2, source3, function (s1, s2, s3) {
            return {person:s1.value, screening:s2.value, eligibility:s3};
        });

        var finishedObj = {};

        var logObserver = Rx.Observer.create(

            function (result) {
                finishedObj = result;
            },

            function (err) {
                resObj = response.respond(resObj, {err:err, code:"QCR5"});
            },

            function () {

                var questionIds = [];
                var critObjs = [];

                for (var i = 0; i < finishedObj.eligibility.length; i++) {

                    var currentElig = finishedObj.eligibility[i];

                    for (var j = 0; j < currentElig.conditions.length; j++) {

                        var currentCondition = currentElig.conditions[j];

                        for (var k = 0; k < currentCondition.subconditions.length; k++) {

                            var currentSubcondtion = currentCondition.subconditions[k];

                            /* push question_ids */
                            if (currentSubcondtion.source == "question") {
                                if (questionIds.indexOf(currentSubcondtion.criteria.question_id) == -1)
                                    questionIds.push(currentSubcondtion.criteria.question_id);
                            }
                            /* push criteria object */
                            else if (currentSubcondtion.source == "person") {

                                if (!(reqObj.data[0].criteria == false)) {

                                    var criteria = currentSubcondtion.criteria;
                                    if (critObjs.indexOf(criteria) == -1) {
                                        /* if person does not have this criteria attribute push it to client */
                                        var attr = personDocFunc.getPersonAttribute(finishedObj.person, criteria.attributeName);
                                        if ((attr == "" || attr == null) && criteria.attributeType != "boolean")
                                            critObjs.push(JSON.stringify(criteria));
                                    }
                                }
                            }
                        }
                    }
                }

                var query = "SELECT META(x).id, x.* FROM deco_db x WHERE META(x).id IN "
                    + JSON.stringify(questionIds) + " ORDER BY META(x).id";

                couchActions.n1qlRequest(query, function(err, res) {

                    if (err) {
                        resObj = response.respond(resObj, {err:err, code:"QCR8"});
                        return;
                    }

                    var questionDocs = res;
                    var questionsArray = [];

                    for (var l = 0; l < questionDocs.length; l++) {

                        var conditions = true;

                        //var conditions;

                        //if (screeningDoc.displayConditions == true)
                        //    conditions = conditionDoc.testConditions(questionDocs[l], personDoc, screeningDoc);
                        //else
                        //    conditions = true;

                        /* true and "NMI" go in false does not */
                        if (conditions) {
                            questionsArray.push(questionDocs[l]);
                        }
                    }

                    questionsArray = sortScreeningQuestions(questionsArray);

                    /* turning strings into objects */
                    for (var m = 0; m < critObjs.length; m++) {
                        critObjs[m] = JSON.parse(critObjs[m]);
                    }

                    resObj = response.respond(resObj, {"data":[{"questions":questionsArray,"criterion":critObjs}]});

                });
            }
        );
        combination.subscribe(logObserver);
    }
    else
        resObj = response.respond(resObj, "bad parameters!", true, true);
};

/* PUBLIC API */
module.exports.questionsCollectionRequest = questionsCollectionRequest;
