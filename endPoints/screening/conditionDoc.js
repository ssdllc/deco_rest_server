var condition = require('./../../helpers/conditionCheck.js');
var personDocFunc = require('./../../helpers/personDocFunc.js');

var getScreeningResult = function (screeningDocResults, id) {

    for (var i = 0; i < screeningDocResults.length; i++) {
        /* if there is a matching question id and the answers array is not empty */
        if (screeningDocResults[i].question_id == id && screeningDocResults[i].answers.length > 0) {
            return screeningDocResults[i];
        }
    }

    return -1;
};

var testSubConditionEquals = function (currentSubcondition, currentScreeningAnswerArray, currentSubCheck) {

    for (var k = 0; k < currentScreeningAnswerArray.length; k++) {

        if (currentSubcondition.results.answer_id) {

            if (currentScreeningAnswerArray[k].answer_id === currentSubcondition.results.answer_id) {
                return true;
            }
        } else {

            if (currentScreeningAnswerArray[k].answerNumber === currentSubcondition.results.answerNumber) {
                return true;
            }
        }
    }
    return currentSubCheck;
};

var testSubConditionNotEquals = function (currentSubcondition, currentScreeningAnswerArray) {

     return currentScreeningAnswerArray.every( function(obj) {
        //EVERY FUNCTION WILL RETURN TRUE ONLY IF EACH ITERATION RETURNS TRUE
        //AS SOON AS WE FIND A SCREENING ANSWER THAT MATCHES THE "NOT EQUAL TO" ANSWER
        //WE RETURN FALSE AND THE FINAL RESULT FOR THIS SUBCONDITION IS FALSE
        if (currentSubcondition.results.answer_id) {

            if (obj.answer_id !== currentSubcondition.results.answer_id) {
                return true;
            } else {
                return false;
            }

        } else {

            if (obj.answerNumber !== currentSubcondition.results.answerNumber) {
                return true;
            } else {
                return false;
            }
        }
    });
};

var testSubCondition = function (currentSubcondition, currentScreeningAnswerArray, currentSubCheck) {
    var returnVal = currentSubCheck;

    // if we want to test to see if the answerNumbers are equal
    if (currentSubcondition.results.equal === true) {

        returnVal = testSubConditionEquals(currentSubcondition, currentScreeningAnswerArray, currentSubCheck);
    }
    // if we want to test to see if the answerNumbers are NOT equal
    else if (currentSubcondition.results.equal === false) {

        if (currentScreeningAnswerArray.length > 0)
            returnVal = testSubConditionNotEquals(currentSubcondition, currentScreeningAnswerArray);
    }
    return returnVal;
};

var falseCheck = function (check1, check2) {

    /*
     * true --> false
     * true --> NMI
     * NMI --> false
     * */

    if (check1 == true && check2 == false)
        return false;
    else if (check1 == true && check2 == "NMI")
        return "NMI";
    else if (check1 == "NMI" && check2 == false)
        return false;
    else
        return check1;
};

var trueCheck = function (check1, check2) {

    /*
     * false --> true
     * false --> NMI
     * NMI --> true
     * */

    if (check1 == false && check2 == true)
        return true;
    else if (check1 == false && check2 == "NMI")
        return "NMI";
    else if (check1 == "NMI" && check2 == true)
        return true;
    else
        return check1;
};

var checkOrConditions = function (conditionDoc, personDoc, screeningDoc) {

    /* conditionDoc can be either eligibilityObjects or Questions */

    var check = false;

    for(var i = 0; i < conditionDoc.conditions.length; i++) { //or

        var currentCondition = conditionDoc.conditions[i];
        var subCheck = true;

        for(var j = 0; j < currentCondition.subconditions.length; j++) { //and

            var currentSubcondition = currentCondition.subconditions[j];
            var currentSubCheck = true;

            if (currentSubcondition.source == "person") {

                var value1 = personDocFunc.getPersonAttribute(personDoc, currentSubcondition.criteria.attributeName);
                var value2a = currentSubcondition.results.value1;
                var value2b = currentSubcondition.results.value2;
                var comparator = currentSubcondition.results.comparator;

                /* if personDoc does not have enough information assume true, but keep going */
                if (value1 !== "") {
                    if (value1 != null) {
                        currentSubCheck = condition.conditionCheck(value1, value2a, value2b, comparator)

                    } else currentSubCheck = "NMI";
                } else currentSubCheck = "NMI";
            }
            else if (currentSubcondition.source == "question") {
                //GET ANSWERS FROM SCREENING THAT ARE REVELANT TO THIS CONDITION
                var screeningResult = getScreeningResult(screeningDoc.results, currentSubcondition.criteria.question_id);

                if (screeningResult == -1) {
                    currentSubCheck = "NMI"; //NO ANSWERS FOR THIS QUESTION
                }
                else {
                    //WE HAVE AN ANSWER - TEST IT
                    /* if in the else statement currentSubCheck is automatically false unless found to be true */
                    currentSubCheck = false;

                    for (var k = 0; k < screeningResult.answers.length; k++) {

                        var currentScreeningAnswerArray = screeningResult.answers;
                        if (currentScreeningAnswerArray.length) {

                            currentSubCheck = testSubCondition(currentSubcondition, currentScreeningAnswerArray, currentSubCheck);
                        }
                    }

                    //if (!currentSubCheck)
                    //    break;
                }
            }
            /*
             * subCheck = true by default
             * true --> false
             * true --> NMI
             * NMI --> false
             * false = break;
             * */

            subCheck = falseCheck(subCheck, currentSubCheck);

            //if (subCheck == true && currentSubCheck == false)
            //    subCheck = false;
            //else if (subCheck == true && currentSubCheck == "NMI")
            //    subCheck = "NMI";
            //else if (subCheck == "NMI" && currentSubCheck == false)
            //    subCheck = false;

            if (!subCheck)
                break;
        }
        /*
         * check = false by default
         * false --> true
         * false --> NMI
         * NMI --> true
         * true = return true;
         * */

        check = trueCheck(check, subCheck);

        //if (check == false && subCheck == true)
        //    check = true;
        //else if (check == false && subCheck == "NMI")
        //    check = "NMI";
        //else if (check == "NMI" && subCheck == true)
        //    check = true;

        if (check == true)
            return true;
    }
    return check;
};




var checkAndConditions = function (conditionDoc, personDoc, screeningDoc) {

    /* conditionDoc can be either eligibilityObjects or Questions */

    var check = true;

    for(var i = 0; i < conditionDoc.conditions.length; i++) { //and

        var currentCondition = conditionDoc.conditions[i];
        var subCheck = false;

        subconditions_loop:
            for(var j = 0; j < currentCondition.subconditions.length; j++) { //or

                var currentSubcondition = currentCondition.subconditions[j];
                var currentSubCheck = false;

                if (currentSubcondition.source == "person") {

                    var value1 = personDocFunc.getPersonAttribute(personDoc, currentSubcondition.criteria.attributeName);
                    var value2a = currentSubcondition.results.value1;
                    var value2b = currentSubcondition.results.value2;
                    var comparator = currentSubcondition.results.comparator;

                    /* if personDoc does not have enough information assume true, but keep going */
                    if (value1 !== "") {
                        if (value1 != null) {
                            if (condition.conditionCheck(value1, value2a, value2b, comparator)) {
                                currentSubCheck = true;
                                //break;
                            }
                        } else currentSubCheck = "NMI";
                    } else currentSubCheck = "NMI";
                }
                else if (currentSubcondition.source == "question") {

                    var screeningResult = getScreeningResult(screeningDoc.results, currentSubcondition.criteria.question_id);

                    if (screeningResult == -1) {
                        currentSubCheck = "NMI";
                    }
                    else {

                        var currentScreeningAnswerArray = screeningResult.answers;
                        if (currentScreeningAnswerArray.length) {

                            currentSubCheck = testSubCondition(currentSubcondition, currentScreeningAnswerArray, currentSubCheck);
                        }
                    }
                }
                /*
                 * subCheck = false by default
                 * false --> true
                 * false --> NMI
                 * NMI --> true
                 * true = break;
                 * */

                subCheck = trueCheck(subCheck, currentSubCheck);

                //if (subCheck == false && currentSubCheck == true)
                //    subCheck = true;
                //else if (subCheck == false && currentSubCheck == "NMI")
                //    subCheck = "NMI";
                //else if (subCheck == "NMI" && currentSubCheck == true)
                //    subCheck = true;

                if (subCheck == true)
                    break;
            }
        /*
         * check = true by default
         * true --> false
         * true --> NMI
         * NMI --> false
         * false = return false;
         * */

        check = falseCheck(check, subCheck);

        //if (check == true && subCheck == false)
        //    check = false;
        //else if (check == true && subCheck == "NMI")
        //    check = "NMI";
        //else if (check == "NMI" && subCheck == false)
        //    check = false;

        if (!check)
            return false;
    }
    return check;
};

var checkOperator = function (conditionDoc, personDoc, screeningDoc) {

    if (conditionDoc.conditionType == "or") {
        var conditions = checkOrConditions(conditionDoc, personDoc, screeningDoc);
    }
    else if (conditionDoc.conditionType == "and") {
        var conditions = checkAndConditions(conditionDoc, personDoc, screeningDoc);
    }

    return conditions;
};

/* PUBLIC API */
module.exports.getScreeningResult = getScreeningResult;
module.exports.testSubConditionEquals = testSubConditionEquals;
module.exports.testSubConditionNotEquals = testSubConditionNotEquals;
module.exports.testSubCondition = testSubCondition;
module.exports.falseCheck = falseCheck;
module.exports.trueCheck = trueCheck;
module.exports.checkOrConditions = checkOrConditions;
module.exports.checkAndConditions = checkAndConditions;

module.exports.testConditions = checkOperator;