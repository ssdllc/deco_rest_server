var async = require('async'),
    couchActions = require('../../requests/couchActions.js'),
    response = require('../../helpers/response.js'),
    conditionDoc = require('./conditionDoc.js');

function updateScreeningDoc (screening, callback) {

    var screeningId = screening.id;
    var screeningDoc = {};

    Object.assign(screeningDoc, screening);

    delete screeningDoc.id;

    couchActions.updateDoc(screeningId, screeningDoc, function (err, res) {
        callback(err, res);
    });
}

function addAnswerToScreeningDoc (question, screening) {

    var results = screening.results;
    for (var i = 0; i < results.length; i++) {

        // if answer already exists
        if (question.id === results[i].question_id)
            return false;
    }

    var newAnswer = {
        "answers": [
            {
                "answer": "N/A",
                "answerNumber": null
            }
        ],
        "question_id": question.id
    };

    results.push(newAnswer);
    return true;
}

function findUndisplayedQuestions (res) {

    var undisplayedQuestions = [];
    var questions = res.questions;
    var continueUntilNoMoreNA = true;

    while (continueUntilNoMoreNA) {

        continueUntilNoMoreNA = false;
        undisplayedQuestions.length = 0;

        for (var i = 0; i < questions.length; i++) {

            var currentQuestion = questions[i];
            var conditions = conditionDoc.testConditions(currentQuestion, res.person, res.screening);

            if (!conditions) {
                var bool = addAnswerToScreeningDoc(currentQuestion, res.screening);
                if (bool === true)
                    continueUntilNoMoreNA = true;

                undisplayedQuestions.push({"id":currentQuestion.id});
            }
        }
    }

    return undisplayedQuestions;
}

var undisplayedQuestions = function (reqObj, resObj) {

    var questionsQuery =
        "SELECT META(x).id AS id, x.* " +
        "FROM deco_db x " +
        "WHERE x.type = 'question' " +
        "AND ARRAY_LENGTH(x.conditions) > 0";

    async.parallel({
        screening: function(callback){
            couchActions.getDoc(reqObj.data[0].screening_id, function(err, res) {
                if (err) {
                    callback({err:err, code:"SCRE1"}, null);
                    return;
                }
                res.value["id"] = reqObj.data[0].screening_id;
                callback(err, res.value);
            });
        },
        person: function(callback){
            couchActions.getDoc(reqObj.data[0].person_id, function(err, res) {
                if (err) {
                    callback({err:err, code:"SCRE2"}, null);
                    return;
                }
                callback(err, res.value);
            });
        },
        questions: function(callback){
            couchActions.n1qlRequest(questionsQuery, function(err, res) {
                if (err) {
                    callback({err:err, code:"SCRE3"}, null);
                    return;
                }
                callback(err, res);
            });
        }
    }, function(err, res) {

        if (err) {
            resObj = response.respond(resObj, err);
            return;
        }

        var undisplayedQuestions = findUndisplayedQuestions(res);
        if (undisplayedQuestions.length > 0) {

            updateScreeningDoc(res.screening, function (err, res) {

                if (err) {
                    resObj = response.respond(resObj, {err:err, code:"SCRE4"});
                    return;
                }

                resObj = response.respond(resObj, {"data":undisplayedQuestions});
            });

        } else resObj = response.respond(resObj, {"data":undisplayedQuestions});
    });
};

/* PUBLIC API */
module.exports = {
    undisplayedQuestions: undisplayedQuestions
};