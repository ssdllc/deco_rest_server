const fs = require('fs'),
    uuid = require('node-uuid'),
    csvParser = require('csv-parser'),
    moment = require('moment'),
    validator = require('validator'),
    config = require('../config.json'),
    response = require('../helpers/response.js'),
    couchActions = require('../requests/couchActions.js'),
    create = require('../crud/create/create.js'),
    ssn_ignoreList = ["777777777","666666666"];

var listNames,
    processed = {
        "persons":0,
        "screenings":0
    },
    filename = {
        "originalName":"",
        "serverName":""
    };

function personWithMoreInfo (person1,person2) {
    if (person1.ssn && !person2.ssn) {
        return person1;
    } else if (person2.ssn && !person1.ssn) {
        return person2;
    } else if (person1.nameFirst && person1.nameLast && person1.dateOfBirth
        && (!person2.nameFirst || !person2.nameLast || !person2.dateOfBirth)) {
        return person1;
    } else if (person2.nameFirst && person2.nameLast && person2.dateOfBirth
        && (!person1.nameFirst || !person1.nameLast || !person1.dateOfBirth)) {
        return person2;
    } else if (person1.nameFirst && person1.nameLast && (!person2.nameFirst || !person2.nameLast)) {
        return person1;
    } else if (person2.nameFirst && person2.nameLast && (!person1.nameFirst || !person1.nameLast)) {
        return person2;
    } else {
        return person1;
    }
}

function samePersonCheck (person1, person2) {

    if (person1.ssn && person2.ssn) {
        if(person1.ssn == person2.ssn) {
            return personWithMoreInfo(person1,person2);
        } else {
            return null;
        }

    } else if (person1.nameFirst && person1.nameLast && person1.dateOfBirth
        && person2.nameFirst && person2.nameLast && person2.dateOfBirth) {

        if(person1.nameFirst == person2.nameFirst && person1.nameLast == person2.nameLast
            && person1.dateOfBirth == person2.dateOfBirth) {
            return personWithMoreInfo(person1,person2);
        } else {
            return null;
        }

    } else if (person1.nameFirst && person1.nameLast && person2.nameFirst && person2.nameLast) {
        if(person1.nameFirst == person2.nameFirst && person1.nameLast == person2.nameLast) {
            return personWithMoreInfo(person1,person2);
        } else {
            return null;
        }

    } else {
        return null;
    }
}

function escapeChars (str) {
    if (str)
        return str.replace(/'/g, "''");
    else return str;
}

/* does real person exist in couchbase */
function doesPersonExist (person, resObj, callback) {

    var ssn = escapeChars(person.ssn);
    var nameFirst =  escapeChars(person.nameFirst);
    var nameLast = escapeChars(person.nameLast);
    var dateOfBirth = escapeChars(person.dateOfBirth);

    var query;

    if (ssn && nameFirst && nameLast && dateOfBirth) {
        query = "SELECT META(x).id AS id, x.* FROM deco_db x " +
            "WHERE x.type = 'person' " +
            "AND ((x.ssn = '" + ssn + "') " +
            "OR (x.nameFirst = '" + nameFirst + "' " +
            "AND x.nameLast = '" + nameLast + "' " +
            "AND x.dateOfBirth = '" + dateOfBirth + "'))";

        couchActions.n1qlRequest(query, function (err, res) {

            if (err) {
                respond(resObj, {err:err, code:"XI4"});
                return;
            }

            var result = res;

            if(result.length == 0) {
                callback(-1);
            } else callback(result[0]);
        });
    } else if (ssn && nameLast) {
        query = "SELECT META(x).id AS id, x.* FROM deco_db x " +
            "WHERE x.type = 'person' " +
            "AND x.ssn = '" + ssn + "' " +
            "AND x.nameLast = '" + nameLast + "'";

        couchActions.n1qlRequest(query, function (err, res) {

            if (err) {
                respond(resObj, {err:err, code:"XI5"});
                return;
            }

            var result = res;

            if(result.length == 0) {
                callback(-1);
            } else callback(result[0]);
        });
    } else if (nameFirst && nameLast && dateOfBirth) {
        query = "SELECT META(x).id AS id, x.* FROM deco_db x " +
            "WHERE x.type = 'person' " +
            "AND x.nameFirst = '" + nameFirst + "' " +
            "AND x.nameLast = '" + nameLast + "' " +
            "AND x.dateOfBirth = '" + dateOfBirth + "'";

        couchActions.n1qlRequest(query, function (err, res) {

            if (err) {
                respond(resObj, {err:err, code:"XI6"});
                return;
            }

            var result = res;

            if(result.length == 0) {
                callback(-1);
            } else callback(result[0]);
        });
    } else {
        var debugMessage = "Person: " + nameFirst + " " + nameLast + " ignored because missing minimum information";

        createDebugDoc("createDoc", person, debugMessage, resObj, function() {
            callback(-2);
        });
    }
}

/* does real screening exist in couchbase */
function doesScreeningExist (screening, callback) {

    var accountNumber = escapeChars(screening.accountNumber);
    var medicalRecordNumber = escapeChars(screening.medicalRecordNumber);
    var firstClientId = escapeChars(screening.client_ids[0]);

    var query = "SELECT META(x).id AS id FROM deco_db x " +
        "WHERE x.type = 'screening' " +
        "AND x.accountNumber = '" + accountNumber + "' " +
        "AND x.medicalRecordNumber = '" + medicalRecordNumber + "' " +
        "AND x.client_ids[0] = '" + firstClientId + "'";

    couchActions.n1qlRequest(query, function (err, res) {

        if (err) {
            respond(resObj, {err:err, code:"XI7"});
            return;
        }

        var result = res;

        if(result.length == 0) {
            callback(-1);
        } else callback(result[0].id);
    });
}

function getAttributeId (listValue, listName) {

    for (var i = 0; i < listNames.length; i++) {

        console.log(listNames[i].listName);
        console.log(listName);
        console.log('-------------------------');
        if (listNames[i].listName == listName) {
            if (listNames[i].value.toLowerCase() == listValue) {
                return listNames[i].id;
            }
        }
    }

    console.log('ERROR: Attribute ID not found: '+listValue);
    return "";
}

function bonSecoursAttrId (xmlValue, listName) {

    /* lower case data */
    xmlValue = xmlValue.toLowerCase();

    if (listName == "relationshipStatus") {

        switch(xmlValue) {
            case "s":
                return getAttributeId ("single", listName);
            case "m":
                return getAttributeId ("married", listName);
            case "d":
                return getAttributeId ("divorced", listName);
            default: console.log('ERROR: relationshipStatus letter not supported');
        }
    } else if (listName == "country") {
        if (xmlValue == "us")
            getAttributeId ("usa", listName);
    } else return getAttributeId (xmlValue, listName);

    return "";
}

function checkSSNIgnoreList (ssn) {

    if (ssn_ignoreList.indexOf(ssn) >= 0)
        return null;
    else
        return ssn;
}

function generateScreeningDoc (currentRow, client_id, user_id) {

    var screening = {
        "displayConditions": true,
        "date_created": moment().format(),
        "date_updated": moment().format(),
        "user_id": user_id,
        "filename": filename,
        "person_id": "",
        "type": "tempScreening",
        "results": [
        ],
        "accountNumber": currentRow.AccountNumber,
        "admission_date":currentRow.AdmissionDate,
        "medicalRecordNumber": currentRow.MRnumber,
        "aoPlacementNumber":"",
        "client_ids": [],
        "state_id": bonSecoursAttrId(currentRow.hospitalState, "state"),
        "channels": [
            "ssd"
        ]
    };

    if (client_id)
        screening.client_ids.push(client_id);



    return screening;
}

function generateGuarantorDoc (currentRow, client_id, user_id) {

    var str = currentRow.RespName.split(",");
    var str2 = str[1].split(" ");

    var person = {
        "type": "tempPerson",
        "namePrefix_id": "",
        "nameFirst": str2[0],
        "nameMiddle": str2[1],
        "nameLast": str[0],
        "nameSuffix_id": "",
        "nameMaiden": "",
        "ssn": checkSSNIgnoreList(currentRow.RespSSN.replace(/-/g,'').trim()),
        "user_id": user_id,
        "filename": filename,
        "gender_id": "",
        "dateOfBirth": currentRow.RespDOB,
        "dateOfDeath": "",
        "homeless": false,
        "address": [
            {
                "residence": true,
                "mailingAddress": true,
                "addressTitle": "",
                "street": currentRow.RespAddress,
                "unitNumber": "",
                "city": currentRow.RespCity,
                "state_id": bonSecoursAttrId(currentRow.RespState, "state"),
                "zip": currentRow.RespZip,
                "zipPlus4": "",
                "country_id": ""
            }
        ],
        "email": [
            {
                "emailTitle": "",
                "emailAddress": ""
            }
        ],
        "phone": [
            {
                "phoneType_id": "",
                "phoneNumber": currentRow.RespPhone1
            },
            {
                "phoneType_id": "",
                "phoneNumber": currentRow.RespPhone2
            }
        ],
        "client_ids": [ client_id ],
        "language_id": "",
        "educationLevel_id": "",
        "race_id": "",
        "citizenshipStatus_id": "",
        "relationshipStatus_id": "",
        "immigrationStatus_id": "",
        "veteran": false,
        "dateOfEntry": "",
        "greenCardIssueDate": "",
        "countryOfOrigin_id": "",
        "employerJobTitle": "",
        "employerCompanyName": "",
        "guarantor": {
            "person_id": "",
            "relation_id": ""
        },
        "channels": [
            "ssd"
        ]
    };

    return person;
}

function generatePersonDoc (currentRow, client_id, user_id) {

    var str = currentRow.FirstName.split(" ");

    var person = {
        "type": "tempPerson",
        "namePrefix_id": "",
        "nameFirst": str[0],
        "nameMiddle": str[1],
        "nameLast": currentRow.LastName,
        "nameSuffix_id": "",
        "nameMaiden": "",
        "ssn": checkSSNIgnoreList(currentRow.SSN.replace(/-/g,'').trim()),
        "user_id": user_id,
        "filename": filename,
        "gender_id": "",
        "dateOfBirth": moment(currentRow.DOB).format('MM/DD/YYYY'),
        "dateOfDeath": "",
        "homeless": false,
        "address": [
            {
                "residence": true,
                "mailingAddress": true,
                "addressTitle": "",
                "street": currentRow.Address,
                "unitNumber": "",
                "city": currentRow.City,
                "state_id": bonSecoursAttrId(currentRow.State, "state"),
                "zip": currentRow.Zip,
                "zipPlus4": "",
                "country_id": ""
            }
        ],
        "email": [
            {
                "emailTitle": "",
                "emailAddress": currentRow.Email
            }
        ],
        "phone": [
            {
                "phoneType_id": "",
                "phoneNumber": currentRow.Phone1
            },
            {
                "phoneType_id": "",
                "phoneNumber": currentRow.Phone2
            },
            {
                "phoneType_id": "",
                "phoneNumber": currentRow.Phone3
            }
        ],
        "client_ids": [ client_id ],
        "language_id": "",
        "educationLevel_id": "",
        "race_id": "",
        "citizenshipStatus_id": "",
        "relationshipStatus_id": "",
        "immigrationStatus_id": "",
        "veteran": false,
        "dateOfEntry": "",
        "greenCardIssueDate": "",
        "countryOfOrigin_id": "",
        "employerJobTitle": "",
        "employerCompanyName": "",
        "guarantor": {
            "person_id": "",
            "relation_id": ""
        },
        "channels": [
            "ssd"
        ]
    };

    if (currentRow.Sex && currentRow.Sex.toLowerCase() === "m") {
        person.gender_id = bonSecoursAttrId("Male", "gender")
    } else if (currentRow.Sex && currentRow.Sex.toLowerCase() === "f") {
        person.gender_id = bonSecoursAttrId("Female", "gender")
    } else if (currentRow.Sex)
        person.gender_id = bonSecoursAttrId(currentRow.Sex, "gender");

    return person;
}

function respond (resObj, data, send, err) {

    /* reset globals */
    processed = {
        "persons":0,
        "screenings":0
    };
    filename = {
        "originalName":"",
        "serverName":""
    };

    if (send && err)
        resObj = response.respond(resObj, data, true, true);
    else
        resObj = response.respond(resObj, data);

}

function createXMLUploadHistoryDoc (resObj) {
    var xmlUploadHistory = {
        "filename": filename,
        "date_created": moment().format(),
        "user_id": resObj.res.req.session.user_id,
        "processed": processed,
        "committedOn": "",
        "client_id": "",
        "type": "xmlUploadHistory",
        "channels": [
            "ssd"
        ]
    };

    if (resObj.res.req.body.client_id)
        xmlUploadHistory.client_id = resObj.res.req.body.client_id;

    return xmlUploadHistory;
}

function createDebugDoc (functionName, data, message, resObj, callback) {

    var debugDoc = {
        "date_created": moment().format(),
        "functionName": functionName,
        "data":data,
        "message":message,
        "user_id":resObj.res.req.session.user_id,
        "filename":filename,
        "type":"debug"
    };

    var id = uuid.v1();

    createDoc(id, debugDoc, resObj, function () {
        callback();
    });
}

function createDoc (id, doc, resObj, callback) {

    /* creating doc in Couchbase */
    couchActions.createDoc(id, doc, function (err, res) {

        if (err) {
            createDebugDoc("createDoc", doc, err.message, resObj, function() {
                callback();
            });
            //} else  {
            //
            //    if (doc.type == "person") {
            //
            //        create.createHouseholdMemberSelf(id, resObj, function () {
            //            callback();
            //        })
        } else callback();
        //}
    });
}

function updateDoc (id, doc, resObj, callback) {

    /* creating doc in Couchbase */
    couchActions.updateDoc(id, doc, function (err, res) {

        if (err) {
            createDebugDoc("updateDoc", doc, err.message, resObj, function() {
                callback();
            });
        } else callback();
    });
}

function deleteDocsOfSameFileName (resObj, callback) {

    var query = "DELETE FROM deco_db x " +
        "WHERE (x.type = 'debug' OR x.type = 'tempPerson' " +
        "OR x.type = 'tempScreening' OR x.type = 'xmlUploadHistory') " +
        "AND x.user_id = '" + resObj.res.req.session.user_id  + "' " +
        "AND x.filename.originalName = '" + filename.originalName + "'";

    couchActions.n1qlRequest(query, function (err, res) {

        if (err) {
            respond(resObj, {err:err, code:"XI8"});
            return;
        }

        callback();
    });
}

function canRespond (dictionary) {

    for (var i = 0; i < dictionary.length; i++) {
        if (dictionary[i].complete == false)
            return false;
    }

    return true;
}

function admComplete (adm, dictionary, resObj) {

    adm.complete = true;

    /* Checks every dictionary index to see if all responses have been made */
    if (canRespond(dictionary)) {

        /* set and create xmlUploadHistory Doc */
        var xmlUploadHistoryDoc = createXMLUploadHistoryDoc(resObj);

        var id = uuid.v1();

        createDoc(id, xmlUploadHistoryDoc, resObj, function() {

            respond(resObj, {"filename":filename.originalName});
        });
    }
}

function createScreening(adm, patientID, dictionary, resObj) {

    var screening = adm.screening;

    /* add screening information */
    screening.person_id = patientID;

    doesScreeningExist(screening, function (id) {

        /* if screeningDoc does not exist in Couchbase */
        if (id == -1) {

            /* new ID */
            var screeningID = uuid.v1();

            /* creating screeningDoc in Couchbase */
            createDoc(screeningID, screening, resObj, function () {

                processed.screenings++;

                admComplete(adm, dictionary, resObj);
            });
        } else admComplete(adm, dictionary, resObj);
    });
}

function createPatient (adm, guarantorID, dictionary, resObj) {


    var xmlPatient = adm.person;

    doesPersonExist(xmlPatient, resObj, function (patient) {

        var patientID = patient.id;

        /* if personDoc does not exist in Couchbase */
        if (patient == -1) {

            /* new ID */
            patientID = uuid.v1();

            /* add guarantor information */
            if (guarantorID) {

                if (guarantorID == "self") {
                    xmlPatient.guarantor.person_id = patientID;
                    xmlPatient.guarantor.relation_id = bonSecoursAttrId("self", "relation");
                } else {
                    xmlPatient.guarantor.person_id = guarantorID;
                    xmlPatient.guarantor.relation_id = bonSecoursAttrId("unknown", "relation");
                }
            }

            /* creating personDoc in Couchbase */
            createDoc(patientID, xmlPatient, resObj, function () {

                processed.persons++;

                createScreening(adm, patientID, dictionary, resObj);

            });
        } else if (patient == -2) {
            /* not enough information on patient */
            admComplete(adm, dictionary, resObj);
        } else {
            /* real person exists */
            createScreening(adm, patientID, dictionary, resObj);
        }
    });

}

function createGuarantor (adm, dictionary, resObj) {

    var samePatient = samePersonCheck(adm.person,adm.guarantor);

    if (samePatient) {

        adm.person = samePatient;
        createPatient(adm, "self", dictionary, resObj)

    } else {

        var guarantor = adm.guarantor;

        doesPersonExist(guarantor, resObj, function (guar) {

            /* if personDoc does not exist in Couchbase AND makes sure the doc is not empty */
            if ((guar == -1) && (guarantor.nameFirst != "" && guarantor.nameLast != "")) {

                /* new ID */
                var guarantorID = uuid.v1();

                /* creating personDoc (guarantor) in Couchbase */
                createDoc(guarantorID, guarantor, resObj, function () {

                    processed.persons++;

                    createPatient(adm, guarantorID, dictionary, resObj);
                });
            } else createPatient(adm, "", dictionary, resObj);
        });
    }

}

function processAdmDict (dictionary, resObj) {

    if (dictionary.length !== 0) {
        for (var i = 0; i < dictionary.length; i++) {
            createGuarantor(dictionary[i], dictionary, resObj);
        }
    } else respond(resObj, {"XmlUploadComplete":"No records in the dictionary"});
}

function generateAdmDict (parsedCSV, resObj) {

    var dictionary = [];
    var client_id = "";
    var user_id = resObj.res.req.session.user_id;

    if (resObj.res.req.body.client_id)
        client_id = resObj.res.req.body.client_id;

    for (var i = 0; i < parsedCSV.length; i++) {

        /* generating respective Documents */
        var guarantor = generateGuarantorDoc(parsedCSV[i], client_id, user_id);
        var person = generatePersonDoc(parsedCSV[i], client_id, user_id);
        var screening = generateScreeningDoc(parsedCSV[i], client_id, user_id);

        /* updating id dictionary */
        dictionary.push({"guarantor":guarantor, "person":person, "screening":screening, "complete":false});
    }

    processAdmDict(dictionary, resObj);
}

function setUpRequests (parsedCSV, resObj) {

    if (resObj.res.req.body.client_id) {

        deleteDocsOfSameFileName(resObj, function () {

            var query = "SELECT META(x).id AS id, x.* " +
                "FROM deco_db x " +
                "WHERE x.type = 'list' AND " +
                "(x.listName = 'gender' OR x.listName = 'relationshipStatus' " +
                "OR x.listName = 'state' OR x.listName = 'country' " +
                "OR x.listName = 'phoneType' OR x.listName = 'relation')";

            couchActions.n1qlRequest(query, function (err, res) {

                if (err) {
                    respond(resObj, {err:err, code:"XI9"});
                    return;
                }

                listNames = res;
                generateAdmDict(parsedCSV, resObj)
            });
        });

    } else response.respond(resObj, {"Unsuccessful":"We need a client_id to import documents"});
}

const parseCSV = function (originalFileName, csvFileName, resObj) {

    var parsedCSV = [];

    fs.createReadStream(config.pathToParentFolder + 'deco_uploads/' + csvFileName)
        .pipe(csvParser({
            separator: '|',
            headers: [
                'DN4HospitalID',
                'DN4AdmissionID',
                'AdmissionDate',
                'DischargeDate',
                'LengthOfStay',
                'AccountNumber',
                'MRnumber',
                'HospType',
                'TotalCharges',
                'MCD_STATUS',
                'MCD_ActionSummary',
                'MCD_RefDate',
                'MCD_RefType',
                'MCD_DECOCW',
                'Balance',
                'MCD_Type',
                'MCD_ReturnDate',
                'MCD_MADates',
                'MCD_MAStartDate',
                'MCD_MANumber',
                'b1',
                'b2',
                'b3',
                'b4',
                'b5',
                'TotalAdjustments',
                'b6',
                'MCD-Closed',
                'b7',
                'b8',
                'b9',
                'HospitalService',
                'InsurancePlan',
                'FinancialClass',
                'MCD_FUDate',
                'b10',
                'b11',
                'b12',
                'b13',
                'b14',
                'b15',
                'b16',
                'b17',
                'b18',
                'b19',
                'b20',
                'b21',
                'b22',
                'b23',
                'b24',
                'b25',
                'b26',
                'b27',
                'b28',
                'b29',
                'b30',
                'b31',
                'b32',
                'b33',
                'b34',
                'b35',
                'b36',
                'b37',
                'b38',
                'b39',
                'b40',
                'b41',
                'b42',
                'b43',
                'b44',
                'b45',
                'b46',
                'b47',
                'b48',
                'b49',
                'b50',
                'b51',
                'b52',
                'b53',
                'b54',
                'b55',
                'b56',
                'b57',
                'b58',
                'b59',
                'b60',
                'b61',
                'b62',
                'b63',
                'b64',
                'otherSSNdotdotdot',
                'b65',
                'hospitalName',
                'hospitalStreet',
                'hospitalCity',
                'hospitalState',
                'hospitalZip',
                'hospitalPhoneNumber',
                'b66',
                'b67',
                'b68',
                'b69',
                'b70',
                'b71',
                'b72',
                'b73',
                'b74',
                'b75',
                'b76',
                'b77',
                'b78',
                'b79',
                'b80',
                'b81',
                'b82',
                'b83',
                'b84',
                'b85',
                'b86',
                'b87',
                'b88',
                'b89',
                'b90',
                'b91',
                'b92',
                'b93',
                'b94',
                'b95',
                'b96',
                'b97',
                'b98',
                'b99',
                'b100',
                'MCD_FeeText',
                'PatientID',
                'FirstName',
                'LastName',
                'SSN',
                'DOB',
                'Sex',
                'Language',
                'Deceased',
                'Address',
                'City',
                'State',
                'Zip',
                'Phone1',
                'RespName',
                'RespSSN',
                'RespDOB',
                'RespRel',
                'RespPhone1',
                'RespAddress',
                'RespCity',
                'RespState',
                'RespZip',
                'RespPhone2',
                'Phone2',
                'Phone3',
                'C1Name',
                'C1Rel',
                'C1Phone1',
                'C1Phone2C1Address',
                'Email',
                'C1City',
                'C1State',
                'C1Zip',
                'C1Country',
                'C2Name',
                'C2Rel',
                'C2phone1',
                'C2Phone2',
                'C2Address',
                'C2City',
                'C2State',
                'C2Zip',
                'C2Country',
                'C3Name',
                'C3Rel',
                'C3Phone1',
                'C3Phone2',
                'C3address',
                'C3City',
                'C3State',
                'C3Zip',
                'C3Country',
                'C4Name',
                'C4Rel',
                'C4Phone1',
                'C4Phone2',
                'C4Address',
                'C4City',
                'C4State',
                'C4Zip',
                'C4Country'
            ]
        }))
        .on('data', function(data) {
            parsedCSV.push(data);
        })
        .on('finish', function() {
            filename.originalName = originalFileName;
            filename.serverName = csvFileName;
            setUpRequests(parsedCSV, resObj);
        });
};


/* PUBLIC API */
module.exports.parseCSV = parseCSV;