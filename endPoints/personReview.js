var async = require('async'),
    personDocFunc = require('../helpers/personDocFunc.js'),
    response = require('../helpers/response.js'),
    db = require('../requests/db.js');

function buildHouseholdWithIncomeAssets (householdMemberDocs, incomeDocs, assetDocs, personDoc) {

    var householdWithIandA = [];

    if (householdMemberDocs.length > 0) {

        for (var i = 0; i < householdMemberDocs.length; i++) {

            var mem = householdMemberDocs[i];

            /* adding name if relationshipType is self */
            if (mem.relationshipType_display == "Self") {
                mem.nameFirst = personDoc.nameFirst;
                mem.nameLast = personDoc.nameLast;
            }

            var newObj = {
                "nameFirst":mem.nameFirst,
                "nameLast":mem.nameLast,
                "relationshipType_display":mem.relationshipType_display,
                "income":[],
                "assets":[]
            };

            for (var j = 0; j < incomeDocs.length; j++) {
                if (incomeDocs[j].householdMember_id == mem.id) {
                    newObj.income.push({
                        "incomeType_display": incomeDocs[j].incomeType_display,
                        "amount":incomeDocs[j].amount
                    });
                }
            }
            for (var k = 0; k < assetDocs.length; k++) {
                if (assetDocs[k].householdMember_id == mem.id) {
                    newObj.assets.push({
                        "assetType_display": assetDocs[k].assetType_display,
                        "amount":assetDocs[k].amount
                    });
                }
            }
            householdWithIandA.push(newObj);
        }
    }
    return householdWithIandA;
}

function pluckScreeningSummaries (screeningSummaries) {

    for (var i = 0; i < screeningSummaries.length; i++) {

        var currentSummary = screeningSummaries[i];

        if (loggedInUser.workingClients.indexOf(currentSummary.client_id) === -1) {
            screeningSummaries.splice(i, 1);
            i--;
        }
    }
    return screeningSummaries;
}

var getPersonRelatedData = function (person_id, callback) {

    var householdMembersQuery = "SELECT META(x).id AS id, x.nameFirst, x.nameLast, rel.display AS relationshipType_display " +
        "FROM deco_db x " +
        "JOIN deco_db rel ON KEYS x.relationshipType_id " +
        "WHERE x.type = 'householdMember' AND x.person_id = '" + person_id + "' " +
        "ORDER BY x.nameLast, x.nameFirst";

    var incomeDocsQuery = "SELECT META(x).id AS id, x.householdMember_id, x.amount, income.display AS incomeType_display, " +
        "house.nameFirst, house.nameLast " +
        "FROM deco_db x " +
        "JOIN deco_db income ON KEYS x.incomeType_id " +
        "JOIN deco_db house ON KEYS x.householdMember_id " +
        "WHERE x.type = 'income' AND house.person_id = '" + person_id + "'";

    var assetsDocsQuery = "SELECT META(x).id AS id, x.householdMember_id, x.amount, asset.display AS assetType_display, " +
        "house.nameFirst, house.nameLast " +
        "FROM deco_db x " +
        "JOIN deco_db asset ON KEYS x.assetType_id " +
        "JOIN deco_db house ON KEYS x.householdMember_id " +
        "WHERE x.type = 'asset' AND house.person_id = '" + person_id + "'";

    var screeningSummaryQuery =
        "SELECT META(screeningSummary).id id, screeningSummary.clientName, screeningSummary.client_ids[0] as client_id, " +
        "screening.date_updated screening_date " +
        "FROM deco_db screeningSummary " +
        "JOIN deco_db screening ON KEYS screeningSummary.screening.id " +
        "WHERE screeningSummary.type = 'screeningSummary' AND screening.person_id = '" + person_id + "' " +
        "GROUP BY screeningSummary.screening.id " +
        "ORDER BY screening.date_updated desc";

    async.parallel({
        person: function(callback){
            db.getDoc(person_id, function (err, res) {

                if (err) {
                    callback({err:err, code:"PR1"}, null);
                    return;
                }

                callback(err, res);
            });
        },
        householdMembers: function(callback){
            db.n1qlRequest(householdMembersQuery, function(err, res) {

                if (err) {
                    callback({err:err, code:"PR2"}, null);
                    return;
                }

                callback(err, res);
            });
        },
        incomeDocs: function(callback){
            db.n1qlRequest(incomeDocsQuery, function(err, res) {

                if (err) {
                    callback({err: err, code: "PR3"});
                    return;
                }

                callback(err, res);
            });
        },
        assetDocs: function(callback){
            db.n1qlRequest(assetsDocsQuery, function(err, res) {

                if (err) {
                    callback({err: err, code: "PR4"});
                    return;
                }

                callback(err, res);
            });
        },
        screeningSummaries: function(callback){
            db.n1qlRequest(screeningSummaryQuery, function(err, res) {

                if (err) {
                    callback({err: err, code: "PR5"});
                    return;
                }

                callback(err, res);
            });
        }
    }, function(err, res) {

        if (err) {
            callback(err, res);
            return;
        }

        personDocFunc.personIdsToValues(res.person.value, function (personDoc) {

            res.person = personDoc;

            res.householdMembers = buildHouseholdWithIncomeAssets(res.householdMembers, res.incomeDocs, res.assetDocs, res.person);

            delete res.incomeDocs;
            delete res.assetDocs;

            res.screeningSummaries = pluckScreeningSummaries(res.screeningSummaries);

            callback(err, res);
        });
    });
};

var getReview = function (req, resObj) {

    var person_id = req.body.person_id;

    getPersonRelatedData(person_id, function (err, res) {

        if (err) {
            resObj = response.respond(resObj, err);
            return;
        }

        resObj = response.respond(resObj, res);
    });
};

/* PUBLIC API */
module.exports.getReview = getReview;