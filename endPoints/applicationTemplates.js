var moment = require('moment'),
    async = require('async'),
    pdfFiller = require('pdffiller'),
    uuid = require('node-uuid'),
    idsToValues = require('../helpers/idsToValues.js'),
    config = require('../config.json'),
    couchActions = require('../requests/couchActions.js'),
    db = require('../requests/db.js'),
    response = require('../helpers/response.js'),
    personDocFunc = require('../helpers/personDocFunc.js');
const fs = require('fs');

// Function to remove unnecessary attributes
function removeAttributes (doc) {

    var attributesToRemove = ['channels', 'client_ids', 'workingClients', 'date_updates', 'filename',
        'type', 'user_id', 'date_created', 'date_updated', 'password'];

    for (var i = 0; i < attributesToRemove.length; i++) {

        if (doc.hasOwnProperty(attributesToRemove[i]))
            delete doc[attributesToRemove[i]];
    }

    return doc;
}

// Compiling data from database into the form fields to fill the form
function compileData (patientData, formData) {

    for (var formProp in formData) {

        for (var patientProp in patientData) {

            var currentPatientProp = patientData[patientProp];

            if (formProp === patientProp) {

                // if patient attribute is a boolean
                if (typeof(currentPatientProp) === "boolean") {

                    if (currentPatientProp === true) {
                        formData[formProp] = "Yes";
                        formData[formProp + "Off"] = "Off";
                    }
                    else {
                        // either no checkmark or a checkmark in false box
                        formData[formProp] = "Off";
                        formData[formProp + "Off"] = "Yes";
                    }

                    break;
                }

                // change any nulls to empty string so null does not show up in pdf
                if (currentPatientProp === null)
                    currentPatientProp = "";

                formData[formProp] = currentPatientProp;
                break;
            }

            else if (formProp === patientProp+"Male" || formProp === patientProp+"Female") {

                if (currentPatientProp) {
                    if (currentPatientProp.toLowerCase() === "male") {
                        formData[patientProp + "Male"] = "Yes";
                        formData[patientProp + "Female"] = "Off";
                        break;
                    }
                    else if (currentPatientProp.toLowerCase() === "female") {
                        // either no checkmark or a checkmark in false box
                        formData[patientProp + "Male"] = "Off";
                        formData[patientProp + "Female"] = "Yes";
                        break;
                    }
                }

                // change any nulls to empty string so null does not show up in pdf
                if (currentPatientProp === null)
                    currentPatientProp = "";

                formData[formProp] = currentPatientProp;
                break;
            }
        }
    }
    return formData;
}

function requestDatabaseData (person_id, req, callback) {

    async.parallel({
        person: function(callback){
            couchActions.getDoc(person_id, function(err, res) {
                callback(err, res);
            });
        },
        user: function(callback){
            db.getDoc(req.session.user_id, function(err, res) {
                callback(err, res);
            });
        }
    }, function(err, res) {

        if (err) {
            callback({err:err, code:"AT1"}, res);
            return;
        }

        var databaseData = {
            person: removeAttributes(res.person.value),
            user: removeAttributes(res.user.value)
        };

        callback(err, databaseData);
    });
}

// Requesting data from database and pdf file form fields
function requestData (person_id, sourcePDF, req, callback) {

    async.parallel({
        databaseData: function(callback){
            requestDatabaseData(person_id, req, function (err, res) {
                callback(err, res);
            });
        },
        formData: function(callback){
            pdfFiller.generateFDFTemplate(sourcePDF, null, function(err, res) {
                callback(err, res);
            });
        }
    }, function(err, res) {
        callback(err, res);
    });
}

var fillPDF = function (person_id, applicationTemplateFilename, req, resObj) {

    var sourcePDF = config.pathToParentFolder + "deco_pdfs/template_pdfs/" + applicationTemplateFilename;

    requestData(person_id, sourcePDF, req, function (err, res) {

        if (err) {
            resObj = response.respond(resObj, err);
            return;
        }

        var patientData = res.databaseData;
        var nameFirst = patientData.person.nameFirst;
        var nameLast = patientData.person.nameLast;
        var formData = res.formData;

        var populatedFilename = moment().format("YYYYMMDD") + "_"
            + nameLast.replace(/\s+/g, '') + "_" + nameFirst.replace(/\s+/g, '') + "_" + applicationTemplateFilename;
        var destinationPDF =  config.pathToParentFolder + "deco_pdfs/filled_pdfs/" + populatedFilename;

        var flatPatientData = JSON.flatten(patientData);

        idsToValues.replaceFlatJSON(flatPatientData, function (err, res) {

            if (err) {
                resObj = response.respond(resObj, err);
                return;
            }

            formData = compileData(res, formData);

            pdfFiller.fillForm(sourcePDF, destinationPDF, formData, function(err) {

                if (err) {
                    resObj = response.respond(resObj, err);
                    return;
                }

                // Set disposition and send it.
                resObj.res.download(destinationPDF, function (err) {
                    if (err) throw err;

                    fs.unlink(destinationPDF, (err) => {
                        if (err) throw err;
                        console.log('successfully deleted file');
                    });
                });
            });
        });
    });
};

var uploadPDF = function (req, resObj) {

    var data = req.body;
    var filename = req.file.filename;
    var id = uuid.v1();

    var applicationTemplate = {
        "type": "applicationTemplate",
        "fileName":filename,
        "displayName":data.displayName,
        "program_id":data.program_id
    };

    couchActions.createDoc(id, applicationTemplate, function (err, res) {

        if (err) {
            resObj = response.respond(resObj, {err:err, code:"AT2"});
        } else
            resObj = response.respond(resObj, {"id": filename});
    });
};

/* PUBLIC API */
module.exports.fillPDF = fillPDF;
module.exports.uploadPDF = uploadPDF;