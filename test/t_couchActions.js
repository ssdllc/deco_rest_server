var uuid = require('node-uuid');
var chai = require('chai');
var expect = chai.expect;

require('../init.js');
var db = require('../requests/db.js');

describe('Accessing the Couchbase Database', function() {

    var user = {
        "type": "user",
        "nameFirst": "Elon",
        "nameLast": "Musk",
        "email": "emusk@the.god",
        "channels": ["ssd"]
    };

    var newID = uuid.v1();

    describe('via a createDoc request', function() {

        it('will create a user document', function(done) {

            db.createDoc(newID, user, function(err ,res) {

                expect(err === null).to.be.true;
                expect(res).to.have.property('cas');

                done();

            });
        });
    });

    describe('via a getDoc request', function() {

        it('will return a document that has the id of ' + newID, function(done) {

            db.getDoc(newID, function(err, res) {

                expect(err === null).to.be.true;
                expect(res).to.be.instanceof(Object);
                expect(res).to.have.property('value');
                expect(res.value).to.have.property('type');

                done();
            });

        });
    });

    describe('via a updateDoc request', function() {

        it('will update a user document', function(done) {

            var user = {
                "type": "user",
                "nameFirst": "Elony",
                "nameLast": "Musky",
                "email": "emusk@the.gody",
                "role_ids": [],
                "channels": ["ssd"]
            };

            db.updateDoc(newID, user, function(err ,res) {

                expect(err === null).to.be.true;
                expect(res).to.have.property('cas');

                done();
            });

        });
    });

    describe('via a deleteDoc request', function() {

        it('will delete a user document', function(done) {

            db.deleteDoc(newID, function(err ,res) {

                expect(err === null).to.be.true;
                expect(res).to.have.property('cas');

                done();
            });

        });
    });

    describe('via a viewQuery request', function() {

        it('will return an array full of objects of listName = gender', function(done) {

            var query = db.ViewQueryTemp.from('lookUps', 'documentsOfListName').range(["gender"],["gender",999]).stale(db.ViewQueryTemp.Update.BEFORE);
            db.viewQuery(query, function(err, res) {

                expect(err === null).to.be.true;

                expect(res).to.be.instanceof(Object);
                expect(res).to.be.instanceof(Array);

                expect(res[0]).to.have.property('key');
                expect(res[0]).to.have.property('value');

                done();
            });

        });
    });

    describe('via a N1QL request', function() {

        it('will return all documents of type user', function(done) {

            var query = "SELECT x.* FROM deco_db x WHERE x.type = 'user'";

            db.n1qlRequest(query, function(err, res) {

                expect(err === null).to.be.true;

                expect(res).to.be.instanceof(Object);
                expect(res).to.be.instanceof(Array);

                expect(res[0]).to.have.property('type');

                done();
            });
        });
    });
});