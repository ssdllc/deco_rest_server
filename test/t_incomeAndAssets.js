var chai = require('chai');
var expect = chai.expect;

var incomeAndAssets = require('../endPoints/screening/incomeAndAssets.js');
var data = require('./data/d_incomeAndAssets.json');

describe('incomeAndAssets.js', function() {

    describe('determineLimitFromBaseline', function() {

        it('will return a limit', function(done) {

            var limit = incomeAndAssets.determineLimitFromBaseline(
                {
                    "min":0,
                    "max":9999999999999999
                },
                2,
                data.baselineDictionary["9481c710-c083-11e5-ae66-bfbde649b320"],
                300,
                "income"
            );

            expect(limit.max).to.equal(47790);
            expect(limit.min).to.equal(0);

            done();
        });
    });

    describe('calculateFamilySizeAssetLimit_7', function() {

        it('will return a family size asset limit', function(done) {

            var familySizeAssetLimit = incomeAndAssets.calculateFamilySizeAssetLimit_7(
                2,
                data.dictionary,
                data.program_id
            );

            expect(familySizeAssetLimit.min).to.equal(0);
            expect(familySizeAssetLimit.max).to.equal(450);

            done();
        });
    });

    describe('calculateAssetDisregard_6', function() {

        it('will return an asset disregard', function(done) {

            var assetDisregard = incomeAndAssets.calculateAssetDisregard_6(
                data.aANDiDocs,
                data.dictionary,
                data.program_id
            );

            expect(assetDisregard.disregard).to.equal(0);
            expect(assetDisregard.overallDisregard.amount).to.equal(40);
            expect(assetDisregard.overallDisregard.fixedOrPercent).to.equal("percent");

            done();
        });
    });

    describe('calculateTotalAssets_5', function() {

        it('will return a total asset', function(done) {

            var totalAssets = incomeAndAssets.calculateTotalAssets_5(
                data.aANDiDocs,
                data.dictionary,
                data.program_id
            );

            expect(totalAssets).to.equal(2000);

            done();
        });
    });

    describe('test_4t7t', function() {

        it('will return true', function(done) {

            var bool = incomeAndAssets.test_4t7t(
                {min:1, max:999},
                1003,
                {disregard:0, overallDisregard:{amount:40, fixedOrPercent:"percent"}}
            );

            expect(bool).to.be.true;

            done();
        });

        it('will return false', function(done) {

            var bool = incomeAndAssets.test_4t7t(
                {min:1, max:300},
                1003,
                {disregard:0, overallDisregard:{amount:40, fixedOrPercent:"percent"}}
            );

            expect(bool).to.be.false;

            done();
        });
    });

    describe('calculateFamilySizeIncomeLimit_4', function() {

        it('will return a family size income limit', function(done) {

             var familySizeIncomeLimit = incomeAndAssets.calculateFamilySizeIncomeLimit_4(
                2,
                data.dictionary,
                data.baselineDictionary,
                data.program_id
            );

            expect(familySizeIncomeLimit.min).to.equal(1);
            expect(familySizeIncomeLimit.max).to.equal(999);

            done();
        });
    });

    describe('calculateIncomeDisregards_3', function() {

        it('will return a income disregard', function(done) {

            var incomeDisregard = incomeAndAssets.calculateAssetDisregard_6(
                data.aANDiDocs,
                data.dictionary,
                data.program_id
            );

            expect(incomeDisregard.disregard).to.equal(0);
            expect(incomeDisregard.overallDisregard.amount).to.equal(40);
            expect(incomeDisregard.overallDisregard.fixedOrPercent).to.equal("percent");

            done();
        });
    });

    describe('calculateTotalIncome_2', function() {

        it('will return total income', function(done) {

            var totalIncome = incomeAndAssets.calculateTotalIncome_2(
                data.aANDiDocs,
                data.dictionary,
                data.program_id
            );

            expect(totalIncome).to.equal(1003);

            done();
        });
    });

    describe('calculateFamilySize_1', function() {

        it('will return family size', function(done) {

            var familySize = incomeAndAssets.calculateFamilySize_1(
                data.householdDocs,
                data.dictionary,
                data.program_id
            );

            expect(familySize).to.equal(2);

            done();
        });
    });

    describe('getFamilySize', function() {

        it('will find family size', function(done) {

            var familySize = incomeAndAssets.getFamilySize(
                data.householdDocs,
                data.dictionary,
                data.program_id
            );

            expect(familySize).to.equal(2);

            done();
        });
    });

    describe('testIncomeAndAssets', function() {

        it('will return false', function(done) {

            var bool = incomeAndAssets.testIncomeAndAssets(
                data.householdDocs,
                data.aANDiDocs,
                data.baselineDictionary,
                data.dictionary,
                data.program_id
            );

            expect(bool).to.be.false;

            done();
        });

        it('will return true', function(done) {

            data.aANDiDocs = [data.aANDiDocs[1]];

            var bool = incomeAndAssets.testIncomeAndAssets(
                data.householdDocs,
                data.aANDiDocs,
                data.baselineDictionary,
                data.dictionary,
                data.program_id
            );

            expect(bool).to.be.true;

            done();
        });
    });
});