
var conditionDoc = {
    "answerOptions": [
        {
            "answer": "> 6 months",
            "answerNumber": "1"
        },
        {
            "answer": "< 6 months",
            "answerNumber": "2"
        }
    ],
    "category_id": "526344a0-50dd-11e5-b238-0b6941b3afec",
    "channels": [
        "ssd"
    ],
    "client_ids": [
    ],
    "conditionType": "and",
    "conditions": [
        {
            "subconditions": [
                {
                    "criteria": {
                        "question_id": "e9937820-e08b-11e5-b0e4-81c8c9588f4c"
                    },
                    "results": {
                        "answerNumber": "1",
                        "equal": true
                    },
                    "source": "question"
                }
            ]
        }
    ],
    "date_created": "2016-03-02T11:08:54-05:00",
    "date_updated": "2016-04-26T13:26:23-04:00",
    "managedList": false,
    "multiselect": false,
    "question": "How long have you been receiving SSDI?",
    "type": "question"
};

var personDoc = {
    "address": [
        {
            "street": "123 Main Street",
            "unitNumber": "B",
            "city": "Kansas City",
            "zip": "12346",
            "zipPlus4": "",
            "addressTitle": "",
            "state_id": "5264f254-50dd-11e5-b238-0b6941b3afec",
            "country_id": "52636bb7-50dd-11e5-b238-0b6941b3afec",
            "residence": true
        }
    ],
    "channels": [
        "ssd"
    ],
    "citizenshipStatus_id": "",
    "client_ids": [
        "47ebd650-ef92-11e5-8b4d-8f035fe4178b",
        "8d8a9260-db00-11e5-b986-83fc8c1466a6",
        "526c9370-50dd-11e5-b238-0b6941b3afec"
    ],
    "countryOfOrigin_id": "",
    "dateOfBirth": "1942-09-10",
    "dateOfDeath": "",
    "dateOfEntry": "",
    "educationLevel_id": "52636bb2-50dd-11e5-b238-0b6941b3afec",
    "email": [
        {
            "emailAddress": "nick@adams.com",
            "emailTitle": "personal"
        }
    ],
    "employerCompanyName": "",
    "employerJobTitle": "",
    "filename": {
        "originalName": "ah20150903dec.BSC.BAL",
        "serverName": "2da4e4e80e5abaf5bc560f6bec5aa457"
    },
    "gender_id": "52636bba-50dd-11e5-b238-0b6941b3afec",
    "greenCardIssueDate": "",
    "guarantor": {
        "person_id": "249863c0-f1f0-11e5-b734-e51fee80604f",
        "relation_id": "52642f05-50dd-11e5-b238-0b6941b3afec"
    },
    "homeless": false,
    "immigrationStatus_id": "",
    "language_id": "526392c6-50dd-11e5-b238-0b6941b3afec",
    "nameFirst": "NICHOLAS",
    "nameLast": "ADAMS",
    "nameMaiden": "",
    "nameMiddle": "D",
    "namePrefix_id": "5263b9d0-50dd-11e5-b238-0b6941b3afec",
    "nameSuffix_id": "5263e0e2-50dd-11e5-b238-0b6941b3afec",
    "phone": [
        {
            "phoneNumber": "202-222-2222",
            "phoneType_id": "5263e0e8-50dd-11e5-b238-0b6941b3afec"
        }
    ],
    "race_id": "526407f3-50dd-11e5-b238-0b6941b3afec",
    "relationshipStatus_id": "52642f01-50dd-11e5-b238-0b6941b3afec",
    "ssn": "220640815",
    "type": "person",
    "user_id": "7c5a8f70-5335-11e5-8ff1-75243753f1a6",
    "veteran": true,
    "date_updated": "2016-05-10T11:41:42-04:00"
};

var screeningDoc = {
    "accountNumber": "11111111",
    "admission_date": "04/06/2016",
    "channels": [
        "ssd"
    ],
    "client_ids": [
        "526c9370-50dd-11e5-b238-0b6941b3afec"
    ],
    "date_created": "2016-04-26T12:28:05-04:00",
    "date_updated": "2016-05-19T14:30:32-04:00",
    "displayConditions": true,
    "hospital_id": "",
    "medicalRecordNumber": "1111111111",
    "person_id": "249863c0-f1f0-11e5-b734-e51fee80604f",
    "results": [
        {
            "answers": [
                {
                    "answer": "Yes",
                    "answerNumber": "1"
                }
            ],
            "question_id": "e9937820-e08b-11e5-b0e4-81c8c9588f4c",
            "user_id": "update"
        },
        {
            "answers": [
                {
                    "answer": "N/A",
                    "answerNumber": null
                }
            ],
            "question_id": "12951de0-e0ab-11e5-978f-29cc2f67ab21"
        },
        {
            "answers": [
                {
                    "answer": "N/A",
                    "answerNumber": null
                }
            ],
            "question_id": "68e1f350-dc9e-11e5-aa02-19cff5c9bf13"
        },
        {
            "answers": [
                {
                    "answer": "N/A",
                    "answerNumber": null
                }
            ],
            "question_id": "d529fd80-e0a6-11e5-978f-29cc2f67ab21"
        },
        {
            "answers": [
                {
                    "answer": "N/A",
                    "answerNumber": null
                }
            ],
            "question_id": "faceafb0-e090-11e5-b923-d377740fc2dd"
        },
        {
            "answers": [
                {
                    "answer": "N/A",
                    "answerNumber": null
                }
            ],
            "question_id": "0fa63a20-e091-11e5-b923-d377740fc2dd"
        },
        {
            "question_id": "6342ee60-dfdf-11e5-b97c-79067903eb4d",
            "answers": [
                {
                    "answer": "",
                    "answer_id": "52636bbc-50dd-11e5-b238-0b6941b3afec"
                }
            ],
            "user_id": "update"
        },
        {
            "answers": [
                {
                    "answer": "N/A",
                    "answerNumber": null
                }
            ],
            "question_id": "8d262920-dc9e-11e5-aa02-19cff5c9bf13"
        }
    ],
    "state_id": "5264f254-50dd-11e5-b238-0b6941b3afec",
    "type": "screening",
    "user_id": "7c5a8f70-5335-11e5-8ff1-75243753f1a6"
};

/* PUBLIC API */
module.exports.conditionDoc = conditionDoc;
module.exports.personDoc = personDoc;
module.exports.screeningDoc = screeningDoc;