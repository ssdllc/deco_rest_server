var chai = require('chai');
var expect = chai.expect;

var conditionDoc = require('../endPoints/screening/conditionDoc.js');
var data = require('./data/d_conditionDoc.js');

describe('conditionDoc.js', function() {

    describe('getScreeningResult', function() {

        var results = [
            {
                "question_id": "2d7236e0-19d0-11e5-a584-1f18c61b3745",
                "answers": [
                    {
                        "answer": "60,000+",
                        "answerNumber": "2"
                    }
                ]
            },
            {
                "question_id": "6aa246e0-19d0-11e5-a584-1f18c61b3745",
                "answers": [
                    {
                        "answer": "",
                        "answerNumber": "2"
                    }
                ]
            }
        ];

        it('will return a result', function(done) {

            var screeningResult = conditionDoc.getScreeningResult(results, "2d7236e0-19d0-11e5-a584-1f18c61b3745");

            expect(screeningResult).to.be.instanceof(Object);
            expect(screeningResult).to.equal(results[0]);

            done();
        });

        it('will return -1', function(done) {

            var screeningResult = conditionDoc.getScreeningResult(results, "not-a-real-id");

            expect(screeningResult).to.equal(-1);

            done();
        });
    });

    describe('testSubConditionEquals', function() {

        var currentSubCondition = {
            criteria: {
                question_id: "faceafb0-e090-11e5-b923-d377740fc2dd"
            },
            results: {
                answerNumber: "1"
            },
            source: "question"
        };

        var currentScreeningAnswerArray = [
            {
                answer: "Yes",
                answerNumber: "1"
            },
            {
                answer: "No",
                answerNumber: "3"
            }
        ];

        it('will return true', function(done) {

            var currentSubCheck = conditionDoc.testSubConditionEquals(currentSubCondition, currentScreeningAnswerArray, false);

            expect(currentSubCheck).to.be.true;

            done();
        });

        it('will return currentSubCheck', function(done) {

            currentScreeningAnswerArray[0].answerNumber = "2";
            var currentSubCheck = conditionDoc.testSubConditionEquals(currentSubCondition, currentScreeningAnswerArray, false);

            expect(currentSubCheck).to.be.false;

            done();
        });
    });

    describe('testSubConditionNotEquals', function() {

        var currentSubCondition = {
            criteria: {
                question_id: "faceafb0-e090-11e5-b923-d377740fc2dd"
            },
            results: {
                answerNumber: "1"
            },
            source: "question"
        };

        var currentScreeningAnswerArray = [
            {
                answer: "Yes",
                answerNumber: "2"
            },
            {
                answer: "Maybe",
                answerNumber: "3"
            }
        ];

        it('will return true', function(done) {

            var currentSubCheck = conditionDoc.testSubConditionNotEquals(currentSubCondition, currentScreeningAnswerArray);

            expect(currentSubCheck).to.be.true;

            done();
        });

        it('will return false', function(done) {

            currentScreeningAnswerArray[0].answerNumber = "1";
            var currentSubCheck = conditionDoc.testSubConditionNotEquals(currentSubCondition, currentScreeningAnswerArray);

            expect(currentSubCheck).to.be.false;

            done();
        });
    });

    describe('falseCheck', function() {

        it('will return false', function(done) {

            var elig1 = conditionDoc.falseCheck(true, false);
            var elig2 = conditionDoc.falseCheck("NMI", false);

            expect(elig1).to.be.false;
            expect(elig2).to.be.false;

            done();
        });

        it('will return "NMI"', function(done) {

            var elig = conditionDoc.falseCheck(true, "NMI");

            expect(elig).to.equal("NMI");

            done();
        });

        it('will return the first parameter', function(done) {

            var elig = conditionDoc.trueCheck(true, true);

            expect(elig).to.be.true;

            done();
        });
    });

    describe('trueCheck', function() {

        it('will return true', function(done) {

            var elig1 = conditionDoc.trueCheck(false, true);
            var elig2 = conditionDoc.trueCheck("NMI", true);

            expect(elig1).to.be.true;
            expect(elig2).to.be.true;

            done();
        });

        it('will return "NMI"', function(done) {

            var elig = conditionDoc.trueCheck(false, "NMI");

            expect(elig).to.equal("NMI");

            done();
        });

        it('will return the first parameter', function(done) {

            var elig = conditionDoc.trueCheck(false, false);

            expect(elig).to.be.false;

            done();
        });
    });

    describe('checkOperator exported as testConditions', function() {

        it('will return true', function(done) {

            var elig = conditionDoc.testConditions(data.conditionDoc, data.personDoc, data.screeningDoc);

            expect(elig).to.be.true;

            done();
        });

        it('will return false', function(done) {

            data.conditionDoc.conditions[0].subconditions[0].results.answerNumber = "2";
            var elig = conditionDoc.testConditions(data.conditionDoc, data.personDoc, data.screeningDoc);

            expect(elig).to.be.false;

            done();
        });

        it('will return NMI', function(done) {

            data.screeningDoc.results = [];
            var elig = conditionDoc.testConditions(data.conditionDoc, data.personDoc, data.screeningDoc);

            expect(elig).to.equal("NMI");

            done();
        });
    });
});