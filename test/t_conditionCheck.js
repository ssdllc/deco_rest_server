var moment = require('moment');
var chai = require('chai');
var expect = chai.expect;

require('../init.js');

var conditionCheck = require('../helpers/conditionCheck.js');

describe('conditionCheck.js', function() {

    describe('conditionCheck', function() {

        it("will return true and false for 'equals'", function(done) {

            var trueCondition = conditionCheck.conditionCheck(1,1,-1,"equals");
            var falseCondition = conditionCheck.conditionCheck(1,2,-1,"equals");

            expect(trueCondition).to.be.true;
            expect(falseCondition).to.be.false;

            done();
        });

        it("will return true and false for 'different'", function(done) {

            var trueCondition = conditionCheck.conditionCheck(1,2,-1,"different");
            var falseCondition = conditionCheck.conditionCheck(1,1,-1,"different");

            expect(trueCondition).to.be.true;
            expect(falseCondition).to.be.false;

            done();
        });


        // DATES

        it("will return true and false for 'lessThanDate'", function(done) {

            var trueCondition = conditionCheck.conditionCheck("1995-01-01","1995-01-02",-1,"lessThanDate");
            var falseCondition = conditionCheck.conditionCheck("1995-01-01","1995-01-01",-1,"lessThanDate");

            expect(trueCondition).to.be.true;
            expect(falseCondition).to.be.false;

            done();
        });

        it("will return true and false for 'lessThanOrEqualToDate'", function(done) {

            var trueCondition1 = conditionCheck.conditionCheck("1995-01-01","1995-01-02",-1,"lessThanOrEqualToDate");
            var trueCondition2 = conditionCheck.conditionCheck("1995-01-01","1995-01-01",-1,"lessThanOrEqualToDate");
            var falseCondition = conditionCheck.conditionCheck("1996-01-01","1995-01-01",-1,"lessThanOrEqualToDate");

            expect(trueCondition1).to.be.true;
            expect(trueCondition2).to.be.true;
            expect(falseCondition).to.be.false;

            done();
        });

        it("will return true and false for 'moreThanDate'", function(done) {

            var trueCondition = conditionCheck.conditionCheck("1995-01-02","1995-01-01",-1,"moreThanDate");
            var falseCondition = conditionCheck.conditionCheck("1995-01-01","1995-01-01",-1,"moreThanDate");

            expect(trueCondition).to.be.true;
            expect(falseCondition).to.be.false;

            done();
        });

        it("will return true and false for 'moreThanOrEqualToDate'", function(done) {

            var trueCondition1 = conditionCheck.conditionCheck("1995-01-02","1995-01-01",-1,"moreThanOrEqualToDate");
            var trueCondition2 = conditionCheck.conditionCheck("1995-01-01","1995-01-01",-1,"moreThanOrEqualToDate");
            var falseCondition = conditionCheck.conditionCheck("1995-01-01","1996-01-01",-1,"moreThanOrEqualToDate");

            expect(trueCondition1).to.be.true;
            expect(trueCondition2).to.be.true;
            expect(falseCondition).to.be.false;

            done();
        });

        it("will return true and false for 'equalToDate'", function(done) {

            var trueCondition = conditionCheck.conditionCheck("1995-01-01","1995-01-01",-1,"equalToDate");
            var falseCondition = conditionCheck.conditionCheck("1995-01-01","1996-01-01",-1,"equalToDate");

            expect(trueCondition).to.be.true;
            expect(falseCondition).to.be.false;

            done();
        });

        it("will return true and false for 'betweenDates'", function(done) {

            var trueCondition = conditionCheck.conditionCheck("1991-01-05","1991-01-04","1991-01-06","betweenDates");
            var falseCondition = conditionCheck.conditionCheck("1991-01-05","1991-01-05","1991-01-06","betweenDates");

            expect(trueCondition).to.be.true;
            expect(falseCondition).to.be.false;

            done();
        });

        // DATES BY DAY

        it("will return true and false for 'lessThanDateByDay'", function(done) {

            var trueCondition = conditionCheck.conditionCheck("1995-01-01","1995-01-02",-1,"lessThanDateByDay");
            var falseCondition = conditionCheck.conditionCheck("1995-01-01","1995-01-01",-1,"lessThanDateByDay");

            expect(trueCondition).to.be.true;
            expect(falseCondition).to.be.false;

            done();
        });

        it("will return true and false for 'lessThanOrEqualToDateByDay'", function(done) {

            var trueCondition1 = conditionCheck.conditionCheck("1995-01-01","1995-01-02",-1,"lessThanOrEqualToDateByDay");
            var trueCondition2 = conditionCheck.conditionCheck("1995-01-01","1995-01-01",-1,"lessThanOrEqualToDateByDay");
            var falseCondition = conditionCheck.conditionCheck("1996-01-01","1995-01-01",-1,"lessThanOrEqualToDateByDay");

            expect(trueCondition1).to.be.true;
            expect(trueCondition2).to.be.true;
            expect(falseCondition).to.be.false;

            done();
        });

        it("will return true and false for 'moreThanDateByDay'", function(done) {

            var trueCondition = conditionCheck.conditionCheck("1995-01-02","1995-01-01",-1,"moreThanDateByDay");
            var falseCondition = conditionCheck.conditionCheck("1995-01-01","1995-01-01",-1,"moreThanDateByDay");

            expect(trueCondition).to.be.true;
            expect(falseCondition).to.be.false;

            done();
        });

        it("will return true and false for 'moreThanOrEqualToDateByDay'", function(done) {

            var trueCondition1 = conditionCheck.conditionCheck("1995-01-02","1995-01-01",-1,"moreThanOrEqualToDateByDay");
            var trueCondition2 = conditionCheck.conditionCheck("1995-01-01","1995-01-01",-1,"moreThanOrEqualToDateByDay");
            var falseCondition = conditionCheck.conditionCheck("1995-01-01","1996-01-01",-1,"moreThanOrEqualToDateByDay");

            expect(trueCondition1).to.be.true;
            expect(trueCondition2).to.be.true;
            expect(falseCondition).to.be.false;

            done();
        });

        it("will return true and false for 'equalToDateByDay'", function(done) {

            var trueCondition = conditionCheck.conditionCheck("1995-01-01","1995-01-01",-1,"equalToDateByDay");
            var falseCondition = conditionCheck.conditionCheck("1995-01-01","1996-01-01",-1,"equalToDateByDay");

            expect(trueCondition).to.be.true;
            expect(falseCondition).to.be.false;

            done();
        });

        it("will return true and false for 'betweenDatesByDay'", function(done) {

            var trueCondition = conditionCheck.conditionCheck("1991-01-05","1991-01-04","1991-01-06","betweenDatesByDay");
            var falseCondition = conditionCheck.conditionCheck("1991-01-05","1991-01-05","1991-01-06","betweenDatesByDay");

            expect(trueCondition).to.be.true;
            expect(falseCondition).to.be.false;

            done();
        });

        // YEAR

        it("will return true and false for 'lessThanYear'", function(done) {

            var trueVal = moment().diff('1990-01-01', 'years');
            var falseVal = moment().diff('1995-01-01', 'years');

            var trueCondition = conditionCheck.conditionCheck("1991-01-01",trueVal,-1,"lessThanYear");
            var falseCondition = conditionCheck.conditionCheck("1991-01-01",falseVal,-1,"lessThanYear");

            expect(trueCondition).to.be.true;
            expect(falseCondition).to.be.false;

            done();
        });

        it("will return true and false for 'lessThanOrEqualToYear'", function(done) {

            var trueVal1 = moment().diff('1990-01-01', 'years');
            var trueVal2 = moment().diff('1991-01-01', 'years');
            var falseVal = moment().diff('1995-01-01', 'years');

            var trueCondition1 = conditionCheck.conditionCheck("1991-01-01",trueVal1,-1,"lessThanOrEqualToYear");
            var trueCondition2 = conditionCheck.conditionCheck("1991-01-01",trueVal2,-1,"lessThanOrEqualToYear");
            var falseCondition = conditionCheck.conditionCheck("1991-01-01",falseVal,-1,"lessThanOrEqualToYear");

            expect(trueCondition1).to.be.true;
            expect(trueCondition2).to.be.true;
            expect(falseCondition).to.be.false;

            done();
        });

        it("will return true and false for 'moreThanYear'", function(done) {

            var trueVal1 = moment().diff('1992-01-01', 'years');
            var falseVal = moment().diff('1985-01-01', 'years');

            var trueCondition = conditionCheck.conditionCheck("1991-01-01",trueVal1,-1,"moreThanYear");
            var falseCondition = conditionCheck.conditionCheck("1991-01-01",falseVal,-1,"moreThanYear");

            expect(trueCondition).to.be.true;
            expect(falseCondition).to.be.false;

            done();
        });

        it("will return true and false for 'moreThanOrEqualToYear'", function(done) {

            var trueVal1 = moment().diff('1992-01-01', 'years');
            var trueVal2 = moment().diff('1991-01-01', 'years');
            var falseVal = moment().diff('1985-01-01', 'years');

            var trueCondition1 = conditionCheck.conditionCheck("1991-01-01",trueVal1,-1,"moreThanOrEqualToYear");
            var trueCondition2 = conditionCheck.conditionCheck("1991-01-01",trueVal2,-1,"moreThanOrEqualToYear");
            var falseCondition = conditionCheck.conditionCheck("1991-01-01",falseVal,-1,"moreThanOrEqualToYear");

            expect(trueCondition1).to.be.true;
            expect(trueCondition2).to.be.true;
            expect(falseCondition).to.be.false;

            done();
        });

        it("will return true and false for 'equalToYear'", function(done) {

            var trueVal = moment().diff('1991-01-01', 'years');
            var falseVal = moment().diff('1995-01-01', 'years');

            var trueCondition = conditionCheck.conditionCheck("1991-01-01",trueVal,-1,"equalToYear");
            var falseCondition = conditionCheck.conditionCheck("1991-01-01",falseVal,-1,"equalToYear");

            expect(trueCondition).to.be.true;
            expect(falseCondition).to.be.false;

            done();
        });

        it("will return true and false for 'betweenYears'", function(done) {

            var trueVal1 = moment().diff('1990-01-01', 'years');
            var trueVal2 = moment().diff('1995-01-01', 'years');
            var falseVal1 = moment().diff('1992-01-01', 'years');
            var falseVal2 = moment().diff('1995-01-01', 'years');


            var trueCondition = conditionCheck.conditionCheck("1991-01-01",trueVal2,trueVal1,"betweenYears");
            var falseCondition = conditionCheck.conditionCheck("1991-01-01",falseVal2,falseVal1,"betweenYears");

            expect(trueCondition).to.be.true;
            expect(falseCondition).to.be.false;

            done();
        });

        // MONTH

        it("will return true and false for 'lessThanMonth'", function(done) {

            var trueVal = moment().diff('1990-01-01', 'months');
            var falseVal = moment().diff('1995-01-01', 'months');

            var trueCondition = conditionCheck.conditionCheck("1991-01-01",trueVal,-1,"lessThanMonth");
            var falseCondition = conditionCheck.conditionCheck("1991-01-01",falseVal,-1,"lessThanMonth");

            expect(trueCondition).to.be.true;
            expect(falseCondition).to.be.false;

            done();
        });

        it("will return true and false for 'lessThanOrEqualToMonth'", function(done) {

            var trueVal1 = moment().diff('1990-01-01', 'months');
            var trueVal2 = moment().diff('1991-01-01', 'months');
            var falseVal = moment().diff('1995-01-01', 'months');

            var trueCondition1 = conditionCheck.conditionCheck("1991-01-01",trueVal1,-1,"lessThanOrEqualToMonth");
            var trueCondition2 = conditionCheck.conditionCheck("1991-01-01",trueVal2,-1,"lessThanOrEqualToMonth");
            var falseCondition = conditionCheck.conditionCheck("1991-01-01",falseVal,-1,"lessThanOrEqualToMonth");

            expect(trueCondition1).to.be.true;
            expect(trueCondition2).to.be.true;
            expect(falseCondition).to.be.false;

            done();
        });

        it("will return true and false for 'moreThanMonth'", function(done) {

            var trueVal1 = moment().diff('1992-01-01', 'months');
            var falseVal = moment().diff('1985-01-01', 'months');

            var trueCondition = conditionCheck.conditionCheck("1991-01-01",trueVal1,-1,"moreThanMonth");
            var falseCondition = conditionCheck.conditionCheck("1991-01-01",falseVal,-1,"moreThanMonth");

            expect(trueCondition).to.be.true;
            expect(falseCondition).to.be.false;

            done();
        });

        it("will return true and false for 'moreThanOrEqualToMonth'", function(done) {

            var trueVal1 = moment().diff('1992-01-01', 'months');
            var trueVal2 = moment().diff('1991-01-01', 'months');
            var falseVal = moment().diff('1985-01-01', 'months');

            var trueCondition1 = conditionCheck.conditionCheck("1991-01-01",trueVal1,-1,"moreThanOrEqualToMonth");
            var trueCondition2 = conditionCheck.conditionCheck("1991-01-01",trueVal2,-1,"moreThanOrEqualToMonth");
            var falseCondition = conditionCheck.conditionCheck("1991-01-01",falseVal,-1,"moreThanOrEqualToMonth");

            expect(trueCondition1).to.be.true;
            expect(trueCondition2).to.be.true;
            expect(falseCondition).to.be.false;

            done();
        });

        it("will return true and false for 'equalToMonth'", function(done) {

            var trueVal = moment().diff('1991-01-01', 'months');
            var falseVal = moment().diff('1995-01-01', 'months');

            var trueCondition = conditionCheck.conditionCheck("1991-01-01",trueVal,-1,"equalToMonth");
            var falseCondition = conditionCheck.conditionCheck("1991-01-01",falseVal,-1,"equalToMonth");

            expect(trueCondition).to.be.true;
            expect(falseCondition).to.be.false;

            done();
        });

        it("will return true and false for 'betweenMonths'", function(done) {

            var trueVal1 = moment().diff('1990-01-01', 'months');
            var trueVal2 = moment().diff('1995-01-01', 'months');
            var falseVal1 = moment().diff('1992-01-01', 'months');
            var falseVal2 = moment().diff('1995-01-01', 'months');


            var trueCondition = conditionCheck.conditionCheck("1991-01-01",trueVal2,trueVal1,"betweenMonths");
            var falseCondition = conditionCheck.conditionCheck("1991-01-01",falseVal2,falseVal1,"betweenMonths");

            expect(trueCondition).to.be.true;
            expect(falseCondition).to.be.false;

            done();
        });

        // DAY

        it("will return true and false for 'lessThanDay'", function(done) {

            var trueVal = moment().diff('1990-01-01', 'days');
            var falseVal = moment().diff('1995-01-01', 'days');

            var trueCondition = conditionCheck.conditionCheck("1991-01-01",trueVal,-1,"lessThanDay");
            var falseCondition = conditionCheck.conditionCheck("1991-01-01",falseVal,-1,"lessThanDay");

            expect(trueCondition).to.be.true;
            expect(falseCondition).to.be.false;

            done();
        });

        it("will return true and false for 'lessThanOrEqualToDay'", function(done) {

            var trueVal1 = moment().diff('1990-01-01', 'days');
            var trueVal2 = moment().diff('1991-01-01', 'days');
            var falseVal = moment().diff('1995-01-01', 'days');

            var trueCondition1 = conditionCheck.conditionCheck("1991-01-01",trueVal1,-1,"lessThanOrEqualToDay");
            var trueCondition2 = conditionCheck.conditionCheck("1991-01-01",trueVal2,-1,"lessThanOrEqualToDay");
            var falseCondition = conditionCheck.conditionCheck("1991-01-01",falseVal,-1,"lessThanOrEqualToDay");

            expect(trueCondition1).to.be.true;
            expect(trueCondition2).to.be.true;
            expect(falseCondition).to.be.false;

            done();
        });

        it("will return true and false for 'moreThanDay'", function(done) {

            var trueVal1 = moment().diff('1992-01-01', 'days');
            var falseVal = moment().diff('1985-01-01', 'days');

            var trueCondition = conditionCheck.conditionCheck("1991-01-01",trueVal1,-1,"moreThanDay");
            var falseCondition = conditionCheck.conditionCheck("1991-01-01",falseVal,-1,"moreThanDay");

            expect(trueCondition).to.be.true;
            expect(falseCondition).to.be.false;

            done();
        });

        it("will return true and false for 'moreThanOrEqualToDay'", function(done) {

            var trueVal1 = moment().diff('1992-01-01', 'days');
            var trueVal2 = moment().diff('1991-01-01', 'days');
            var falseVal = moment().diff('1985-01-01', 'days');

            var trueCondition1 = conditionCheck.conditionCheck("1991-01-01",trueVal1,-1,"moreThanOrEqualToDay");
            var trueCondition2 = conditionCheck.conditionCheck("1991-01-01",trueVal2,-1,"moreThanOrEqualToDay");
            var falseCondition = conditionCheck.conditionCheck("1991-01-01",falseVal,-1,"moreThanOrEqualToDay");

            expect(trueCondition1).to.be.true;
            expect(trueCondition2).to.be.true;
            expect(falseCondition).to.be.false;

            done();
        });

        it("will return true and false for 'equalToDay'", function(done) {

            var trueVal = moment().diff('1991-01-01', 'days');
            var falseVal = moment().diff('1995-01-01', 'days');

            var trueCondition = conditionCheck.conditionCheck("1991-01-01",trueVal,-1,"equalToDay");
            var falseCondition = conditionCheck.conditionCheck("1991-01-01",falseVal,-1,"equalToDay");

            expect(trueCondition).to.be.true;
            expect(falseCondition).to.be.false;

            done();
        });

        it("will return true and false for 'betweenDays'", function(done) {

            var trueVal1 = moment().diff('1990-01-01', 'days');
            var trueVal2 = moment().diff('1995-01-01', 'days');
            var falseVal1 = moment().diff('1992-01-01', 'days');
            var falseVal2 = moment().diff('1995-01-01', 'days');


            var trueCondition = conditionCheck.conditionCheck("1991-01-01",trueVal2,trueVal1,"betweenDays");
            var falseCondition = conditionCheck.conditionCheck("1991-01-01",falseVal2,falseVal1,"betweenDays");

            expect(trueCondition).to.be.true;
            expect(falseCondition).to.be.false;

            done();
        });
    });
});